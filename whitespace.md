#terminology

## Whitespace
Characters that you can't see, i.e. characters that would appear "white" when printed on white paper.

## Example
The most common whitespace characters are:
- ``' '``, space
- ``'\t'``, tab
- ``'\n'``, newline
- ``\'r'``, return
