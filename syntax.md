---
aliases: [syntax error]
---
#terminology

## Definition
The syntax of a programming language is the order in which it is permissible to write tokens in the language. It is like grammar in natural languages.

## Example
A variable definition in C++ looks like this:

```cpp
double temp = 21.4;
```

This definition consists of five tokens: `double`, `temp`, `=`, `21.4`, and `;`.

Here are some examples of syntax errors for this definition.

```cpp
double temp = 21.4    // syntax error: missing ;
temp double = 21.4;   // syntax error: type should come before name
double temp 21.4;     // syntax error: missing =
double temp == 21.4;  // syntax error: should be =, not ==
```

For a *definition* of a variable, this is a syntax error:

```cpp
temp = 21.4;  // syntax error: missing type
```

However, the statement `temp = 21.4` is a correct *assignment statement*. But it has a syntax error if it's meant as a *definition*.

In C++, syntax errors are usually caught by the compiler.
