Inheritance is a trick for making new classes by extending old classes. In many object-oriented programming languages inheritance is one of the key ways of creating and organizing classes.

## The Intuitive Idea
Suppose you have this struct for representing people:

```cpp
struct Person {
	string first;
	string last;
	string get_name() const { 
	   return first + " " + last; 
	}
};
```

You could use it like this:

```cpp
Person p{"Grace", "Hopper"};
cout << p.get_name();
// ...
```

In memory the object is like this:

![[Person.png]]

Now suppose we create another struct for representing people, but this one includes there age:

```cpp
struct Person2 {
	string first;
	string last;
	string get_name() const { 
	   return first + " " + last; 
	}
	int age;
};
```

You could use it like this:

```cpp
Person2 p{"Grace", "Hopper", 20};
cout << p.get_name();
// ...
```

In memory the object is like this:

![[Person2.png]]

Notice that anything you can do to a `Person` object, you can also do to a `Person2` object *if* you ignore the `age` variable. In other word, any code that works with a `Person` object should have no problem working with a `Person2` object.

When we define `Person` and `Person2` separately as we've done here, then C++ doesn't know that they are related. But if define `Person2` by **inheriting** from `Person`, then this tells C++ that they are related, and C++ will let you use a `Person2` anywhere you need a `Person`.

`Person2` can be define like this:

```cpp
struct Person2 : public Person {   // Person2 inherits from Person
   int age;
};
```

We say that `Person2` **inherits** from `Person`. `Person2` automatically gets a copy of all the variables and methods in `Person`, and all we we need to put in `Person2` are its extra features, i.e. the `age` variable.

This makes the definition of `Person2` shorter, although it is now up to the programmer to check what it is inheriting from `Person`. This can be quite tricky when you have classes with inherit from classes, that inherit from other classes, and so on.

Another reason to do define classes with inheritance is that it tells C++, and other programmers, more about how the classes are related.

## Example: an int_vec from a vector
The standard `vector<int>` class has many uses and lets us write code like this:

```cpp
vector<int> v;
v.push_back(4);
v.push_back(5);
v.push_back(1);
for(int n : v) cout << n << "\n";
```

Now suppose we want a new class that is just like ``vector<int>``, but with a few extra features, e.g.:

```cpp
int_vec v("Table 1");                // extra feature: has a name
v.push_back(4);
v.push_back(5);
v.push_back(1);

cout << v.get_name() << "\n";        // extra feature: name getter
v.sort_increasing();                 // extra feature: sort the vector
for(int n : v) cout << n << "\n";
cout << "sum = " << v.sum() << "\n"; // extra feature: sum the vector
```

`int_vec` does everything a `vector<int>`, plus a few extra things. We could create `int_vec` from scratch, but it's much easier is to use inheritance because it gives us all the features of `vector<int>` for free.

## Step 1: Inheriting from a vector
To create `int_vec`, we first start by inheriting from `vector<int>` like this:

```cpp
#include <iostream>
#include <vector>

using namespace std;

class int_vec : public vector<int> {  // int_vec inherits from vector<int>
	// nothing yet!
}; // class int_vec

int main() {
	int_vec v;
	v.push_back(4);
	v.push_back(5);
	v.push_back(1);
	for(int n : v) cout << n << "\n";
}
```

The line `class int_vec : public vector<int>` tells C++ we want all the members of `vector<int>` to be put into `int_vec`.  Even without adding anything else to `int_vec`, we now have a new class that does everything a `vector<int>` can do.

### A Note on public Inheritance
We are using [[public inheritance]] when we write ``: public vector<int>``

The `public` qualifier here means that the `public` members of `vector<int>` remain `public` in the inheriting class, and  the `private` members remain `private`.

C++ also allows `: protected vector<int>`, which inherits all member variables or methods in `vector<int>` so that they are `protected` in `int_vec`. A `protected` member is essentially one that is accessible just in its class and any class that inherits from it.

C++ also allows `: private vector<int>` inheritance, which inherits all member variables or methods in `vector<int>` so that they are `private` in `int_vec`.

**In this course, inheritance will always mean** `public`-style inheritance,
e.g. `: public vector<int>`.

In `public`-style inheritance, the visibility of member variables and methods in the class being inherited stay the same.


## Step 2: Adding the sum() Method
We can add new methods to `int_vec`, e.g.:

```cpp
#include <iostream>
#include <vector>
#include <numeric>

using namespace std;

class int_vec : public vector<int> {
public:
	// The begin() and end() methods are inherited from vector<int>, and so we
	// are free to use them in int_vec methods.
	int sum() const {
		return std::accumulate(begin(), end(), 0);
	}

}; // class int_vec

int main() {
	int_vec v;

	v.push_back(4);
	v.push_back(5);
	v.push_back(1);
	
	for(int n : v) cout << n << "\n";
	cout << "sum = " << v.sum() << "\n";
}
```

`sum` returns the sum of all the elements in the `int_vec`. `begin()` and `end()` are methods defined in `vector<int>` that return [[iterators]] to the begin and end of the vector. `accumulate` is a standard [[standard template library]] function that sums elements, and so `std::accumulate(begin(), end(), 0)` returns the sum of all the elements in the vector (the 0 means that the accumulator starts with the value 0).

## Step 3: Adding a Sorting Method
Here's a helper method for sorting:

```cpp
#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>

using namespace std;

class int_vec : public vector<int> {
public:
	int sum() const {
		return std::accumulate(begin(), end(), 0);
	}

	// The begin() and end() methods are inherited from vector<int>, and so we
	// are free to use them in int_vec methods.
	void sort_increasing() {
		std::sort(begin(), end());
	}

}; // class int_vec

int main() {
	int_vec v;

	v.push_back(4);
	v.push_back(5);
	v.push_back(1);
	
	v.sort_increasing();
	for(int n : v) cout << n << "\n";
	cout << "sum = " << v.sum() << "\n";
}
```

The `int_vec::sort_increasing` method calls the standard [[standard template library]] sorting function named `std::sort`. 

## Step 4: Adding a Name
Now lets add a string for the name of the `int_vec`. This is a little more involved because we're making the name private, and adding a constructor to initialize it:

```cpp
#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>
#include <string>

using namespace std;

class int_vec : public vector<int> {
	string name;
public:
	// constructor requires a name
	int_vec(const string& s)
	: vector<int>(),  // call default constructor of vector<int>
	  name(s)
	{ }

	// name getter
	string get_name() const { return name; }

	int sum() const {
		return std::accumulate(begin(), end(), 0);
	}

	void sort_increasing() {
		std::sort(begin(), end());
	}

}; // class int_vec

int main() {
	int_vec v("Table 1");

	v.push_back(4);
	v.push_back(5);
	v.push_back(1);
	
	v.sort_increasing();
	cout << v.get_name() << ":\n";
	for(int n : v) cout << n << "\n";
	cout << "sum = " << v.sum() << "\n";
}
```

First, notice how the constructor calls the [[default constructor]] of `vector<int>()` in its [[initialization list]]. This is important! We must call the constructor for `vector<int>` to ensure that its variables are initialized properly. After the `vector<int>` constructor finishes, we then initialize `name`.

Second, we add a [[getter]] called `get_name` that returns the name of the `int_vec`. In this design, there's no way to change the name of after it has been initialized.

Again, the important idea here is that `int_vec` *extends* the functionality of `vector<int>` by adding new methods and member variables. An `int_vec` does *everything* a `vector<int>` can do, plus a little more.

## Using an int_vec When a vector is Needed
We can use `int_vec` in situations that require a `vector<int>`. Consider this code:

```cpp
// ...

void summarize(const vector<int>& v) {
	for(int i = 0; i < v.size(); i++) {
		cout << "v[" << i << "] = " << v[i] << "\n";
	}
	cout << "size: " << v.size() << "\n";
}

int main() {
	int_vec v("Table 1");

	v.push_back(4);
	v.push_back(5);
	v.push_back(1);
	
	summarize(v);
}
```

`summarize` takes a `vector<int>` as input, but here we pass it an `int_vec`. This is fine, because we declared `int_vec` by inheriting from `vector<int>`, and so an `int_vec` can do everything a `vector<int>` can do (and more).

Now suppose we add the function `fancy_summarize`, which takes an `int_vec` as input:

```cpp
// ...

void fancy_summarize(const int_vec& v) {
	cout << v.get_name() << ":\n";
	for(int n : v) cout << "   " << n << "\n";
	cout << "sum = " << v.sum() << "\n";
}

int main() {
	int_vec v("Table 1");
	
	v.push_back(4);
	v.push_back(5);
	v.push_back(1);
	
	summarize(v);
	cout << "\n";
	fancy_summarize(v);
}
```

Since `v` is of type `int_vec`, we can pass it to `fancy_summarize`. However, we *cannot* pass a `vector<int>` to `fancy_summarize`:

```cpp
	int main() {
		vector<int> w;

		fancy_summarize(w);  // error: doesn't compile
	}
```

This causes a compiler error because `fancy_summarize` needs an `int_vec`. You can see why: `fancy_summarize` calls `v.get_name()` and `v.sum()`, but a `vector<int>` does not have those methods.

## Class Diagrams
[[Class diagrams]] are a visual way of showing how classes relate. Classes  are drawn as rectangles, with the name of the class written inside near the top edge. Under the title you write the any important member variables or methods. Arrows are used to indicate inheritance.

For example, the`int_vec` class inherits from the `vector<int>` class, and we can draw this [[class diagram]]:

![[int_vec_ClassDiagram.svg]]

The direction of the arrow matters: it points from the inheriting class (`int_vec`) to the class it inherits from (`vector<int>`). Inside the box we list any important member variables and methods. Even though `vector<int>` has many methods and variables, we only list of couple of them most relevant to the example code given above.


## A Bigger Example: Printable Objects
Consider these three classes: `Point`, `Person`, and `Reading` (for temperature readings):

```cpp
#include "cmpt_error.h"
#include <iostream>

using namespace std;

class Point {
private:
    double x;
    double y;

public:
    // default constructor
    Point() : x(0), y(0) { }

    // copy constructor
    Point(const Point& other) : x(other.x), y(other.y) { }

    Point(double a, double b) : x(a), y(b) { }

    // getters
    double get_x() const { return x; }
    double get_y() const { return y; }

    void print() const {
        cout << '(' << x << ", " << y << ')';
    }

    void println() const {
        print();
        cout << "\n";
    }
}; // class Point


class Person {
    string name;
    int age;
public:
    Person(const string& n, int a)
    : name{n}, age{a}
    {
        if (age < 0) cmpt::error("negative age");
    }

    string get_name() const { return name; }
    int get_age() const { return age; }

    void print() const {
        cout << "Name: '" << name << ", Age: " << age;
    }

    void println() const {
        print();
        cout << "\n";
    }
}; // class Person


class Reading {
private:
    string loc;
    double temp;
public:
    Reading(const string& l, double t)
    : loc{l}, temp{t}
    { }

    string get_loc() const { return loc; }
    double get_temp() const { return temp; }

    void print() const {
        cout << temp << " degrees at " << loc;
    }

    void println() const {
        print();
        cout << "\n";
    }
}; // class Reading

int main() {
    Point a{1, 2};
    a.println();

    Person b{"Katja", 22};
    b.println();

    Reading c{"backyard", 2.4};
    c.println();
}
```

Here is a [[class diagram]] for these methods:

![[pointPersonReadingClassDiagram.svg]]

There are no arrows because none of of these classes inherit from any other class. They are unrelated classes.

Note that each class has a method called `println` with exactly the same definition:

```cpp
void println() const {
    print();
    cout << "\n";
}
```

Repeated methods is a sign that inheritance might help. Instead of writing the same method in each class, maybe we could inherit it?

So lets create a new class, called `Printable`, that holds the `println` method:

```cpp
class Printable {
public:

    // prints the object to cout followed by "\n"
    void println() const {
        print();
        cout << "\n";
    }

}; // class Printable
```

Now we can re-write the classes to inherit from `Printable`. Here's how `Point` would be re-written (the idea is the same for the other two classes):

```cpp
class Point : public Printable {
private:
    double x;
    double y;

public:
    // default constructor
    Point() : x(0), y(0) { }

    // copy constructor
    Point(const Point& other) : x(other.x), y(other.y) { }

    Point(double a, double b) : x(a), y(b) { }

    // getters
    double get_x() const { return x; }
    double get_y() const { return y; }

    void print() const {
        cout << '(' << x << ", " << y << ')';
    }

}; // class Point
```

The difference is that now `Point` inherits from `Printable` thanks to the line `: public Printable`,  and thus we no longer have to write `println` inside `Point`.

But there's a problem: **this example does not compile!**

The error is in the `Printable` class: it uses, but does not define, a `print()` method:

```cpp
class Printable {
public:
	void println() const {
		print();  //  <---- error on this line: print() is not defined
		cout << "\n";
	}

}; // class Printable
```

The `Point` class does define a `print()` method, but `Printable` doesn't know anything about it. So we need to add a `print()` method to `Printable`.

But, unlike `println()`, `print()` will be *different* for every class. How should `print()` be implemented inside `Printable`? There's no way to know.

What we want to tell C++ is that the class that inherits from `Printable` will supply the `print()` method. C++ lets us do that like this:

```cpp
class Printable {
public:
    // prints the object to cout
    virtual void print() const = 0;

    // prints the object to cout followed by "\n"
    void println() const {
        print();
        cout << "\n";
    }

}; // class Printable
```

Notice these things about the `print()` method:
- It is declared `virtual`. This means that an inheriting class is allowed to supply its own implementation of `print`. Note that `println` is *not* virtual, and so classes that inherit from `Printable` *cannot* provide  their own implementation of `println`.
- It has no implementation. We write `= 0` to indicate no implementation is provided here (the implementation will be supplied by an inheriting class).

With this, the code ... still doesn't compile! At least, it doesn't compile using the course makefile, which uses the option `-Wnon-virtual-dtor`.

So to fix that, add a [[virtual destructor]]:

```cpp
class Printable {
public:
    // virtual destructor
    virtual ~Printable() { }

    // prints the object to cout
    virtual void print() const = 0;

    // prints the object to cout followed by "\n"
    void println() const {
        print();
        cout << "\n";
    }

}; // class Printable
```

## Inheritance and Destructors
It's not obvious why a [[virtual destructor]] is needed, so lets go through an example that shows the problem. In what follows, we assume `-Wnon-virtual-dtor` is *not* used as a compiler option.

Lets add a [[destructor]] to `Point`:

```cpp
class Point : public Printable {
private:
    // ... 

public:
    // ...

    ~Point() {
        print();
        cout << " destroyed\n";
    }

    // ...
}; // class Point
```

This code works as expected:

```cpp
Point a{1, 2};
a.println();
```

It prints this:

```
(1, 2)
(1, 2) destroyed
```

This code also works as expected:

```cpp
Point* p = new Point(3, 4);
p->println();   // short for: (*p).println()
delete p;
```

it prints this:

```
(3, 4)
(3, 4) destroyed
```

But something different happens here:

```cpp
Printable* p = new Point(3, 4);  // notice the difference!
p->println();
delete p;
```

Do you see what has changed? `p` is of type `Printable*`, not `Point*` --- everything else is the same.

This compiles (with warnings!), and prints this:

```
(3, 4)
```

Apparently the [[destructor]] is *not being called*!

That's a big problem --- [[destructor|destructors]] are often vital, e.g. think of the `int_vec` class where the [[destructor]] is responsible for deleting the internal array (thus avoiding a [[memory leak]] and a [[dangling pointer]]).

To see what's happening, lets add a [[destructor]] to `Printable`:

```cpp
class Printable {
public:
    // prints the object to cout
    virtual void print() const = 0;

    // prints the object to cout followed by "\n"
    void println() const {
        print();
        cout << "\n";
    }

    ~Printable() {
        cout << "Printable destructor called\n";
    }

}; // class Printable
```

Now run this code again:

```cpp
Printable* p = new Point(3, 4);
p->println();
delete p;
```

It prints this:

```
Printable destructor called
(3, 4)
```

So the `Printable` [[destructor]] is called, but the `Print` [[destructor]] is not. And it is pretty weird that the [[destructor]] for `Printable` is called *before* `p->println()`?! The warning message we ignored from above, it says the we are doing something that will result in undefined behaviour:

```
warning: deleting object of abstract class type ‘Printable’ which has 
non-virtual destructor will cause undefined behaviour 
[-Wdelete-non-virtual-dtor]
delete p;
```

## Virtual Destructors
The solution to these problems is to add a [[virtual destructor]] to `Printable`:

```cpp
class Printable {
public:
    // prints the object to cout
    virtual void print() const = 0;

    // prints the object to cout followed by "\n"
    void println() const {
        print();
        cout << "\n";
    }

    // virtual destructor
    virtual ~Printable() { }

}; // class Printable
```

The keyword `virtual` means that methods in classes that extend `Printable` are allowed to provide their own [[destructor]] if they like.

Now this code compiles without warning:

```cpp
Printable* p = new Point(3, 4);
p->println();
delete p;
```

And prints this:

```
(3, 4)
(3, 4) destroyed
```

Which is exactly what we want!

In this course, we'll always include `virtual` destructors in a [[base class]] so that inheriting classes can, if they choose to, provide their own destructors.

To help with this in `g++`, we use the `-Wnon-virtual-dtor` option  so that `g++` will warn us when we've forgotten to make a [[destructor]] virtual.

Here is a [[class diagram]] of our final result:

![[printableClassDiagram.svg]]

## Some Terminology
Here is the `Printable` class we have settled on:

```cpp
class Printable {
public:
    // prints the object to cout
    virtual void print() const = 0;

    // prints the object to cout followed by "\n"
    void println() const {
        print();
        cout << "\n";
    }

    // virtual destructor
    virtual ~Printable() { }

}; // class Printable
```

`Printable` is an [[abstract class]] because not all of its methods are implemented, i.e. the `print` method has no body.

- `= 0` indicates a method is [[abstract method|abstract]]; such methods are also known as [[abstract method|pure virtual method]]
- a class is considered abstract if it has one, or more, abstract methods
- it's possible for an abstract class to have one, or more, methods that are non-abstract methods (as in `Printable` above)

We call `Printable` a [[base class]], because its main use is to have other
classes derive from it. For example, the `Point` class is said to **derive** from `Printable`:

```cpp
class Point : public Printable {
   // ...
};
```

There are many [[synonyms for inheritance]]. We can say that:
- `Point` is **derived** from `Printable`
- or that `Point` **inherits** from `Printable`
- or that `Point` is a **subclass** of `Printable`
- or that `Point` **extends** `Printable`
- or that `Point` is a **child** of `Printable`

We can also say that `Printable` is a **superclass** of `Point`, or that `Printable` is a **parent** class of `Point`.

C++ allows so-called [[multiple inheritance]], where a class can extend more than one class. However, in this course we will only deal with [[single inheritance]], where a class has exactly one parent class.

## The Idea of Binding
Consider a regular function that is not inside any class or struct:

```cpp
void hello() {          // hello is bound to its body at compile-time
    cout << "Hello!\n";
}
```

When C++ compiles this function, the name `hello` gets **bound to** (associated with) the function body **at compile-time**.

When you call `hello()`, the compiler knows exactly what code will be executed. A name is bound at compile time is an example of [[static binding]].

A [[virtual method]], such as `print` in `Printable` is *not* necessarily bound at compile time. It is possible that, at compile-time, there is no way for the compiler to know exactly which `print()` method is being called.

For example:

```cpp
Printable* p;
string s;
cin >> s;
if (s == "point") {
    p = new Point{2, 3};
} else {
    p = new Person{"Jill", 93};
}

p.print();  // Which print is called? Point's or Person's?
```

There is no way for the compiler to know exactly which `print` is being called in `p.print()` --- it depends on what the user types. It is only at run-time when C++ knows which `print()` to call.

We say that `print` is [[dynamic binding|dynamically bound]], i.e. `print` is associated with its body at run-time instead of compile-time.

[[Dynamic binding]] is one of the key features of object-oriented programming.

## Pointers to Objects
Since `Printable` is an [[abstract class]], it's impossible to create a `Printable` object:

```cpp
Printable p; // compiler error: print() not implemented
```

However, *pointers* of type `Printable*` can be created, and they are very useful.

```cpp
Printable* p1 = new Point{1, 2};
Printable* p2 = new Person{"Max", 2};
Printable* p3 = new Reading{"Black Rock", 41.5};

p1->println(); // (1, 2)
p2->println(); // Name: 'Max, Age: 2
p3->println(); // 41.5 degrees at Black Rock
```

The pointers `p1`, `p2`, and `p3` are all of type `Printable*`, but the *objects* they point to are all different types.

Suppose `p` is of type `Pointer*` and points to either a `Point`, `Person`, or `Reading` object (we don't know which). What does `p->print()` print? There's no way to know without knowing the type of the object being pointed to. We can't tell from `p->print()` alone what gets printed!

The fact that all the *pointers* are the same type means we can, for example, put the pointers in a `vector`:

```cpp
vector<Printable*> v = { new Point{1, 2},
                         new Person{"Max", 2},
                         new Reading{"Black Rock", 41.5}
                       };
for(Printable* p : v) {
    p->print();
    cout << "\n";
}

// ...
```

Each time `p->println()` is called, a different version of `print()` is called. Thanks to [[dynamic binding]], this works out: C++ knows that it will be able to figure out at run-time which `print()` implementation to call.

In general, if `p` is of type `Printable*` and pointing to an object, we don't know the exact type of the object `p` is pointing to. All we can be sure of is that we can call methods through `p` that are declared in the `Printable` class. Any class that inherits from `Printable` is guaranteed to have those methods as well. All objects that `p` points to are guaranteed to have `print()` and `println()`.

So, if `p` is of type `Printable*`, `p->print()` and `p->println()` are the only methods we can definitely call through `p`.

```cpp
Reading* r = new Reading{"Black Rock", 41.5};
r->println();          // okay
cout << r->get_temp()  // okay
     << "\n"; 

Printable* p = r;      // okay
p->println();          // okay
cout << p->get_temp()  // compile-time error: p is not guaranteed to point
     << "\n"; !        //                     to an object that has a
	                   //                     get_temp method
```

## Designing with Inheritance
[[Interface inheritance]] is a style of inheritance where you only inherit [[abstract method|abstract methods]]. This is in contrast to [[implementation inheritance]], where you inherit non-abstract methods that have a body.

Sometimes large object-oriented programs organize all classes into a [[class hierarchy]]. A [[class diagram]] is a convenient way to visualize such a hierarchy.

It's not always obvious how, or if, two seemingly related classes should inherit from each other. For example, suppose you have a class called `Circle` that represents circles, and a class called `Ellipse` that represents ellipses.

Should `Circle` inherit from `Ellipse`, or should `Ellipse` inherit from `Circle`?

You could say `Circle` should inherit from `Ellipse` because, mathematically, a circle is an ellipse, i.e. the set of circles is a subset of the set of ellipses. But this can lead to serious problems! If `Ellipse` has methods `set_height(h)` and `set_width(w)`, then you can write code like this:

```cpp
Circle c(10);  // a circle of radius 10
     
c.set_width(5);
c.set_height(15);  // ???: a circle's width and height can't be different!
```

This is inconsistent and confusing!

So maybe `Ellipse` should inherit from `Circle`. A reason for that is because a `Circle` stores one radius, and an `Ellipse` stores two radii, and so `Ellipse` extends `Circle` by adding one more radius. But this can also lead to serious problems! Consider this function:

```cpp
double area(Circle c) {
   return 3.14 * c.radius() * c.radius();
}
```

Since an  `Ellipse` inherits from `Circle, we can write code like this:

```cpp
Ellipse e(5, 10);  // an ellipse 5 wide, 10 high
cout << area(e);   // this can't return the correct area!
```

The `area` function only checks one radius, but to correctly calculate the area of an `Ellipse` you need two radii.

So it seems that `Circle` and `Ellipse` have no good way to be related by inheritance at all! Both ways of doing the inheritance can lead to serious problems.

## C++ and OOP
C++ was among the first popular mainstream languages to support OOP. Earlier languages used it extensively (e.g. [Simula 67](https://en.wikipedia.org/wiki/Simula), [SmallTalk](https://en.wikipedia.org/wiki/Smalltalk)), but they never became as popular as C++. 

Most modern programming languages support OOP, often with somewhat simplified [[syntax]] compared to C++. For example, Java renames `->` to `.` and supports interfaces, which are like C++ classes where all the methods are [[virtual method|virtual]] and [[abstract method|abstract]].

## Sample  Code for Inheritance
### Inheriting from a vector
- [[vector_inherit1_cpp|vector_inherit1.cpp]]: Creates a new class named `int_vec` that inherits from `vector<int>`.
- [[vector_inherit2_cpp|vector_inherit2.cpp]]: Adds a `sum` method to the `int_vec` class from [[vector_inherit1_cpp|vector_inherit1.cpp]].
- [[vector_inherit3_cpp|vector_inherit3.cpp]]: Adds a sort method to the `int_vec` class from [[vector_inherit2_cpp|vector_inherit2.cpp]].
- [[vector_inherit4_cpp|vector_inherit4.cpp]]: Adds a `name` member variable (and corresponding getter) to the `int_vec` class from [[vector_inherit2_cpp|vector_inherit2.cpp]].
- [[vector_inherit5_cpp|vector_inherit5.cpp]]: Adds `summarize` and `fancy_summarize` functions (not methods!) to [[vector_inherit4_cpp|vector_inherit4.cpp]].

### Inheriting from a Base Class
- [[printable1_cpp|printable1.cpp]]: Shows three separate classes (`Point`, `Person`, and `Reading`) that are *not* related through inheritance, but all have the same `println` method.
- [[printable2_cpp|printable2.cpp]]:  The three classes from [[printable1_cpp|printable1.cpp]] inherit `println` from the same base class `Printable`. `Printable` includes a virtual abstract method `print` that is necessary for `println` to be inherited.

### Virtual Destructors
- [[virtual_destructor_cpp|virtual_destructor.cpp]]: Provides sample code that demonstrates why virtual destructors are necessary. The makefile has some compiling options turned off so the incorrect code can run.

## Practice Questions
1. Add a `sort_decreasing()` method to `int_vec` that sorts all the elements of an `int_vec` into order from largest to smallest.
2. Add a default method to `int_vec` that initializes the name to `"<no name>"`.
3. In your own words, explain what inheritance is and how it works in C++.
4. In your own words, explain what [[public inheritance]] means in C++. What other kinds of inheritance are there in C++?
5. Suppose class `B` inherits from class `A`, and function `f` takes one object of class `A` as input. Will `f` always work if it's passed an object of class `B`? Justify your answer.
6. Suppose class `B` inherits from class `A`, and function `f` takes one object of class `B` as input. Will `f` always work if it's passed an object of class `A`? Justify your answer.
7. In your own words, describe what it means for a method to be [[virtual method|virtual]]. How does it differ from a non-virtual method?
8. What does  it mean when a method ends with `= 0`? When is such a method useful?
9. Why should destructors always be declared virtual in C++?
10. How can you tell if a class is abstract?
11. Is every base class also an abstract class?
12. In your own words, explain the difference between [[single inheritance]] and [[multiple inheritance]].
13. In your own words, describe the difference between [[static binding]] and [[dynamic binding]].
14. In your own words, describe the difference between [[implementation inheritance]] and [[interface inheritance]].
