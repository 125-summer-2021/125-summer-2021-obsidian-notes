#terminology

## Definition
A region of memory in a running C++ program that manages function calls, including parameters, return values, and local variables. The call stack is a [[stack data structure]]. 

See [[calling a function]] for details on how the call stack works.
