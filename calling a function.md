Here's a PDF file that contains a detailed example of [[Calling Functions in C++.pdf|calling functions with the call stack]].

## Introduction
It's useful to understand how C++ calls functions in a running program. Like most programming languages, C++ calls functions using a [[call stack]]. 

We'll use these functions as an example:

```cpp
int f(int a) {
	return a + 1;
}

int g(int a) {
	int n = a + 2;
	return 3*n;
}

int h(int a) {
	int x = f(a);
	int y = g(a);
	return x + y;
}
```

## The Stack Data Structure
C++ calls functions using a [[call stack]], which is a kind of stack data structure.

![[stack data structure]]

## The Call Stack
When a C++ program runs, it first sets aside a portion of its memory to store the [[call stack]]. *Calling* a function corresponds to *pushing* the function onto the [[call stack]]. When a function ends, it is *popped* from the [[call stack]].

For example, consider this function call (where `f` is the function from above):

```cpp
int f(int a) {      // definition of f
	return a + 1;
}

f(3);               // call of f
```

When `f(3)` is called, the following happens:
- `f(3)` is *pushed* onto the top of the call stack.
- The flow of control jumps to the body of `f`.
- The body of `f` is executed; the parameter `a` is stored on the stack with the function call, and `a` acts like a [[local variable]] inside of `f`.
- When the `return` statement is executed the functions ends, and `f(3)` is *popped* from the call stack; the value returned by the function is put on the top of the call stack.

When we say a function is pushed onto the call stack, a special function-call data structure, sometimes called a [[stack frame]], is what actually gets pushed. The [[stack frame]] contains all the information needed to run the function, and is also where the local variables for the function are stored.

For example, here is `g` from above:

```cpp
int g(int a) {       // definition of g
	int n = a + 2;
	return 3*n;
}
```

When we call `g(2)`, this happens:
- `g(2)` is *pushed* onto the top of the call stack.
- The flow of control jumps to the body of `g`.
- The body of `g` is executed; the parameter `a` is stored on the stack with the function call, and `a` acts like a [[local variable]] inside of `g`.
- [[Local variable]] `n` inside of `g` is also stored on the stack with the function call, i.e. it is created when the function is called.
- When the `return` statement is executed the functions ends, and `g(2)` is *popped* from the call stack, and the [[local variable]] `n` is removed when this happens. The value returned by the function is put on the top of the call stack.

An import property of a [[stack data structure|stack]] is that when items are pushed onto it, any items that are already there remain on the stack undisturbed. So when a function calls a function, the functions get stacked up on the call stack. 

To better understand function calling, it is useful to sketch the stack as the functions are called to see exactly how it changes.

## Example 1
Here is function `h` from above:

```cpp
int h(int a) {
	int x = f(a);
	int y = g(a);
	return x + y;
}
```

Notice that it calls both functions `f` and `g` inside of it. If we call `h(3)`, then `h(3)` gets push onto the stack:

```
|                |
|                |
|                |  
| h(3), x=?, y=? | <-- h(3) is on the top of the stack
|________________|  bottom of the stack
```

When the body of `h(3)` runs, it first reserves memory for the [[local variable|local variables]] `x` and `y`. It initializes`x` by calling `f(3)`, and, as with any function call, `f(3)` is pushed onto the stack:

```
|                |
|                |
| f(3)           | <-- f(3) is on the top of the stack 
| h(3), x=?, y=? | 
|________________|  bottom of the stack
```

`f(3)` evaluates to 4, and so when it's done it's popped from the stack and we can imagine that 4 is on the top of the stack:

```
|                |
|                |
| 4              | <-- 4 (return value of f(3)) is on the top
| h(3), x=?, y=? | 
|________________|  bottom of the stack
```

Now the 4 is removed and assigned to `x`:

```
|                |
|                |
|                | 
| h(3), x=4, y=? | <-- h(3) is on the top of the stack
|________________|  bottom of the stack
```

Next the initial value of `y` is computed by calling `g(3)`:

```
|                |
|                |
| g(3), n=6      | <-- g(3) is on the top of the stack 
| h(3), x=4, y=? | 
|________________|  bottom of the stack
```

Notice that even though `x` is there on the stack, C++ does *not* allow `g` to access local variables of different functions.  When `g(3)` finishes it is popped from the stack and its return  value, 15, is left in its place: 

```
|                |
|                |
| 15             | 
| h(3), x=4, y=? | <-- h(3) is on the top of the stack 
|________________|  bottom of the stack
```

Now `y` can be assigned its initial value:

```
|                |
|                |
|                | 
| h(3), x=4, y=15| <-- h(3) is on the top of the stack 
|________________|  bottom of the stack
```

Finally, `h` calculates `x + y`, and is popped from the stack. It's return value, 19, is left on top of the stack so the code that called `h(3)` can access it:

```
|                |
|                |
|                | 
| 19             | <-- 19 is the value returned by h(3) 
|________________|  bottom of the stack
```

Notice that when functions with [[local variable|local variables]] are popped from the [[call stack]], their local variables are popped as well. This is very useful: [[local variable|local variables]] are created when the function is called, and then *automatically* de-allocated when the function ends. 

## Example 2
Let's trace the function call `f(f(1))`. First, the inner function call `f(1)` is pushed onto the [[call stack]]:

```
|                |
|                |
|                | 
| f(1)           | <-- f(1) is on the top of the stack 
|________________|  bottom of the stack
```

It evaluates to 2:

```
|                |
|                |
|                | 
| 2              | <-- 2 is the value returned by f(1) 
|________________|  bottom of the stack
```

This 2 is the parameter to the out `f` call, and so now `f(2)` is pushed:

```
|                |
|                |
|                | 
| f(2)           | <-- f(1) is on the top of the stack 
|________________|  bottom of the stack
```

This evaluates to 3:

```
|                |
|                |
|                | 
| 3              | <-- f(1) is on the top of the stack 
|________________|  bottom of the stack
```

The returned 3 is now on the top of the stack so that whatever code called `f(f(1))` can access it.

One final detail: it is possible for there to be so many function calls that the [[call stack]] runs out of memory. This is known as a [[stack overflow]], and is an error that is often associated with recursive functions.

## Practice Questions
1. In your own words, explain the [[stack data structure]], and what pushing and popping mean. Use an example in your explanation.
2. For each of the following function calls, draw a sequence of diagrams that shows how the [[call stack]] changes:
	1. `f(2)`
	2. `g(1)`
	3. `h(0)`
	4. `h(h(0))`
3. What is a [[stack frame]]?
4. In your own words, explain how [[local variable|local variables]] are automatically allocated and de-allocated on the [[call stack]].
5. In your own words, describe how a [[stack overflow]] error could occur with the [[call stack]]. What do you think might happen if a running C++ program has a [[call stack]] overflow error?