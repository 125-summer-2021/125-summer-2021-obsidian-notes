## Question Solutions

For each of the following, draw a finite state machine that accepts just **non-empty** bit strings that match the description. Use as few states as you can. Also, write a regular expression that describes the same set of strings the finite machine accepts.

 1. Ends with 1.
    Answer: `(0|1)*1`
 2. All 0s.
    Answer: `0 | 0*` (spaces added for readability)
 3. Starts with 1.
    Answer: `1 | (0|1)*` (spaces added for readability)
 4. Starts and ends with 1.
    Answer: `1 | (1(0|1)*1)` (spaces added for readability)
 5. The first bit and the last bit are the same. For example, "10011" and
    "0" should be accepted, but not "01" or "101110".
    Answer: `(0|1) | (1(0|1)*1) | (0(0|1)*0)`  (spaces added for readability)
 6. All 0s, or all 1s.
    Answer: `(0|1) | (0*|1*)`  (spaces added for readability)
 7. Six bits, the first three are 0, and the last three are 1.
    Answer: `000111`


