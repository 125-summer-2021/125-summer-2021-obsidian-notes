WSL lets you run Linux on a Windows computer. Compared to a traditional virtual machine installation of Linux on Windows, WSL starts faster, and works with Windows files and folders without any extra setup.

WSL does **not** support graphical Linux apps. You can edit files using Windows apps, and then use the WSL command line when you need to run a Linux command.

The [VS Code text editor](https://code.visualstudio.com/) has good built-in support for WSL, so you might consider using that.

To get started with WSL, you can follow the [Windows Subsystem for Linux Installation Guide for Windows 10](https://docs.microsoft.com/en-us/windows/wsl/install-win10).
- [This video](https://www.youtube.com/watch?v=_fntjriRe48) might also help you get started. You should use WSL 2 if you can.
- You should also install the [Windows Terminal](https://docs.microsoft.com/en-us/windows/terminal/get-started) mentioned in the instructions.

## Getting to Your Files in WSL
On Windows, the name of the main disk drive is usually "C:", and in WSL the C: drive is in the folder `/mnt/c`.  Your desktop folder should be in `/mnt/c/Users/<loginname>`.

For example, if your Windows login name is `bgates`, then the shell command `cd /mnt/c/Users/bgates` should take you to your desktop folder. 

 Any other disks you have can be accessed similarly, e.g. if you have a D: drive it's at `/mnt/d`.

