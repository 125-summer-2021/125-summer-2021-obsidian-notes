Many standard C++ functions, especially in the [[standard template library|STL]], use asymmetric ranges to specify sub-sequences.

For example, $[0,4)$ is an asymmetric range that represents the integers starting at 0, and going up to, but not including, 4. In other words, $[0,4)=\{0,1,2,3\}$. Similarly:

- $[2,7)=\{2,3,4,5,6\}$
- $[-2,2)=\{-2,-1,0,1\}$
- $[6,8)=\{6,7\}$
- $[9,10)=\{9\}$
- $[12,12)=\{\}$

In general, $[begin,end)=\{begin, begin +1, begin+2, \ldots , end-2,end-1\}$.  For a vector `v`, an asymmetric range represents a contiguous sequence of elements in the vector:

![[arrayBeginEnd.svg]]

A nice feature of asymmetric ranges is that their size is easy to calculate:

- $[2,7)=\{2,3,4,5,6\}$ has size $7-2=5$.
- $[-2,2)=\{-2,-1,0,1\}$  has size $2-(-2)=4$.
- $[6,8)=\{6,7\}$ has size $8-6=2$.
- $[9,10)=\{9\}$ has size $10-9=1$.
- $[12,12)=\{\}$ has size $12-12=0$.

In general, $[begin,end)=\{begin, begin +1, begin+2, \ldots , end-2, end-1\}$ has size $end-begin$.