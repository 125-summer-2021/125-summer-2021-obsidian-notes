Large C++ programs are typically organized into multiple `.cpp` files, and each of those `.cpp` files is compiled separately. into separately compiled units. This makes programs more modular, which makes it easier for multiple programmers to work on the project. It also speeds up compiling because you only need to re-compile the file that changed. 

In this note, we'll see how to separately compile a class called `RGB_color` (for representing colors). 

See also [[splitting int_vec]] for another example of separate compilation.

## The General Idea
Large C++ programs consist of many functions, classes, structs, and variables. For example, suppose you are creating an interactive paint program. It might have a class for representing menus, a class for representing brushes, a class for representing colors, and so on. A big program could easily have dozens or hundreds of classes.

Imagine writing a program with 200 classes. Putting all those classes into a single `.cpp` file is *possible*, but not a good idea. If you change just one class, then you must re-compile the entire program. This can take substantial time and slow down development. 

Another issue is sharing code on projects that have more than one programmer.  How do they make sure they are not making changes overwrites someone else's code? What happens when two programmers make *different* changes to the same line of code? These are difficult issues that make using a single file practically impossible on teams with more than one programmer.

So we divide large programs into multiple files. Typically, we put groups of related classes and functions into their own `.cpp` file. This allows us to compile each `.cpp` file individually, and different programmers can agree to work on different files to help avoid editing conflicts. 

> **Note**: While having programmers work on different files certainly helps avoid editing conflicts, dealing with different versions of files is still a tricky issue. That's where version control tools like [git](https://git-scm.com/) can help. [Git](https://git-scm.com/) keeps track of all changes to files in a project, and can help track versions and prevent inconsistencies.

### Definitions and Declarations 
Once you've decided that your program should be split into multiple files, there are some technical details in C++ that make this tricky. 

C++ follows this rule:

> **Single definition rule** Every variable, function, class, struct, etc. must have *exactly one*  definition. 

For example:

```cpp
int a = 2;  // ok: definition of a
int b = 3;  // ok: definition of b
a = 4;      // ok: assigning a value to a
int a = 5;  // error: a is already defined and cannot be defined again
```

Similarly, functions must be **defined** exactly once:

```cpp
int f(int a, int b) {    // ok: definition of f
	return 2*a + b;
}

int g(int a, int b) {    // ok: definition of g
	return 2*a + b;
}

int f(int a, int b) {    // error: re-definition of f not allowed
	return 2*a + b;
}
```

This single definition rule can become a problem when you split your code into multiple files. You must be very careful to ensure that every variable, function, class, etc. is defined exactly once.

So, there is standard format for arranging  C++ code into multiple files. To be concrete, suppose we want to put the class `RGB_color` into its own `.cpp` file. That's easy to do with cut-and-paste, but we have to allow some way for other files to `#include` `RGB_color` if they want it. So we also create a [[header file]] file, i.e. a C++ file that contains just **declarations** (and *not* definitions) for `RGB_color`. The declarations tell other C++ code how to call/use the code, but declarations do *not* provide implementations.

For functions, declarations are just the header of the function without a body, e.g.:

```cpp
int f(int a, int b);     // declaration of f

int f(int a, int b) {    // ok: definition of f
	return 2*a + b;
}
```

It's okay for a C++ program to have multiple *declarations* of a variable, function, or a class. For every variable, function, and class there must be:
- **exactly one definition**.
- **one, or more, declarations**. Note that a definition also counts as a declaration (but declarations don't count as definitions).

> **Fact** In C++, definitions are also declarations. However, declarations need not be definitions.

This is an important distinction! In general, *definitions* go in `.cpp` files, and *declarations* go in `.h` files. 

So, for example, this code is allowed in C++:

```cpp
int f(int a, int b);     // ok: declaration of f
int f(int a, int b);     // ok: declaration of f

int f(int a, int b) {    // ok: definition of f
	return 2*a + b;
}

int f(int a, int b);     // ok: declaration of f
```

But code like this is will *not* compile:

```cpp
int f(int a, int b);     // ok: declaration of f

int f(int a, int b) {    // ok: definition of f
	return 2*a + b;
}

int f(int a, int b);     // ok: declaration of f

int f(int a, int b) {    // error: re-definition of f
	return 2*a + b;
}
```

The story is similar for classes. For example, here is a *definition* of the `RGB_color` class, and definitions  of all the methods in it:

```cpp
class RGB_color {   // definition of RGB_color
	int red = 0;
	int green = 0; 
	int blue = 0;
public:
	RGB_color() // default constructor
	{ }  // member initialization is used to set red, green, blue

	RGB_color(int r, int g, int b)
	: red(r), green(g), blue(b)
	{
		if (red < 0 || red > 255) cmpt::error("bad red value");
		if (green < 0 || green > 255) cmpt::error("bad green value");
		if (blue < 0 || blue > 255) cmpt::error("bad blue value");
	}

	RGB_color(const RGB_color& other)
	: RGB_color(other.red, other.green, other.blue)
	{ }

	int get_red() const { return red; }
	int get_green() const { return green; }
	int get_blue() const { return blue; }

	void invert() {
		red = 255 - red;
		green = 255 - green;
		blue = 255 - blue;
	}
}; // class RGB_color
```

You can tell it's a definition because the methods *inside* the class have bodies. 

In contrast, here is a *definition* of the class `RGB_color`, but with *declarations* of the individual methods:

```cpp
class RGB_color {   // definition of RGB_color (the class, but not the methods)
	int red = 0;
	int green = 0; 
	int blue = 0;
public:
	RGB_color();                         // methods of RGB_color
	RGB_color(int r, int g, int b);      // are declared, not defined
	RGB_color(const RGB_color& other);

	int get_red() const;
	int get_green() const;
	int get_blue() const;

	void invert();
}; // class RGB_color
```

None of the methods have bodies, just headers. This is enough to be able to create and call `RGB_color` objects. Note that the private variables are included inside the class.

You can *define* the methods of `RGB_color` afterwards like this:

```cpp
class RGB_color {
	// ...
}; 

RGB_color::RGB_color() // default constructor
{ }  // member initialization is used to set red, green, blue

RGB_color::RGB_color(int r, int g, int b)
: red(r), green(g), blue(b)
{
	if (red < 0 || red > 255) cmpt::error("bad red value");
	if (green < 0 || green > 255) cmpt::error("bad green value");
	if (blue < 0 || blue > 255) cmpt::error("bad blue value");
}

RGB_color::RGB_color(const RGB_color& other)
: RGB_color(other.red, other.green, other.blue)
{ }

int RGB_color::get_red() const   { return red;   }
int RGB_color::get_green() const { return green; }
int RGB_color::get_blue() const  { return blue;  }

void RGB_color::invert() {
	red   = 255 - red;
	green = 255 - green;
	blue  = 255 - blue;
}
```

Note that `RGB_color::` is at the front of all the method names: this is how C++ knows they belong to `RGB_color`.

When putting `RGB_color` its own files, you should put *declarations* in `RGB_color.h`, and all the definitions in `RGB_color.cpp`. Code that *uses* `RBG_color` must `#include` the `.h` file, e.g.:

```cpp
#include "RGB_color.h"
#include <iostream>

using namespace std;

int main() {
	RGB_color bg(200, 100, 255);
	cout << "bg=" << bg << "\n";
	
	RGB_color fill(55, 155, 200);
	cout << "fill=" << fill << "\n";

	double d = dist(bg, fill);
	cout << "d=" << d << "\n";
}
```

Compared to more modern programming languages, this is a lot of work just to get separately compiled files. But it works, and has stood the test of time.

> **Note** C++20 has added **modules** to C++, which will hopefully simplify and improve how separate compilation works. But at the time of writing this note (November 2020) modules are not yet widely available in C++ compilers.

## Example: Creating a Separately Compiled File
In what follows we show how to convert a file in a single class into a separately compileable file. All the code discussed below is in [this repository](https://csil-git1.cs.surrey.sfu.ca/cmpt135/cmpt135_fall2020/cmpt-135-notes/-/tree/master/separate_compilation/rgb_color).

### Version 1: All In One File
In [RGB_color_allInOne.cpp], everything is in one file. The class and function definitions, as well as the `main()` function appear in the same file. 

To run this code, type this command (make sure the course makefile is in the same folder):

```bash
$ make RGB_color_allInOne
```

This creates an executable file named `RGB_color_allInOne` that run like
this:

```bash
$ ./RGB_color_allInOne
```

The good thing about this approach is that it is simple. However, it doesn't *scale*: if your program gets bigger, or more programmers get involved, then putting all the code into one file is impractical and inefficient.

### Version 2: All in One File: Methods Outside the Class
See [RGB_color_split.cpp].

[RGB_color_split.cpp] stills puts *everything* into a single `.cpp` file, but with one important change: the bodies of the methods in `RGB_color` are defined *outside* the class instead of inside the class. In [RGB_color_split.cpp] the `RGB_Color` class looks like this:

```cpp
class RGB_color {
	int red = 0;
	int green = 0; 
	int blue = 0;
public:
	RGB_color();
	RGB_color(int r, int g, int b);
	RGB_color(const RGB_color& other);

	int get_red() const;
	int get_green() const;
	int get_blue() const;

	void invert();
}; // class RGB_color
```

The importance of this step is that it prepares for the step that follows, where we put the class and function headers (declarations) into one file, and the their implementations (definitions) into another.

To run this code, type this command:

```bash
$ make RGB_color_split
```

This creates an executable file named `RGB_color_allInOne` that run like this:
```bash
$ ./RGB_color_split
```

### Version 3: A Header and an Implementation
See [RGB_color.h], [RGB_color.cpp], and [color_test.cpp].

This version re-arranges the contents of [RGB_color_split.cpp] into three separate files:
- [RGB_color.h] contains the class and functions headers (i.e. the declarations). Notice that [[include guards]] are used to with this file to prevent multiple inclusion.
- [RGB_color.cpp] contains the implementations (i.e. the definitions) of the class and functions that appear in [RGB_color.h]. Notice that this files includes [RGB_color.h]. It does *not* contain a `main()` function.
- [color_test.cpp] contains the `main()` function, and includes [RGB_color.h].

Now we have two `.cpp` files, and both of them need to be compiled using `g++ -c`:

```bash
$ g++ -c RGB_color.cpp
$ g++ -c color_test.cpp
```

After these files are compile, the files `RGB_color.o` and `color_test.o` should exist.

This creates a `.o` object file named `RGB_color.o`. Since there is no `main` function, we can't create an executable file.

Now we can [[linker|link]] them together using `g++ -o` to make the final executable:

```bash
$ g++ -o color_test color_test.o RGB_color.o
```

This creates the executable file `color_test` which can be run like this:

```bash
$ ./color_test
```

This diagram shows how all the files relate:

![[RGB_color_compile.svg]]

## Practice Questions
1. In your own words, why is it usually a good idea to split a large C++ program into separately compiled files?
2. What is the single definition rule in C++? What is the difference between a definition and a declaration?
3. *True* or *false*: every C++ definition is also a declaration.
4. *True* or *false*: every C++ declaration is also a definition.
5. What is the `g++` command for *just* compiling a file?
6. What is the `g++` command for *linking* object files?

[RGB_color_allInOne.cpp]: https://csil-git1.cs.surrey.sfu.ca/cmpt135/cmpt135_fall2020/cmpt-135-notes/-/blob/master/separate_compilation/rgb_color/RGB_color_allInOne.cpp
[RGB_color_split.cpp]: https://csil-git1.cs.surrey.sfu.ca/cmpt135/cmpt135_fall2020/cmpt-135-notes/-/blob/master/separate_compilation/rgb_color/RGB_color_split.cpp
[RGB_color.h]: https://csil-git1.cs.surrey.sfu.ca/cmpt135/cmpt135_fall2020/cmpt-135-notes/-/blob/master/separate_compilation/rgb_color/RGB_color.h
[RGB_color.cpp]: https://csil-git1.cs.surrey.sfu.ca/cmpt135/cmpt135_fall2020/cmpt-135-notes/-/blob/master/separate_compilation/rgb_color/RGB_color.cpp
[color_test.cpp]: https://csil-git1.cs.surrey.sfu.ca/cmpt135/cmpt135_fall2020/cmpt-135-notes/-/blob/master/separate_compilation/rgb_color/color_test.cpp