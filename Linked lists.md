Linked lists are a dynamic data structure that can be implemented using nodes and pointers, and without using arrays. Compared to arrays or vectors, some linked list operations are more efficient. For example, adding or removing an item at the start of a linked list is an extremely efficient operation that runs at the same speed no matter the size of the list. This is in comparisons to arrays, where adding/removing the first element is an expensive operation  that requires re-locating all subsequent elements of  the array.

However, some operations on a linked list are less efficient than corresponding array operations. For example, to get the 100th element of the array `arr`, the expression `arr[i]` will return it almost instantly. But to find the 100th element of a linked list, you must start at the first element and count 99 elements afterwards.

In practice, while linked lists do have some applications, arrays and vectors tend to be more popular.

The linked lists we discuss here are called **singly-linked lists**, where each element in the list is linked by one pointer to the next element. There are many variations on this basic idea. For example, each element of a **double-linked lists** is linked to the previous element and the next element. This makes certain operations much easier as compared to a singly-linked list, but the implementation is more complicated.

## Linked List Code
In the examples that follow, a singly-linked list implementation is developed starting with a basic implementation using pointers, and ending with an object-oriented version with a number of features. 

### Example 1: A Singly Linked List in main()
This program demonstrates the basic idea of a singly-linked list of `int`s using a `Node` struct and a pointer called `head` that points to the first element of  the list. C

It demonstrates how to add 3 numbers numbers at the start, and then removes 3 numbers from the start.

```cpp
// linked_list1.cpp

#include <iostream>

using namespace std;

struct Node {
	int   data;
	Node* next;
};

int main() {
	//
	// initialize the head pointer
	//
	Node* head = nullptr;

	//
    // add the numbers 1, 2, 3 to the start of list
	//
	head = new Node{1, head};
	head = new Node{2, head};
	head = new Node{3, head};

	//
	// print the values of the list
	//
	cout << head->data             << "\n"; // 3
	cout << head->next->data       << "\n"; // 2
	cout << head->next->next->data << "\n"; // 1

    //
    // print the values of the list using a pointer
    //
    cout << "\n";

    Node* p = head;
   
    cout << p->data << "\n";
    p = p->next;

    cout << p->data << "\n";
    p = p->next;
    
    cout << p->data << "\n";
    p = p->next;  
    // p == nullptr

    //
    // print the values of the list using a loop
    //
    cout << "\n";
    p = head;
    while (p != nullptr) {
	    cout << p->data << "\n";
	    p = p->next; 
    }

	//
	// remove the elements one at a time
	//
	Node* temp;

	temp = head->next;
	delete head;
	head = temp;

	temp = head->next;
	delete head;
	head = temp;

	temp = head->next;
	delete head;
	head = temp;
}
```

### Example 2: A Singly Linked List with Helper Functions
This program is based on the previous program, and uses functions for adding and removing the first element, and for printing items on the list.

```cpp
// linked_list2.cpp

#include <iostream>
#include "cmpt_error.h"

using namespace std;

struct Node {
	int   data;
	Node* next;
};

// Notice that head is a pointer that is passed by reference.
void add_front(Node*& head, int data) {
	head = new Node{data, head};
}

int remove_front(Node*& head) {
	if (head == nullptr) cmpt::error("remove_front: empty list");
	int result = head->data;
	Node* temp = head->next;
	delete head;
	head = temp;
	return result;
}

void print(Node* head) {
	while (head != nullptr) {
		cout << head->data << "\n";
		head = head->next;
	}
}

int main() {
    // add the numbers 1, 2, 3 to the front of the list
	Node* head = nullptr;
	add_front(head, 1);
	add_front(head, 2);
	add_front(head, 3);

	// print the values of the list
	print(head);

	// remove elements from the front of the list
	remove_front(head);
	remove_front(head);
	remove_front(head);
}
```

### Example 3: A Singly Linked List Class
Based on the previous program, this version but puts `head`, the `Node` struct, and the functions in a class. In addition, it includes the method `is_empty()` that returns true if the list is empty (i.e. `head == nullptr`), and `false` otherwise.

An interesting detail is that the `Node` struct is made private in `List`, so the user of `List` doesn't need to know anything about nodes. From the point of view of the user, `List` is just a container of `int`s.

```cpp
// linked_list3.cpp

#include "cmpt_error.h"
#include <iostream>

using namespace std;

class List {
	struct Node {    // The user of List does not need to know about
		int   data;  // the existence of the Node struct, so we make it
		Node* next;  // private inside List.
	};

	Node* head;

public:
	// default constructor
	List()
	: head(nullptr)   // initialization list
	{ }

	// destructor: deletes all elements of the list
	~List() {
		while (!is_empty()) {
			remove_front();
		}
	}

	bool is_empty() const { 
		return head == nullptr;
	}

	void add_front(int data) {
		head = new Node{data, head};
	}

	int remove_front() {
		if (is_empty()) cmpt::error("remove_front: empty list");
		int result = head->data;
		Node* temp = head->next;
		delete head;
		head = temp;
		return result;
	}

	void print() const {
		Node* p = head;
		while (p != nullptr) {
			cout << p->data << "\n";
			p = p->next;
		}
	}
}; // class List

int main() {
    // add the numbers 1, 2, 3 to the list
    List a;
    a.add_front(1);
    a.add_front(2);
    a.add_front(3);

	// print the values of the list
	a.print();

	// // remove the elements one at a time
	// a.remove_front();
	// a.remove_front();
	// a.remove_front();
}
```

### Example 4: A Singly Linked List Class with More Features
Based on the previous version, this one adds a few more convenient methods:

- `first()`, returns the first number in the list
- `size()`, returns the number of elements in the list
- `sum()`, returns the sum of the numbers in the list
- `contains(n)`, returns true if n appears in the list, and false  otherwise
- `last()`, returns the last number in the list
- `operator==(a,b)`, a function that returns true when the lists `a` and `b` have the same elements in the same order
  
```cpp
// linked_list4.cpp

#include "cmpt_error.h"
#include <iostream>
#include <cassert>

using namespace std;

class List {
	struct Node {
		int data;
		Node* next;
	};

	Node* head;

public:
	// default constructor
	List()
	: head(nullptr)   // initialization list
	{ }

	// destructor: deletes all elements of the list
	~List() {
		while (!is_empty()) {
			// cout << "~List: removing " << remove_front() << "\n";
			remove_front();
		}
	}

	bool is_empty() const { 
		return head == nullptr;
	}

	int first() const { 
		if (is_empty()) cmpt::error("first: empty list");
		return head->data;
	}

	void add_front(int data) {
		head = new Node{data, head};
	}

	int remove_front() {
		if (is_empty()) cmpt::error("remove_front: empty list");
		int result = head->data;
		Node* temp = head->next;
		delete head;
		head = temp;
		return result;
	}

	void print() const {
		Node* p = head;
		while (p != nullptr) {
			cout << p->data << "\n";
			p = p->next;
		}
	}

	int size() const {
		int result = 0;
		Node* p = head;
		while (p != nullptr) {
			result++;
			p = p->next;
		}
		return result;
	}

	int sum() const {
		int result = 0;
		Node* p = head;
		while (p != nullptr) {
			result += p->data;
			p = p->next;
		}
		return result;
	}

	bool contains(int n) const {
		Node* p = head;
		while (p != nullptr) {
			if (p->data == n) return true;
			p = p->next;
		}
		return false;
	}

	int last() const {
		if (is_empty()) cmpt::error("last: empty list");
		Node* p = head;
		while (p->next != nullptr) {
			p = p->next;		
		}
		return p->data;
	}

	friend bool operator==(const List& a, const List& b);
}; // class List

// Note: It would cause a memory error if a and b were passed by value 
// here becauses the head pointers to the start of underlying list would 
// be copied, but none of the nodes would be copied. The nodes deleted 
// by the destructor would be the same nodes as the passed-in nodes, 
// resulting a double-deletion memory error.
bool operator==(const List& a, const List& b) {
	if (a.size() != b.size()) return false;
	assert(a.size() == b.size());
	List::Node* ap = a.head;
	List::Node* bp = b.head;
	while (ap != nullptr) {
		assert(bp != nullptr);
		if (ap->data != bp->data) return false;
		ap = ap->next;
		bp = bp->next;
	}
	return true;
}

int main() {
    // add the numbers 1, 2, 3 to the list
    List a;
    a.add_front(1);
    a.add_front(2);
    a.add_front(3);

	// print the values of the list
	a.print();
	cout << "      First: " << a.first()     << "\n";
	cout << "       Size: " << a.size()      << "\n";
	cout << "        Sum: " << a.sum()       << "\n";
	cout << " contains 3: " << a.contains(3) << "\n";
	cout << " contains 4: " << a.contains(4) << "\n";
	cout << "       Last: " << a.last()      << "\n";
	cout << "     a == a: " << (a == a)        << "\n";

	List b;
	cout << "     a == b: " << (a == b) << "\n";
    b.add_front(1);
    cout << "     a == b: " << (a == b) << "\n";
    b.add_front(2);
    cout << "     a == b: " << (a == b) << "\n";
    b.add_front(3);
    cout << "     a == b: " << (a == b) << "\n";
}
```

## Example 5: Recursive Methods
Based on the previous program, but all the methods that used loops now use recursion instead. In each case a private helper method was created that takes one extra pointer as a parameter. The corresponding public method calls the private helper method.

It also includes some `assert`-based tests to check for correctness.

Many programmers feel that recursive functions are more elegant than non-recursive ones. A lot of the recursive methods below are short and simple. This makes it easier to read those methods, and can give us more confidence that they're correct. However, we need a private version of each method to handle the extra pointers, and in cases where the recursive call is *not* the final action of the function, the recursive method is usually more costly in terms of time and memory than the non-recursive versions.

```cpp
// linked_list5.cpp

#include "cmpt_error.h"
#include <iostream>
#include <cassert>

using namespace std;

class List {
	struct Node {
		int data;
		Node* next;
	};

	Node* head;

	// returns the number of nodes from p to the end of the list
	int size(Node* p) const {
		if (p == nullptr) {
			return 0;
		} else {
			return 1 + size(p->next);
		}
	}

	// prints the number of nodes from p to the end of the list
	void print(Node* p) const {
		if (p != nullptr) {
			cout << p->data << "\n";
			print(p->next);
		}
	}

	// returns the sum of node values from p to the end of the list
	int sum(Node* p) const {
		if (p == nullptr) {
			return 0;
		} else {
			return p->data + sum(p->next);
		}
	}

	// returns the value on the last node of the list that p points to
	int last(Node* p) const {
		if (p == nullptr) cmpt::error("last: nullptr");
		if (p->next == nullptr) {
			return p->data;
		} else {
			return last(p->next);
		}
	}

    // returns true if a node on the list p points to contains n as a value,
    // and false otherwise
	bool contains(Node* p, int n) const {
		if (p == nullptr) {
			return false;
		} else if (p->data == n) {
			return true;
		} else {
			return contains(p->next, n);
		}
	}

public:
	// default constructor
	List()
	: head(nullptr)   // initialization list
	{ }

	// destructor: deletes all elements of the list
	~List() {
		while (!is_empty()) {
			// cout << "~List: removing " << remove_front() << "\n";
			remove_front();
		}
	}

	bool is_empty() const { 
		return head == nullptr;
	}

	int first() const { 
		if (is_empty()) cmpt::error("first: empty list");
		return head->data;
	}

	void add_front(int data) {
		head = new Node{data, head};
	}

	int remove_front() {
		if (is_empty()) cmpt::error("remove_front: empty list");
		int result = head->data;
		Node* temp = head->next;
		delete head;
		head = temp;
		return result;
	}

	void print() const {
		print(head);
	}

	int size() const {
		return size(head);
	}

	int sum() const {
		return sum(head);
	}

	bool contains(int n) const {
		return contains(head, n);
	}

	int last() const {
		if (is_empty()) cmpt::error("last: empty list");
		return last(head);
	}

	friend bool operator==(const List& a, const List& b);
}; // class List

// Note: It would cause a memory error if a and b were passed by value here
// becauses the head pointers to the start of the underlying lists would be
// copied, but none of the nodes would be copied. The nodes deleted by the
// destructor would be the same nodes as the passed-in nodes, resulting a
// double-deletion memory error.
bool operator==(const List& a, const List& b) {
	if (a.size() != b.size()) return false;
	assert(a.size() == b.size());
	List::Node* ap = a.head;
	List::Node* bp = b.head;
	while (ap != nullptr) {
		assert(bp != nullptr);
		if (ap->data != bp->data) return false;
		ap = ap->next;
		bp = bp->next;
	}
	return true;
}

bool operator!=(const List& a, const List& b) {
	return !(a == b);
}

int main() {
    // add the numbers 1, 2, 3 to the list
    List a;
    assert(a.size() == 0);
    assert(a.sum() == 0);
    assert(!a.contains(5));
    assert(a == a);

    a.add_front(1);
    assert(a.size() == 1);
    assert(a.first() == 1);
    assert(a.sum() == 1);
    assert(a.contains(1));
    assert(!a.contains(5));
    assert(a.last() == 1);
    assert(a == a);

    a.add_front(2);
    assert(a.size() == 2);
    assert(a.first() == 2);
    assert(a.sum() == 3);
    assert(a.contains(1));
    assert(a.contains(2));
    assert(!a.contains(5));
    assert(a.last() == 1);
    assert(a == a);

    a.add_front(3);
    assert(a.size() == 3);
    assert(a.first() == 3);
    cout << "a.sum: " << a.sum() << "\n";
    a.print();
    assert(a.sum() == 6);
    assert(a.contains(1));
    assert(a.contains(2));
    assert(a.contains(3));
    assert(!a.contains(5));
    assert(a.last() == 1);
    assert(a == a);

	// print the values of the list
	a.print();
	cout << "      First: " << a.first()     << "\n";
	cout << "       Size: " << a.size()      << "\n";
	cout << "        Sum: " << a.sum()       << "\n";
	cout << " contains 3: " << a.contains(3) << "\n";
	cout << " contains 4: " << a.contains(4) << "\n";
	cout << "       Last: " << a.last()      << "\n";
	cout << "     a == a: " << (a == a)      << "\n";

	List b;
	assert(a != b);
	cout << "     a == b: " << (a == b) << "\n";
    
    b.add_front(1);
    assert(a != b);
    cout << "     a == b: " << (a == b) << "\n";

    b.add_front(2);
    assert(a != b);
    cout << "     a == b: " << (a == b) << "\n";
    b.add_front(3);
    assert(a == b);
    cout << "     a == b: " << (a == b) << "\n";
}
```

## Practice Questions
1. Add to `linked_list4.cpp` a method called `double average(int i) const` that returns the average of the elements in the list. If the list is empty, throw an error using `cmpt::error`.
2. Add a **copy constructor** to `List`.
3. Add to `linked_list4.cpp` a **getter** called `int get(int i) const` that returns the `int` at index location `i` of  the list. The indexing starts at 0, i.e. the value if the first node is `get(0)`, and the value of the second node is `get(1)`, and so on. Do bounds checking on `i`: If `i` is less than 0, or equal to `size()` or more, throw an error using `cmpt::error`.
4. Add to `linked_list4.cpp` a **setter** called `int set(int i, int val)` that sets the `int` at index location `i` to be `val`. As in the previous question, indexing starts at 0, and you should do bounds checking on `i`.
5. Modify `linked_list4.cpp` so that a `List` stores its size as a private `int` variable. Rewrite `size()` so it returns this variable (instead of using a loop to count the number of nodes). Make sure that all methods that modify the size of the list correctly update the new size variables.