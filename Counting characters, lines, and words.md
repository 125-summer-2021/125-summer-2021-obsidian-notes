The standard Linux utility `wc` counts characters, lines, and words in a file. For example:

```
$ wc austenPandP.txt
 13427 124580 704158 austenPandP.txt
```

The file **austenPandP.txt** has 13427 lines, 124580 words, and 704158 characters. 

![[austenPandP.txt]]

Create your own version of `wc` following the steps below.

1. Count the number of characters in a file. **Hint**: `cin.get(c)` sets the char `c` to the next character on `cin`. After calling `cin.get(c)`, `cin.eof()` will return `true` if `get` read the end-of-file (eof) character.
    
	You don't need to use any special file processing in your program. Instead, use file re-direction in the shell to process a file like this:
	
  ```
  $ ./count < somefile.txt
  #chars: 704158
  #lines: 13427
   #tabs: 0
  #words: 124580
  ```

2. Count the number of lines in a file. For simplicity, we define a line to be an occurrence of a `'\n'` (newline) character.

3. Count the number of `'\t'` (tab) characters in a file. Use a `switch` statement to check for the newlines and tabs.

4. Count the number of words in a file. To simplify this, just count the number of substrings consisting of one or more whitespace characters. Whitespace characters include `' '` (space), `'\n'` (newline), and `'\t'` (tab).