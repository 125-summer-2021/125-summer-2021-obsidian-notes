## Pointers
Managing memory is one of the trickiest and most important parts of C++ programming. It is done using pointers, and so in this note we will discuss the basics of using pointers in C++

A running C++ program has a relatively simple model of memory. You can think of the memory a running C++ program can access as one long array of bytes:

```
address   0   1   2   3   4   5   6                    N-2 N-1  
        +---+---+---+---+---+---+---+     +---+---+---+---+---+
value   |   |   |   |'a'|   |   |   | ... |   |   |   |   |   |
        +---+---+---+---+---+---+---+     +---+---+---+---+---+
```

Here, `N` is the amount of memory (in bytes) available to your C++ program. Every value in a C++ program is at some address, e.g. the character `a` is at address 3.

It's easy to get the address of a variable in C++ using `&`:

```cpp
char x = 'a';
cout << x;  // prints 'a'
cout << &x; // prints the address of 'a' (in this case 3)
```

`&` is the [[address-of operator]], and returns the memory address of a variable. In general, we don't know at what address a C++ value will be stored at until the program runs. There's also no guarantee that the same address location will be used in different runs of the program. So programs should *not* depend upon being at a specific address value.

In C++, addresses are stored in [[pointer|pointers]]. For example, the address of a `char` is of type `char*`, i.e. `char*` is the type "pointer to a `char`".

We can write code like this:

```cpp
char x   = 'a';
char* px = &x;  // px stores the address of x
                // i.e. px points to x
```

In memory, it might look like this:

```
              408                   951     
     +---+---+---+---+---+     +---+---+---+---+
 ... |   |   |'a'|   |   | ... |   |408|   |   | ...
     +---+---+---+---+---+     +---+---+---+---+
               x                    px      
              *px
```

`x` has address 408, i.e. `&x == 408` in this example (in general, we won't know the exact address).

`px` has address 951, i.e. `&px = 951` in this example (in general, we won't know the exact address). The value in `px` is the address of `x`, so we say `px` **points to** `x`.

Since `px` stores the address of `x`, we can access the value it points to using the [[dereferencing operator]], `*`. C++ uses the notation `*px` to access the value at `x`.

So in this example, address 408 has two names: `x` or `*px`, and you can read/write the value there using either name, e.g.:

```cpp
char x = 'a';
char* px = &x;

x = 'b';
cout << *px; // prints b

*px = 'c';
cout << x;  // prints c
```

## Null Pointers
Any pointer variable can be assigned the special value `nullptr`, e.g.:

```cpp
string* a = nullptr;
char*   b = nullptr;
double* c = nullptr;
```

Intuitively, a pointer with the value `nullptr` points nowhere, i.e. it doesn't point to a valid memory address. You should *never* de-reference a [[null pointer]], i.e. `*a` is an error if `a == nullptr`.

Because of this, you always need to be certain that a pointers value is *not* `nullptr` before you de-reference it. Code that looks like this is common with pointers:

```cpp
if (a == nullptr) {
    cmpt::error("error: null pointer");
} else {
    cout << *a << "\n";
}
```

Note that C++ *references* can never have a null value. For example, if variable `x` has type `int&` (reference to `int`), then `x` is guaranteed to contain an `int`.

## Pointers to Pointers
C++ lets you make a pointer to *any* type of variable, even other pointer types. For example:

```cpp
#include <iostream>

using namespace std;

int main() {
	int n     = 5;
	int* pn   = &n;  // pn points to n
	int** ppn = &pn; // ppn points to pn
	cout << "    n: " << n     << "\n";
	cout << "  *pn: " << *pn   << "\n";
	cout << "**ppn: " << **ppn << "\n";
}
```

In this code, `p` is of type `int` and holds the value 5. `pn` is a pointer and points to `n`, and so `*pn` is 5 as well. Finally, `ppn` points to `pn`. Since `pn` has type `int*`, a pointer to it has type `int**`. The expression `*ppn` is equal to the value of `pn`, i.e. `*ppn == pn`. Thus `*ppn` is of type `int*` and holds the address of `n`, so `**ppn` evaluates to 5, i.e. the value of `n`.

Pointers to pointers can get trick, and so it can be useful to draw diagrams to help keep track of what is pointing where.

## Pointer Arithmetic
It is sometimes useful to do arithmetic with pointers. For example:

```cpp
#include <iostream>

using namespace std;

int main() {
	int arr[] = {3, 2, 1, 5};
	int* begin = arr;
	int* end = arr + 4;

	cout << "  1st of arr: " << *begin   
	     << "\n";
	cout << "  2nd of arr: " << *(begin+1) 
	     << "\n";
	cout << "Last of arr : " << *(end-1) // -1 is important!
	     << "\n";
	cout << "Size of arr : " << end - begin
	     << "\n";
}
```

This creates an array of 4 `int`s named `arr`. `begin` points to the first element, and `end` points to *one past* the last element. This gives us this information:
- `*begin` is the value of the first element of `arr`
- `*(end-1)` is the value of the last element of `arr`. It's *not* `*end` because we defined `end` to be *one `int` past* that element of `arr`. In fact, `*end` causes undefined behaviour. Evaluating `*end` could, for example, crash your program, or it might just return some junk value.
- `end - begin` is the number of elements in `arr`. This expression is correct because `begin` points to the first element of `arr`, and `end` points to one past the last element.

In this example,  a pointer is used to iterate through an array:

```cpp
#include <iostream>

using namespace std;

int main() {
	int arr[] = {3, 2, 1, 5};

	for(int* p = arr; p < arr + 4; p++) {
		cout << *p << "\n";
	}
}
```

This style of looping can take some getting use to, but you comfortable with it is a nice way to write loops. Writing `*p` instead of `arr[i]` makes for less cluttered code.

## Practice Questions
1. What is the [[address-of operator]] and the [[dereferencing operator]]? In your own words, describe what they do, and given an example of how they can be used.
2. What is the type of a pointer to a `double`? What is the type of a pointer to a `char*`
3. What is a [[null pointer]]? What is its purpose? What happens if a program tries to de-reference a null pointer?
4. What is the type of a pointer to a `char*`?
5. Suppose `arr` is an array and `begin` points to its first element, and `end` points to its last element. How many elements are in `arr`?
6. Re-write the for-loop pointer example from the notes as a while-loop.