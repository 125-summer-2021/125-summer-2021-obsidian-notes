## Introduction
[[O-notation]] is a useful way to classify algorithms according to their
running-time. For example, here are 7 common performance classifications of algorithms:

| O-expression  | Name         | Example                   |
|---------------|--------------|---------------------------|
| $O(1)$        | constant     | $1, 10, 34, 3538, \ldots$ |
| $O(\log n)$   | logarithmic  | $8\,\log_2 n + 5$         |
| $O(n)$        | linear       | $29n+5, n-9000$           |
| $O(n \log n)$ | linearithmic | $n\,\log_3 n + 23n - 4$   |
| $O(n^2)$      | quadratic    | $7n^2 - 4n + 22$          |
| $O(n^3)$      | cubic        | $n^3 + 2n^2 + 22n + 1$    |
| $O(2^n)$      | exponential  | $3^n + n^4 - 2n^2 + 80$   |

There is another classification that is of great interest in theoretical computer science: *exponential running time or worse*, or *less than exponential running time*. We'll call an algorithm whose worst-case running time is less than exponential **fast**, and an algorithm whose worst-case running time is exponential (or worse) as **slow**.

|  O-expression |     Name     | Classification |
|:-------------:|:------------:|----------------|
|     $O(1)$    |   constant   |      fast      |
|  $O(\log n)$  |  logarithmic |      fast      |
|     $O(n)$    |    linear    |      fast      |
| $O(n \log n)$ | linearithmic |      fast      |
|    $O(n^2)$   |   quadratic  |      fast      |
|    $O(n^3)$   |     cubic    |      fast      |
|                                                                                                     |
|    $O(2^n)$   |  exponential |      slow      |


Problems that can only be solved by a slow algorithm are called **hard** problems.  Problems that can be solved by a *fast* algorithm is called an **easy** problem.

For example, algorithms like [[insertion sort]], [[linear search]], [[binary search]], and finding [[the max function|the max of an vector]] all have a worst-case running time of $O(p(n))$ where $p(n)$ is a *polynomial* in $n$. Since polynomial running-times are less than exponential running-times, these are all **fast** algorithms, and the corresponding problems they solve are **easy** problems.

Some problems are fundamentally hard, and require exponential time, or worse. For example:

- printing all bit-strings of length $n$ is $O(2^n)$, because there are $2^n$ bit strings of length $n$
- printing all permutations (i.e. arrangements) of $n$ different numbers is $O(n!)$, because $n$ different numbers can be arranged in
  exactly $n!=1 \cdot 2 \cdot 3 \cdot \ldots \cdot n$ ways.

It is possible for an easy problem to be solved by a slow algorithm. For example, the sorting problem is easy because it can be solved in less than exponential time by [[insertion sort]] and [[mergesort]]. But it can also be solved using an algorithm called **permutation sort**: one at a time, generate all possible permutations of the vector you are sorting, and stop when all the elements are in [[sorted order]]. This requires checking $O(n!)$ different permutations in the worst case, and this is a slow algorithm because $O(n!)$ is much slower than $O(2^n)$. 

## The Traveling Salesman Problem
**The Traveling Salesman Problem (TSP)** is a famous hard problem that is easy to state. Given a list of cities on a map, what is the *shortest* tour that visits all the cities, starting and ending at the same city?

For example, suppose there are four cities named A,B,C, and D on a map. The distance between each pair of cities is given in the distance table. If we start in city A and end in city A, then a tour visits all the cities in some order:

![[tsp1.png]]

This tour is A B D C, and it's cost is the sum of the distances traveled, 3+1+4+1=9.

Different tours can have different costs. For example, the tour A D C B has cost 12:

![[tsp2.png]]

The question the traveling salesman problem asks is what tour of all cities, starting and ending at A, has the lowest cost? It turns out there are only 6 possible tours on 4 cities, so we can use a slow **brute force** algorithm and enumerate them all:

|   Tour  |    Cost    |
|:-------:|:----------:|
| A B C D | 3+2+4+3=12 |
| A B D C | 3+1+4+1=9  |
| A C B D | 1+2+1+3=7  |
| A C D B | 1+4+1+1=7  |
| A D B C | 3+1+2+1=7  |
| A D C B | 3+4+2+3=12 |

In this example three tours tie for the cheapest: A C B D, A C D B, and A D B C all cost 7.

With only a few cities it is not hard to calculate the cost of all tours. But in general it's infeasible to calculate all tours. If you have $n$ cities, then there are $(n-1)!$ tours to consider. So solving the TSP by brute force enumeration of all possible tours requires $O(n!)$ work, which is usually much worse than $O(2^n)$.

> $n!$ is **n factorial**, and defined to be the produce of the integers from 1 to $n$, i.e. $n! = 1 \cdot 2 \cdot 3 \cdot \ldots \cdot (n-1) \cdot n$. Factorials get very big very quickly, e.g. $10! = 3628800$, and $11! = 39916800$ (over 39 million).

If you wanted to calculate the cost of all tours of, say, the capitals of all 49 US continental states, that's $49! = 608281864034267560872252163321295376887552831379210240000000000$ tours. There's no computer (or computers!) fast enough to calculate so many tours.

The TSP is a well-studied problem, but all known general-purpose TSP-solving algorithms are slow in the worst case. But it is currently unknown if there is a fast polynomial-time algorithm for solving the TSP. In other words, it is unknown if the TSP is an easy or hard problem.

Since it has not been proven that a fast polynomial time algorithm *doesn't* exist for solving the TSP, there is the possibility that one day someone (maybe you!) will find such an algorithm. But beware: most computer scientists and mathematicians believe that no such algorithm exists, and the best that can be done is approximate algorithms, or algorithms that are fast only in certain special cases.

> **Note** The TSP has lots of applications, and a rich history. This [TSP Website](http://www.math.uwaterloo.ca/tsp/) discusses it in depth, and has many interesting examples, including lists of the best-known TSP tours for various problems (like visiting the capitals of all US states).


## NP-Completeness
The TSP is a member of a class of algorithmic problems known as [[NP-complete problem|NP-complete problems]]. Here are a couple of other [[NP-complete problem|NP-complete problems]].

## [The Number Partition Problem](https://en.wikipedia.org/wiki/Partition_problem)
Consider these numbers:

3 4 3 10

Can you partition them into groups of numbers, such that:
- each group has at least one number
- every number is in exactly one group
- the sum of the numbers in each group is the same

The answer for these numbers is *yes*, since both {10} and {3,3,4} both sum to 10.

This problem gets harder with more numbers. For example:

4 3 1 6 2 12 8 2 5 2 1

These numbers can be partition into {1,1,4,5,12} and {2,2,2,3,6,8}, both of which sum to 23.

In general, the number partition asks if you put $n$ different integers into two groups (under the constraints above), such that the sum of the numbers in each group is as close as possible.

> For example, suppose you and a friend are carrying grocery bags from shopping. How can you divide the bags between yourselves so that you each carry the same weight (or as close as possible to the same weight)?

Efficient algorithms are known for solving *some* instances of this problem, but in general there is no known algorithm that always runs in less than exponential time for *all* instances.

## [Logical satisfiability (SAT)](https://en.wikipedia.org/wiki/Boolean_satisfiability_problem)
Consider this logical expression:

(not p) or ((not q) and r)

Can you find true/false values for p, q, and r that make it true (i.e. that *satisfy* it)? One way to answer this question is to create a [truth table](https://en.wikipedia.org/wiki/Truth_table) for the expression to see when it's true and when it's false:

|   p   |   q   |   r   | (not p) or ((not q) and r) |
|:-----:|:-----:|:-----:|:--------------------------:|
| false | false | false |            true            |
| false | false |  true |            true            |
| false |  true | false |            true            |
| false |  true |  true |            true            |
|  true | false | false |            false           |
|  true | false |  true |            true            |
|  true |  true | false |            false           |
|  true |  true |  true |            false           |

This tables shows that, yes, there are values that can be assigned to p, q, and r that make (not p) or ((not q) and r), and so it is satisfiable. 

However, a [truth table](https://en.wikipedia.org/wiki/Truth_table) for an expression with $n$ variables has $2^n$ rows, and so $O(2^n)$ calculations are needed in the worst case. This is infeasible in practice because you can easily run into expressions with *hundreds*, or even *thousands*, of variables. For example, for an expression with 250 variables the [truth table](https://en.wikipedia.org/wiki/Truth_table) has $2^{250}=1809251394333065553493296640760748560207343510400633813116524750123642650624$ rows.

Interestingly, there are efficient *practical* programs, called [SAT solvers](https://en.wikipedia.org/wiki/Boolean_satisfiability_problem#Algorithms_for_solving_SAT>), that can quickly satisfy many large logical expressions that arise in practical applications (such as scheduling). The algorithms used by such programs are often *much* better than brute force, but unfortunately they cannot guarantee to work efficiently in *all* cases.

All known general-purpose SAT algorithms run in $O(2^n)$ (or worse!) in the worst case.

## NP-Complete Problems
[[NP-complete problem|NP-complete problems]] are a subset of hard problems that have an amazing property: if you can find a fast algorithm for *one* NP-complete problem, then there must be a fast algorithms for *all* NP-complete problems. 

Many important and useful problems turn out to be NP-complete. For example, the traveling salesman problem, logical satisfiability, and number partition are all NP-complete. So, if you find a fast algorithm for solving the traveling salesman problem, then this means there must be a fast algorithm for solving logical satisfiability problems.

Currently, no one knows if NP-complete problems are easy or hard.

Most mathematicians and computer scientists believe that there is *no* fast algorithm for *any* NP-complete problem. There are hundreds more NP-complete problems ([take a look at the list given here](https://en.wikipedia.org/wiki/List_of_NP-complete_problems)), many of them well-studied for decades, and since no fast algorithm has been found for any of them it seems unlikely that there is some master algorithm that efficiently solves them all.
 
However, no one has been able to *prove* that NP-complete problems are hard. Many computer scientists consider this to be the most important unsolved problem in all of theoretical computer science. It is often referred to as the **P=NP problem**.

[Leonid Levin](https://en.wikipedia.org/wiki/Leonid_Levin) and [Stephen Cook](https://en.wikipedia.org/wiki/Stephen_Cook) independantly discovered NP-complete problems in the 1970s.

[Leonid Levin](https://en.wikipedia.org/wiki/Leonid_Levin)
![[LeonidLevin.png]]

[Stephen Cook](https://en.wikipedia.org/wiki/Stephen_Cook)
![[StephenCook.png]]

# Practice Problems
1. What does it mean if an [[algorithm]] is *slow*? What does it mean if an [[algorithm]] is *fast*?
2. Given an example of a computational problem that *cannot* be solved in polynomial-time.
3. What do mean when we say a computational problem is *easy*, or *hard*?
4. Suppose an instance of the traveling salesman problem has 25 cities. If you solved it using brute force, about how many different tours would you need to check?
5. What does it mean for a problem to be NP-complete? Give an example of an NP-complete problem.

