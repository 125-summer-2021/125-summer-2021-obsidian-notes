#terminology

## Definition
`::` is the **scope resolution operator**, and is used to identify names in [[namespace]]s and classes.

## Example
In this code, there are two variables named `cout`: one from the `std` [[namespace]], and one declared as a [[local variable]]: 
   
```cpp
#include <iostream>

int main() {
    string cout = "console output"
    std::cout << "Hello, world!\\n";
	std::cout << cout << "\n";
}
```

We must write `std::cout` to use the `cout` from `std`. If we just write `cout`, that refers to the local `cout`.