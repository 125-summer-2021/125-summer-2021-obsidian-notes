## Linear Search
Intuitively, an [[algorithm]] is a precise step-by-step description of a process.  For example, suppose `v` is a `vector<int>`, and `v = {3,7,1,9}`. 

Does 1 appear in `v`? Yes, there is a 1 at index location 2, i.e. `v[2] == 1`.  We can prove this by checking the values of `v` one at a time like this:
- Is `v[0] == 1`? No, so check the next element.
- Is `v[1] == 1`? No, so check the next element.
- Is `v[2] == 1`? Yes: we found a 1 at index location 2, so we're done.

Does 2 appear in `v = {3,7,1,9}`? No, none of the elements in `v` is a 2. We can use the same procedure to prove this:
- Is `v[0] == 1`? No, so check the next element.
- Is `v[1] == 1`? No, so check the next element.
- Is `v[2] == 1`? No, so check the next element.
- Is `v[3] == 1`? No. There are no more elements, and so we've proven that 1 is not in `v`.

We call this algorithm for checking if a value `x` appears in a `vector` **linear search**. It works with a vector of any size `n`. It also works with arrays, strings, vectors of other types, and even files. Linear search can be used on any vector-like data structure.

## Performance
How efficient is linear search? The standard answer to this question is to count comparisons, i.e. how many times `==` is called. There are two main cases to consider:

- **When `x` is in `v`**, the number of times `==` is called depends where `x` appears in `v`. If `x` is near the left end of `v` then only a few comparisons are needed to find it. If `x` is near the right end, then closer to `n` comparisons are needed.

- **When `x` is *not* in `v`**, then we must check that all `n` elements are not equal to `v`. So we must call `==` `n` times.

In general,  the precise number of times `==` is called for any particular run of linear search depends upon where in `v` that `x` occurs, or if it occurs at all. 

One thing we can say in general, is that in the **worst case**, linear search does `n` comparisons. This is an important result to remember: any time you call linear search on a vector of `n` elements, it *might* end up doing `n` comparisons. Yes, it's possible that you could do only a few comparisons, but you should never count on that.

> **Rule of thumb** Hope for the best, but plan for the worst.

We can estimate how many comparisons linear search does on average. When you call linear search, you might get lucky and it does only few a  comparisons. But just as often you'll get unlucky and it will do about `n` comparisons. Over many runs the number of good cases and bad cases will be about the same, which works out to about $\frac{n}{2}$ comparisons on average.
 
## Implementation
Lets see a couple of ways to implement linear search.

We start with a [[pseudocode]] version that searches for a number `x` in a vector `v` of size `n`:

```
for i = 0 to n-1 do  // n-1, not n!
   if v[i] == x then
      return true    // success: found x
   end
end
return false         // failure: didn't find x
```

[[Pseudocode]] is useful here since it lets us ignore some of the technical details of C++ and emphasize the essential algorithm.

Here's a conversion of the [[pseudocode]] into C++:

```cpp
bool contains1(const vector<int>& v, int x) {
	for(int i = 0; i < v.size(); i++) {
		if (v[i] == x) {
			return true;    // success: found x
		}
	}
	return false;           // failure: didn't find x
}
```

### Linear Search: Returning an Index
> **Problem** Write a version of linear search called `index(v, x)` that returns, as an `int`, the index of the left-most occurrence of `x` in `v`. If `x` is not in `v`, return -1.

Here is a solution that uses a for-loop

```cpp
// Pre-condition:
//    none
// Post-condition:
//    Returns the smallest i >= 0 such that v[i] == x; or, if
//    x is not anywhere in v, then -1 is returned
int index1(const vector<int>& v, int x) {
    for(int i = 0; i < v.size(); i++) {
        if (x == v[i]) return i;
    }
    return -1;
}
```

The [[pre-condition]] says what what must be true *before* calling linear search. In this case, there is no pre-condition on linear search, i.e. it should work correctly for any `v` and `x` we give it.

The [[post-condition]] states what must be true *after* the function finishes executing (assuming  the [[pre-condition]] was true at the start). The [[pre-condition]] precisely states what linear search returns.

### Linear Search: contains Re-visited
> **Problem** Re-implement `contains(v, x)` using `index1`.

Since `index1(v, x)` returns -1 if `x` is not in `v`, we can implement contains like this:

```cpp
bool contains2(const vector<int>& v, int x) {
	return index1(v, x) != -1;
}
```

Or if you prefer:

```cpp
bool contains3(const vector<int>& v, int x) {
	if (index1(v, x) == -1) {
		return false;
	} else {
	    return true;
	}
}
```

The first version *without* the if-statement is generally considered better style. But it may be trickier to understand, and so you can use the if-statement version if you prefer.

### Linear Search: Searching a Sub-vector
> **Problem** Implement the function `index2(v, x, begin, end)` that does linear search on the sub-vector `{v[begin], v[begin+1], ..., v[end-2], v[end-1]}`. If `x` is in that sub-vector, then return its index. Otherwise, if `x` is not in that sub-vector, return -1.

This version of linear search works on any contiguous sub-vector of `v`:

```cpp
// linear search on the range [begin, end)
int index2(const vector<int>& v, int x, int begin, int end) {
    for(int i = begin; i < end; ++i) {
        if (x == v[i]) return i;
    }
    return -1;
}
```

`begin` is the index of the *first* element of `v` we want to search, and `end` is the index of *one past the last element* we want to search:

![[arrayBeginEnd.svg]]

The elements of the sub-vector go from `begin` to `end-1` (not `end`!). `[begin, end)` is the set of values `{v[begin], v[begin + 1], v[begin + 2], ..., v[end - 1]}`.

Thus, this searches the sub-vector `v[begin]`, `[begin + 1]`, ..., `v[end - 1]`.

A feature of these [[asymmetric range]]s is that the number of elements in the range is exactly `end - begin`. The C++ standard template library ([[standard template library|STL]]) also uses these kinds of asymmetric ranges. 

### Linear Search: index1 Re-visited
> **Problem** Re-implement `index1(v, x)` using `index2(v, x, begin, end)`.

This can be done with a single `return` statement:

```cpp
int index3(const vector<int>& v, int x) {
    return index2(v, x, 0, v.size());
}
```

### Linear Search: Recursion
> **Problem** Re-implement `index1(v, x)` using recursion. **Hint** Implement `index2(v, x, begin, end)` recursively as a helper function.

Linear search can be implemented recursively using these rules:
- *Base case*: If `v` is empty, return -1
- *Base case*: If `x` is the first element of `v`, return the index of `x`
- *Recursive case*: Otherwise, "remove" the first element of `v` and call linear search on the rest.

```cpp
// recursive linear search on a range
int index3(const vector<int>& v, int x, int begin, int end) {
    if (begin >= end) {
        return -1;
    } else if (v[begin] == x) {
        return begin;
    } else {
        return index3(v, x, begin + 1, end);  // note the + 1
    }
}

int index4(const vector<int>& v, int x) {
	return index3(v, x, 0, v.size());
}
```

Performance-wise, recursive linear search is likely not as efficient as linear search implemented with a loop due to all the function calls it must do, and all the parameters it must save on the [[call stack]]. Some compilers might be able to optimize-away the [[recursion]] using [[tail call elimination]].

## Practice Questions
1. In your own words, what is an algorithm?
2. Given an example of when searching the elements of a list in *reverse*
   order might be better than searching in forward order.
3. What operation is usually counted when estimating the performance of linear search?
4. In your own words, what are some of the advantages of the recursive implementation of linear search? What are some of  the disadvantages?
5. Suppose vector `v` has $n > 0$ numbers in some unknown order, and `x` is known to occur in `v` exactly once.
   a. In the **best-case**, how many comparisons must linear search do to find `x`?
   b. In the **worst-case**, how many comparisons must linear search do to find `x`?
   c. If `x` is *not* in `v`, how many comparisons will linear search do?

   
## Source Code
```cpp
// linear_search.cpp

#include <iostream>
#include <vector>

using namespace std;

// Pre-condition:
//    none
// Post-condition:
//    Returns true if x occurs one or more times in v, and 
//    false otherwise.
bool contains1(const vector<int>& v, int x) {
    for(int i = 0; i < v.size(); i++) {
        if (v[i] == x) {
            return true;    // success: found x
        }
    }
    return false;           // failure: didn't find x
}

// Pre-condition:
//    none
// Post-condition:
//    Returns the smallest i >= 0 such that v[i] == x; or, if
//    x is not anywhere in v, then -1 is returned
int index1(const vector<int>& v, int x) {
    for(int i = 0; i < v.size(); i++) {
        if (x == v[i]) return i;
    }
    return -1;
}

bool contains2(const vector<int>& v, int x) {
    return index1(v, x) != -1;
}

bool contains3(const vector<int>& v, int x) {
    if (index1(v, x) == -1) {
        return false;
    } else {
        return true;
    }
}

// linear search on the range [begin, end)
int index2(const vector<int>& v, int x, int begin, int end) {
    for(int i = begin; i < end; ++i) {
        if (x == v[i]) return i;
    }
    return -1;
}

int index3(const vector<int>& v, int x) {
    return index2(v, x, 0, v.size());
}

// recursive linear search on a range
int index3(const vector<int>& v, int x, int begin, int end) {
    if (begin >= end) {
        return -1;
    } else if (v[begin] == x) {
        return begin;
    } else {
        return index3(v, x, begin + 1, end);  // note the + 1
    }
}

// recursive linear search on a range
int index4(const vector<int>& v, int x) {
    return index3(v, x, 0, v.size());
}

int main() {
  vector<int> v = {5, 4, 3, 82, 2, 1, 0, 12, 2};
  for(int x : v) cout << x << " ";
  
  cout << "\n\n";
  cout << "contains1(v, 2)) = " << contains1(v, 2) << "\n";
  cout << "contains2(v, 2)) = " << contains2(v, 2) << "\n";
  cout << "   index1(v, 2)) = " << index1(v, 2)    << "\n";
  cout << "   index3(v, 2)) = " << index3(v, 2)    << "\n";
  cout << "   index4(v, 2)) = " << index4(v, 2)    << "\n";

  cout << "\n";
  cout << "contains1(v, 6)) = " << contains1(v, 6) << "\n";
  cout << "contains2(v, 6)) = " << contains2(v, 6) << "\n";
  cout << "   index1(v, 6)) = " << index1(v, 6)    << "\n";
  cout << "   index3(v, 6)) = " << index3(v, 6)    << "\n";
  cout << "   index4(v, 6)) = " << index4(v, 6)    << "\n";
}
```

