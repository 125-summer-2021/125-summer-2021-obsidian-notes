#terminology

## Definition
A sequence of zero or more bits. A bit is a 0 or 1. The bit string with no bits is called the **empty bit string**, and is often written `""`.

For a non-negative integer $n$, there are exactly $2^n$ different bit strings with $n$ bits.

## Example
These are all examples of bit strings:
- `""` (the empty bit string)
- `0`
- `1`
- `0000`
- `01011101`
