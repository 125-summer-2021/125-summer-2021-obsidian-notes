Recursive acronyms are a fun example of recursion. Perhaps you've heard of the
`GNU <https://www.gnu.org/>`_ project (they make g++, and much other
software). GNU is an acronym that expands like this:

```
GNU = GNU's Not Unix
```

If you keep replacing GNU with its expansion you get an infinitely long
string:

```
GNU = GNU's Not Unix
    = (GNU's Not Unix)'s Not Unix 
    = ((GNU's Not Unix)'s Not Unix)'s Not Unix
    = (((GNU's Not Unix)'s Not Unix)'s Not Unix)'s Not Unix 
    = ...
```

There is no base case, so it expands forever.

Here are a few more recursive acronyms:

 - YOPY = Your Own Personal YOPY

 - LAME = LAME Ain't an MP3 Encoder

 - These acronyms are *co-recursive*:

   - HURD = Hird of Unix-Replacing Daemons
 
   - HIRD = Hurd of Interfaces Representing Depth

 - MOMS is *doubly recursive* and so expands very quickly:
 
      MOMS = MOMS Offering MOMS Support
           = MOMS Offering MOMS Support Offering MOMS Offering MOMS Support 
             Support
           = MOMS Offering MOMS Support Offering MOMS Offering MOMS Support 
             Support Offering MOMS Offering MOMS Support Offering MOMS Offering 
             MOMS Support Support Support
           = ...

Here is a function that expands MOMS:

```cpp
const string acronym = "MOMS";
const string expansion = "MOMS Offering MOMS Support";
string expand_MOMS(int n) {
   if (n == 0) {
      return "";
   } else if (n == 1) {
      return expansion;
   } else { // n > 1
      string prev = expand_MOMS(n - 1);
      // replace every occurrence of "MOMS" with its expansion
      int i = prev.find(acronym);
      while (i != string::npos) {
         prev.replace(i, acronym.size(), expansion);
         // search for next acronym after the current one
         i += expansion.size(); // skip over the just-replaced string
                                // so the acronyms within it are not expanded
         i = prev.find(acronym, i);
     }
     return prev;
   } // if
}
```
