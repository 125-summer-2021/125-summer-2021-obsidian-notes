The following describes an implementation of a [[dynamic array]] using a class. It covers most of the basic uses of classes in C++, and the complete code is given at the very end.

## Introduction
A [[dynamic array]] is like a regular array, except it can grow in size as more elements are added to it (it might also shrink in size, but we won't do that in this example). In contrast, a regular array in C++ has a fixed size that never changes. You could, for example, use a dynamic array to read all the numbers in a file without needing to know ahead of time how many numbers are in the file.

These notes implement a [[dynamic array]] in an object-oriented way that is similar to the standard C++ `vector`. Our [[dynamic array]] will only store `int`s, and so we'll call it `int_vec`. We will start from a function-oriented implementation.

Recall that the key data structure for the function-oriented version is this `struct`:

```cpp
struct int_vec {
    int capacity; // length of underlying array
    int* arr;     // pointer to the underlying array
    int size;     // # of elements in this int_vec from user's perspective
}; 
```

The basic idea of the object-oriented version is to convert the relevant *functions* into *methods* that go inside `int_vec`, and to use [[information hiding]] to keep the variables private.

First, we will change `struct` to `class`:

```cpp
class int_vec {
public:
    int capacity; // length of underlying array
    int* arr;     // pointer to the underlying array
    int size;     // # of elements in this int_vec from user's perspective
}; 
```

The only important difference between a `struct` and a `class` for us is that, by default, the members of a class are private, while for a `struct` the members are public by default.

Making the member variables of `int_vec` is *not* a good idea, since a programmer might accidentally change them in a way that makes no sense. So, we will follow the principle of [[information hiding]] and make the variables *private*:

```cpp
class int_vec {
private:
    int capacity; // length of underlying array
    int* arr;     // pointer to the underlying array
    int size;     // # of elements in this int_vec from user's perspective
public:
    // nothing here yet
};
```

Now code outside of `int_vec` can't access `arr`, `size`, and `capacity`, and so cannot change them in non-sensical ways.

We do want to allow *some* access to the variables. And so we provide controlled access through methods:

```cpp
class int_vec {
private:
   int capacity; // length of underlying array
   int* arr;     // pointer to the underlying array
   int size;     // # of elements in this int_vec from user's 
                 // perspective

public:
   int get_size() const {      // a getter method
     return size;
   }

   int get_capacity() const {  // a getter method
     return capacity;
   }
};
```

We've added two [[getter|getters]]: `get_size` and `get_capacity`. They are both public, so any code can call them. All they do is *read* the values of `size` and `capacity`, and so calling these [[getter|getters]] cannot cause these variables to be inconsistent.

Importantly, we do *not* provide [[setters|setter]] for `size` or `capacity`. This is a good design decision. We certainly could have provided [[setter methods|setters]], but that doesn't make sense for this particular class: the user should *not* be able to change the size/capacity whenever they want. Those variables should only be changed by `int_vec` in the appropriate circumstances.

This idea of making data private is called [[information hiding]]. We hide details of the implementation so that the programmer doesn't have to worry about them, and can't accidentally (or intentionally!) mess them up. Experience has shown that [[information hiding]] is a very useful technique for building large and complex programs.

There is no [[getter]] or [[setter]] for `arr`. Instead, we provide a [[getter|getters]] and [[setter|setters]] to access its individual elements:

```cpp
 int get(int i) const {
   if (i < 0 || i > size) cmpt::error("get: index out of bounds");
   return arr[i];
 }

 // Note that set is not a const method because it modifies the underlying
 // array.
 void set(int i, int x) {  
   if (i < 0 || i > size) cmpt::error("get: index out of bounds");
   arr[i] = x;
 }
```

Notice that both `set` and `get` check to make sure the index variable `i` is within the bounds of the array. This is something that C++ arrays and vectors do *not* do, i.e. C++ arrays and vectors let you access elements outside of their bounds.

## Constructors
For the function-oriented dynamic array of previous assignment, we used "make" functions to create new `int_vec`s, e.g. `make_empty_vec`, `make_filled_vec`, `make_copy`, .... We also had a function called `deallocate` that deleted the underlying array, and the programmer had to remember to call it at the right time to avoid memory leaks (easier said than done!).

In object-oriented programming, instead of creating new objects with "make" functions we create them with [[constructor|constructors]]. Just like the make functions, the [[constructor]] initializes all the variables in the object to sensible values. 

Importantly, there's no way to create a C++ object without calling a constructor, and so after we construct an `int_vec` object we are guaranteed that it is sensible.

```cpp
class int_vec {
private:
   int capacity; // length of underlying array
   int* arr;     // pointer to the underlying array
   int size;     // # of elements in this int_vec from user's perspective
public:

   // Default constructor (takes no parameters)
   int_vec() {
     capacity = 10;            // set initial values
     arr = new int[capacity];  // of all private
     size = 0;                 // variables
   }

   int get_size() const {
     return size;
   }

   int get_capacity() const {
     return capacity;
   }

   int get(int i) const {
     if (i < 0 || i > size) cmpt::error("get: index out of bounds");
     return arr[i];
   }

   // Note that set is not a const method because it modifies the underlying
   // array.
   void set(int i, int x) {  
     if (i < 0 || i > size) cmpt::error("get: index out of bounds");
     arr[i] = x;
    }
};
```

The constructor initializes the variables in its body, but better is to use an [[initialization list]]:

```cpp
int_vec() 
: capacity(10), arr(new int[capacity]), size(0)  // initializer list
{ }
```

Now we can create  a new `int_vec` of size 0:

```cpp
int_vec v;  // default constructor called
```

For convenience, lets add another constructor that creates a new `int_vec` filled with copies of a given `int`:

```cpp
int_vec(int sz, int fill_value)
: capacity(10), size(sz)  // initializer list
{
    if (size < 0) cmpt::error("can't construct int_vec of negative size");
    if (size > capacity) capacity += size;  // capacity is 10 more than size
    arr = new int[capacity];
    for(int i = 0; i < size; ++i) {
        arr[i] = fill_value;
    }
}
```

Notice that we check inside the constructor body that `size` is 0 or greater: the user is not allowed to create an `int_vec` with a negative size. This is a nice safety feature that can help catch bugs.

Next, lets add a [[copy constructor]]:

```cpp
// copy constructor
int_vec(const int_vec& other)
: capacity(other.capacity), arr(new int[capacity]), size(other.size)
{
    for(int i = 0; i < size; ++i) {
        arr[i] = other.arr[i];
    }
}
```

This [[copy constructor]] makes a brand new array (in the [[initialization list]]) and then copies all the elements of `other` into it.

These constructors now let us write code like this:

```cpp
int_vec a;         // empty vector (size 0)
int_vec b(5, 0);   // size 5 vector initialized to all 0s
int_vec c(b);      // c is a copy of b (using a new underlying array)
```

## Destructors
If you run the current version of `int_vec` with [[valgrind]], it will report that there are [[memory leak|memory leaks]]. That's because the constructors allocate memory using `new`, but that memory not is de-allocated anywhere.

The best place to de-allocate `arr` is in a [[destructor]]. Since the [[destructor]] is only called just when the `int_vec` is de-allocated, `arr` will be automatically deleted as soon as the `int_vec` is deleted:

```cpp
~int_vec() {  // destructor: always ~ followed by class name
   delete[] arr;
}
```

The addition of this one method gets rid of all the `int_vec` [[memory leak|memory leaks]]! There is no way to *not* have the [[destructor]] called, or to have it called at the wrong time. `delete[]` is guaranteed to be called exactly once on `arr` when it is known that it is no longer needed.

You can put any code you like in a [[destructor]], although usually it contains "clean up" code that de-allocates resources used by the object. For debugging, it can be useful to add a print message to see when `arr` is de-allocated, e.g.:

```cpp
~int_vec() {  // destructor
    delete[] arr;
    cout << "int_vec destructor called\n";
}
```

## Printing and Appending

Now lets add methods to print an `int_vec` and to append a value onto the right end.

Here are a `print` and `println` method. Notice that both are [[const method|const methods]]:

```cpp
class int_vec {
private:
   // ...
public:
   // ...

   void print() const {
       if (size == 0) {
           cout << "{}";
       } else {
           cout << "{";
           cout << arr[0];
           for (int i = 1; i < size; ++i) {  // i starts at 1 (not 0)
               cout << ", " << arr[i];
           }
           cout << "}";
       }
   }

   void println() const {
       print();
       cout << "\n";
   }

};
```

And here is an implementation of `append`:

```cpp
class int_vec {
private:
   // ...
public:
   // ...

 void append(int x) {
    if (size >= capacity) {
       capacity = 2 * capacity;          // double the capacity
       int* new_arr = new int[capacity]; // make new underlying array 
                                         // twice size of old one
    
       for(int i = 0; i < size; ++i) {   // copy elements of v
          new_arr[i] = arr[i];           // into new_arr
       }
       
       delete[] arr;                     // delete old arr
      
       arr = new_arr;                    // assign new_arr
    }
    assert(size < capacity);
    arr[size] = x;
    size++;
 }

};
```

`append` is a bit tricky. Sometimes when `append` is called it re-sizes the underlying array by making a new array that is twice the size of the old one, and then copying the elements from the old one to the new one. This re-sizing and copying takes extra time and memory, but in practice it doesn't occur too often, and so this is a reasonably efficient strategy that is usable in many programs.

## Part 2
### Getting Rid of Magic Values
In the code at the end, there were a couple of examples of [[magic number|magic numbers]], or more generally [[magic number|magic values]].

For example, 10 is a [[magic number]] in these two constructors:

```cpp
class int_vec {
	// ...
public:
	int_vec() 
	: capacity(10), arr(new int[capacity]), size(0)
	{ }

	int_vec(int sz, int fill_value)
	: capacity(10), size(sz)
	{
		// ...
	}

	// ...

}; // class int_vec
```

You cannot tell from this code what 10 means. Would it be okay to change it to 9, or 11, or 100? Or -5?

Another problem is that there is no easy way for the user of `int_vec` to know what the default capacity is. So it would be helpful if there was a [[getter]] (or equivalent) that returned the default capacity.

A good solution to both of these problems is to use a [[static class member]]. For example:

```cpp
class int_vec {
	// ...
public:

	static const initial_capacity = 10;

	int_vec() 
	: capacity(initial_capacity), arr(new int[capacity]), size(0)
	{ }

	int_vec(int sz, int fill_value)
	: capacity(initial_capacity), size(sz)
	{
		// ...
	}

	// ...

}; // class int_vec
```

`initial_capacity` is a `static`, which means all `int_vec` objects share a single copy of it. Individual `int_vec` objects do *not* have their own personal copies of `initial_capacity`. Thus, this saves a little bit of memory, especially if you are using many `int_vec` objects.

We've also declared `initial_capacity` to be `const` because we never want a user of `int_vec` to change `initial_capacity`.

You use `::` notation to access static members of a class:

```cpp
int main() {
	cout << "initial capacity of an int_vec: " << int_vec::initial_capacity 
	     << "\n";
	// ...
}
```

Notice that we *don't* need to construct an `int_vec` object in order to access `initial_capacity`. For [[static class member|static class members]], the `::` notation is used to access them, i.e. `int_vec::initial_capacity`. In C++, the `.`-notation works with objects, and `::` with classes.

`::`-notation is often a good way to talk about methods. For example, suppose we write a class called `Product` and a class called `Truck`, and they both have their own `print()` methods. The expression `Truck::print()` refers to method in `Truck`, and `Product::print()` is the one in `Product`.


### Summing
Finally, summing the numbers in an array is a common operation, so lets make that easy by adding a method that returns the sum of the numbers in an `int_vec`:

```cpp
int_vec v;
v.append(4);
v.append(2);
v.append(3);

cout << v.sum();  // prints 9
```

Here are three different implementations of `sum`:

```cpp
// ...

#include <numeric>

// ...

class int_vec {
  // ...
public:

  // ...
  int sum1() const {
     int result = 0;
     for(int i = 0; i < size(); i++) {
         result += (*this)[i];
     }
     return result;
  }

  // trick: using a reference to avoid writing (*this)[]
  int sum2() const {
      int result = 0;
      const int_vec& v = *this;
      for(int i = 0; i < size(); i++) {
          result += v[i];
      }
      return result;
  }

  // trick: use std::accumulate (from <numeric>) 
  int sum3() const {
      return accumulate(arr, arr + sz, 0);
  }

};  // class int_vec
```

In `sum3()`, `accumulate` is a standard C++ function that, in this case, takes three inputs: a pointer (`arr`) to the start of the array, a pointer (`arr + sz`) to *one past* the last element of the array, and finally the initial value of the sum (i.e. 0). The advantage of `sum3` is that it is short, easy to read, most likely error free (functions in the standard library tend to be very extensively by users of C++), and very efficient.


## Practice Questions
1. Does this code work? If so, what exactly does it do?
   ```cpp
   int_vec a(a);
   ```
2. Supposed you wanted to save memory, and rewrote the  [[copy constructor]] for `int_vec` like this:
   ```cpp
   int_vec(const int_vec& other)
   : capacity(other.capacity), arr(new int[capacity]), size(other.size)
   {
       arr = other.arr
   }
   ```
    Would `int_vec` still work correctly? Explain why, or why not.
3. In your own words, what is a [[magic number]]? Give some examples.
4. In your own words, what is a [[static class member]]? How does it differ from a regular member variable or method?
5. What is the difference between the `.` operator and `::` operator? Give an example of when they are used.
6. Give an example of a program where the strategy of "array doubling" used by the `append` method might be very inefficient.
7. In the  `append` method, the capacity of the underlying array is *doubled* if necessary. The 2 used in the doubling calculation is an example of a [[magic number]]. So, replace the 2 in `append` with a suitably named [[static class member|static member variable]] as was done with the initial capacity.
8. Another [[magic number]] occurs in `operator==`: the "10" in the call to `resize`. Fix this by creating a new [[static class member]] as in the previous question.
9. The `int_vec::print` method and `operator<<` both have a couple of [[magic number|magic values]]: the `{` and `}` used to mark the start and end of the vector when printed, and `", "` is used as a separator.
    1. Create  [[static class member|static member variables]] (as done in the previous question) to get rid of these [[magic number|magic values]] in both the `int_vec::print` method and `operator<<`.
    2. How would you modify `int_vec` to allow the user to chance to specify the  start/end characters of a printed `int_vec`, and also the separator?

## int_vec_class_tutorial1.cpp
```cpp
// int_vec_class_tutorial1.cpp

#include <iostream>
#include "cmpt_error.h"
#include <cassert>

using namespace std;

class int_vec {
private:
    int capacity; // length of underlying array
    int* arr;     // pointer to the underlying array
    int size;     // # of elements in this int_vec from user's perspective
public:

    // Default constructor (takes no parameters)
    int_vec() 
    : capacity(10), arr(new int[capacity]), size(0)
    { }

    int_vec(int sz, int fill_value)
    : capacity(10), size(sz)
    {
        if (size < 0) {
		   cmpt::error("can't construct int_vec of negative size");
		}
        if (size > capacity) capacity += size;
        arr = new int[capacity];
        for(int i = 0; i < size; ++i) {
            arr[i] = fill_value;
        }
    }

    // Copy constructor
    int_vec(const int_vec& other)
    : capacity(other.capacity), arr(new int[capacity]), size(other.size)
    {
        for(int i = 0; i < size; ++i) {
            arr[i] = other.arr[i];
        }
    }

    ~int_vec() {
        delete[] arr;
    }

    int get_size() const {
        return size;
    }

    int get_capacity() const {
        return capacity;
    }

    int get(int i) const {
        if (i < 0 || i > size) cmpt::error("get: index out of bounds");
        return arr[i];
    }

    // Note that set is not a const method because it modifies the 
	// underlying array.
    void set(int i, int x) {  
        if (i < 0 || i > size) cmpt::error("get: index out of bounds");
        arr[i] = x;
    }

    void print() const {
        if (size == 0) {
            cout << "{}";
        } else {
            cout << "{";
            cout << arr[0];
            for (int i = 1; i < size; ++i) {  // i starts at 1 (not 0)
                cout << ", " << arr[i];
            }
            cout << "}";
        }
    }

    void println() const {
        print();
        cout << "\n";
    }

   void append(int x) {
       if (size >= capacity) {
          capacity = 2 * capacity;          // double the capacity
          int* new_arr = new int[capacity]; // make new underlying array 
                                            // twice size of old one
        
          for(int i = 0; i < size; ++i) {   // copy elements of v
             new_arr[i] = arr[i];           // into new_arr
          }
        
          delete[] arr;                     // delete old arr
        
          arr = new_arr;                    // assign new_arr
       }
       assert(size < capacity);
       arr[size] = x;
       size++;
   }

}; // class int_vec

int main() {
    int_vec v;
    cout << "    v.size(): " << v.get_size() << "\n";
    cout << "v.capacity(): " << v.get_capacity() << "\n";
    for(int i = 0; i < 25; ++i) {
        v.append(i);
    }
    v.println();
}
```

## int_vec_class_tutorial2.cpp
```cpp
// int_vec_class_tutorial2.cpp

#include "cmpt_error.h"
#include <iostream>
#include <cassert>

using namespace std;


class int_vec {
private:
    int capacity;  // length of underlying array
    int* arr;      // pointer to the underlying array
    int size;      // # of elements in this int_vec from user's 
	               // perspective

    void resize(int new_cap) {
        if (new_cap < capacity) return;
        capacity = new_cap;

        int* new_arr = new int[capacity];  // create new, bigger array

        for(int i = 0; i < size; ++i) {    // copy elements of arr
            new_arr[i] = arr[i];           // into new_arr
        }

        delete[] arr;                      // delete old arr

        arr = new_arr;                     // assign new_arr
    }

public:
    // default constructor (takes no parameters)
    int_vec() 
    : capacity(10), arr(new int[capacity]), size(0)
    { }

    int_vec(int sz, int fill_value)
    : capacity(10), size(sz)
    {
        if (size < 0) {
			cmpt::error("can't construct int_vec of negative size");
		}
        if (size > 0) capacity += size;
        arr = new int[capacity];
        for(int i = 0; i < size; ++i) {
            arr[i] = fill_value;
        }
    }

    // copy constructor
    int_vec(const int_vec& other)
    : capacity(other.capacity), arr(new int[capacity]), size(other.size)
    {
        cout << "int_vec copy constructor called ...\n";
        for(int i = 0; i < size; ++i) {
            arr[i] = other.arr[i];
        }
    }

    ~int_vec() {
        cout << "... ~int_vec called\n";
        delete[] arr;
    }

    int get_size() const {
        return size;
    }

    int get_capacity() const {
        return capacity;
    }

    int get(int i) const {
        if (i < 0 || i > size) cmpt::error("get: index out of bounds");
        return arr[i];
    }

    // Note that set is not a const method because it modifies the 
	// underlying array.
    void set(int i, int x) {  
        if (i < 0 || i > size) cmpt::error("get: index out of bounds");
        arr[i] = x;
    }


    int& operator[](int i) {
        cout << "(modifying operator[] called)\n";
        return arr[i];
    }

    int operator[](int i) const {
        cout << "(const operator[] called)\n";
        return arr[i];
    }

    void print() const {
        if (size == 0) {
            cout << "{}";
        } else {
            cout << "{";
            cout << arr[0];
            for (int i = 1; i < size; ++i) {  // i starts at 1 (not 0)
                cout << ", " << arr[i];
            }
            cout << "}";
        }
    }

    void println() const {
        print();
        cout << "\n";
    }

    void append(int x) {
        if (size >= capacity) {
            resize(2 * capacity);   // double the capacity
       }
       assert(size < capacity);
       arr[size] = x;
       size++;
   }

    int_vec& operator=(const int_vec& other) {
        // self-assignment is a special case: don't need to do anything
        if (this == &other) {
            return *this;
        } else {
            // re-size this int_vecs underlying array if necessary
            if (capacity < other.size) {
                resize(other.size + 10);     // some extra capacity
            }                                // to speed up append
            
            size = other.size;           

            for(int i = 0; i < size; ++i) {  // copy other's values
                arr[i] = other.arr[i];       // into this array
            }

            return *this;
        }
    }

    friend void clear(int_vec& v);
}; // class int_vec


void clear(int_vec& v) {
    v.size = 0;
}

bool operator==(const int_vec& a, const int_vec& b) {
    if (a.get_size() != b.get_size()) {
        return false;
    } else {
        for(int i = 0; i < a.get_size(); ++i) {
            if (a.get(i) != b.get(i)) {
                return false;
            }
        }
        return true;
    }
}

bool operator!=(const int_vec& a, const int_vec& b) {
    return !(a == b);
}

ostream& operator<<(ostream& os, const int_vec& arr) {
    if (arr.get_size() == 0) {
        os << "{}";
    } else {
        os << "{";
        os << arr.get(0);
        for (int i = 1; i < arr.get_size(); ++i) {  // i starts at 1
			os << ", " << arr.get(i);
        }
        os << "}";
    }
    return os;
}

void test1() {
    int_vec a;
    int_vec b;
    for(int i = 0; i < 10; ++i) {
        a.append(i);
        b.append(i);
    }

    if (a != b) {
        cout << "a and b are different\n";
    } else {
        cout << "a and b are the same\n";
    }

    cout << "a = " << a << "\n" 
         << "b = " << b << "\n";
}

void test2() {
    int_vec a;
    for(int i = 0; i < 10; ++i) {
        a.append(i);
    }

    int_vec b = a;  // calls operator=
    int_vec c(a);   // calls copy constructor

    c.append(14);
    c.append(-1);
    b = c;

    if (a != b) {
        cout << "a and b are different\n";
    } else {
        cout << "a and b are the same\n";
    }

    cout << "a = " << a << "\n" 
         << "b = " << b << "\n"
         << "c = " << c << "\n";
}

void test3() {
    int_vec a;
    for(int i = 0; i < 10; ++i) {
        a.append(i);
    }

    cout << "a = " << a << "\n";

    for(int i = 0; i < a.get_size(); ++i) {
        a[i] = 2 * a[i];
    }

    cout << "a = " << a << "\n";

    const int_vec b(10, -1);
    for(int i = 0; i < b.get_size(); ++i) {
        cout << "b[" << i << "] = " << b[i] << "\n";
    }

}

void test4() {
    int_vec a;
    for(int i = 0; i < 10; ++i) {
        a.append(i);
    }

    cout << "a = " << a << "\n";

    clear(a);

    cout << "a = " << a << "\n";
}

int main() {
    test1();
    // test2();
    // test3();
    // test4();
}
```
