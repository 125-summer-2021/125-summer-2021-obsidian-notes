#terminology

## Definition
A dangling pointer is a pointer that is point to memory that is no longer valid. This often occurs when two or more pointers are pointing to the same memory location.

In C++, determining if a pointer is dangling can be difficult. Along with [[memory leak|memory leaks]], they can be a source of tricky memory errors.

## Example
```cpp
int* p = new int(5);   // p points to a new int on the free store
int* q = p;            // q points to the same int as p

delete p;              // okay: value p pointed to de-allocated

//
// Problem! q is now a dangling pointer
//
```

The problem here is that after calling `delete p`, the value that `q` is pointing to is de-allocated. That means `q` is a dangling pointer and should not be used. It's up to the programmer to recognize this.
