CMPT 125 is a second course in computer programming. It uses the C++ programming language, and in addition to covering the basics of C++ programming it introduces a number of theoretical computer science topics.

## Useful Links
CMPT 125 uses the g++ C++ compiler in Linux, and also the [[SFML]] graphics library. 

You should [[Getting Your C++ Environment Up and Running|Install a virtual machine]] so you can use g++ and [[SFML]] graphics library. 

- If you're using Windows, you can also try using [[WSL Windows Subsystem for Linux|the Windows Subsystem for Linux (WSL)]]. This gives you just a command-line, and there is currently not an easy way to run graphical Linux programs (such as [[SFML]]).
  **Warning**:  [[WSL Windows Subsystem for Linux|Windows Subsystem for Linux (WSL)]] and [VirtualBox](https://www.virtualbox.org/) (suggested in [[Getting Your C++ Environment Up and Running|installing a virtual machine]]) do *not* currently work well together. If you want to use both [[WSL Windows Subsystem for Linux|Windows Subsystem for Linux (WSL)]] and a virtual machine, you should use [VMware](https://www.vmware.com/ca/products/workstation-player/workstation-player-evaluation.html) instead.

If you run into problems andcan't get Linux running, here are two other suggestions:
- [Log into a CSIL computer following these instructions](http://www.sfu.ca/computing/about/support/csil/how-to-remote-access-to-csil.html).
- [[using Repl.it|Use Repl.it]].

You should only use these two options as a last resort: getting Linux running on a virtual machine is a useful skill for any software developer.

## Notes 
### Week 1
- [[Introduction to C++]]
- [[helloWorld.pdf]]: a line-by-line description of every part of the traditional "Hello, world" program; a good overview of essential C++ [[syntax]].

### Week 2
- [[Introduction to C++]]
	- [[Introduction to C++#Arithmetic Operators]]
	- [[Introduction to C++#Booleans]]
	- [[Introduction to C++#Relational Operators]]
	- [[Introduction to C++#Enumerations]] onward
- [[SFML|SFML example]]
- Optional example: [[Counting characters, lines, and words]]

### Week 3
- [[Calling a function]]
- [[Recursion]]

### Week 4
- [[Recursion]]
- [[Vectors and strings]]
- [[Introduction to Pointers]]
- [[Memory management]]

### Week 5
- [[Memory management]]
- [[Testing code]]

### Week 6
- [[namespace|Namespaces]]
- [[Introduction to Exceptions|Exceptions]]
- [[Object-oriented programming]]
	- [[object-oriented word counting example]]

### Week 7
- [[Object-oriented programming]]
	- [[object-oriented word counting example]]
- Read on your own: [[Implementing a dynamic array]]
- [[Introduction to inheritance|Inheritance]]
- Optional: [[Separate compilation]], and [[splitting int_vec]] into a `.h` and `.cpp` file

### Week 8
- [[Introduction to inheritance|Inheritance]]
- Monday June 28 2021 lecture: canceled (SFU closed due to heat)
- Friday July 2: review of solutions to assignment 2

### Week 9
- [[Introduction to inheritance|Inheritance]]
- [[Linked lists]]

### Week 10
- [[Abstract data types]]
- [[Calculating large powers]]
	- [[bigPowers_lectureNotes.pdf|template used in lecture (PDF)]]
	- Optional extra: [[the max function]]
- [[linear search]]
	- [[linearSearch_LectureNotes.pdf|template used in lecture (PDF)]]
	- Optional extra: [[extra notes on linear search|algorithms and linear search]]

### Week 11
- [[notes on basic sorting]]
	- [[basicSorting_LectureNotes.pdf|template used in lecture (PDF)]]
- [[notes on binary search]]

### Week  12
- [[O-notation]]
	- [[Onotation_LectureNotes.pdf|template used in lecture (PDF)]]
	- [[expressionTables.pdf|PDF of spreadsheet showing]] graphs and counts for various common O-notation expressions
- [[Easy and hard problems]]
	- [[easyAndHardProblems_LectureNotes.pdf|template used in lecture (PDF)]]
- [[Finite state machines and regular expressions]]
	- [[fsmsAndRegexes_LectureNotes.pdf|template used in lecture (PDF)]]

### Week  13
- [[The work of Alan Turing]]
	- [[AlanTuring_LectureNotes.pdf|template used in lecture (PDF)]]
- [[The STL and generic programming]]
	- [[STLandGenerics_LectureNotes.pdf|template used in lecture (PDF)]]

----------------------

Topics below this line are tentative and might change.

----------------------

### Optional Extras
- [[Recursion on vectors]]
- [[Using Templates]]