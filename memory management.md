## The Three Memory Regions of a Running C++ Program
The memory of a running C++ program can be thought of as one long array of bytes. That array is divided into three main [[C++ memory regions|memory regions]]:

```
 +--------------------------------------------+
 |       |                  |                 |  Main Memory
 +--------------------------------------------+
  Static        Stack            Free store
  memory        memory           memory     
```

[[Static memory]] has a fixed size (that is known at compile-time) that never
gets bigger or smaller. Global variables and constants are usually stored in static memory.

[[Stack memory]] is where local variables and function parameters are stored. Stack memory is *automatically* allocated and de-allocated as needed. When a function is called, the parameters and local variables of the function are put onto stack memory (see [[calling a function]]), and when the function ends its parameters and variables are automatically removed.

[[Free store memory|Free store (heap) memory]] is all the other memory, and in contrast to [[stack memory]] it is *not* automatically managed in C++ (although many other programming languages do manage it automatically). Instead, a C++ programmer must allocate memory on the free store using `new`, and de-allocate it using `delete`/`delete[]`.


## The Free Store
Free store memory is accessed through [[pointers]]:

```cpp
double* y_addr = new double(6.4);  // returns a pointer 
                                   // to a newly allocated
                                   // double on the free store
```

`new` always returns a pointer. `*` is used to [[dereferencing operator|dereference a pointer]], i.e. to access the value it points to:

```cpp
cout << *y_addr << "\n";  // prints 6.4
*y_addr += 2;
cout << *y_addr << "\n";  // prints 8.4
```

In this context, `*` is called the [[dereferencing operator]], or [[dereferencing operator|indirection operator]].


## De-allocating Free Store Memory
When you are finished using memory that's on the free store, you, the C++ programmer, must remember to de-allocate it using `delete` it, e.g.:

```cpp
delete y_addr;
```

If you forget to do this, your program is said to have a [[memory leak]].

For long-running programs (such as an operating system, a web server, a web browser, a text editor, etc.), [[memory leak|memory leaks]] will eventually eat up all the available memory. A future call to `new` could then fail because there is not enough memory left.

Experience shows that finding and fixing memory leaks is *very* difficult in general. It is tricky to be 100% sure that you are not de-allocating memory that you still need.

Many other programming languages use an automatic memory de-allocation technique known as [[garbage collection]] to automatically de-allocate free store memory. But C++ does **not** use garbage collection.

When a garbage collector runs, it slows the program down (or even causes it to briefly stop) while memory is being cleaned-up. This might be a problem in some applications. For example, real-time flight control software might not be able to tolerate any amount of slow-down --- a pilot does not want their controls to lock-up for even a brief moment.

C++ aims for efficiency and flexibility, and so it does **not** provide a garbage collector. Instead, it is up to the programmer to carefully manage all the free store memory they use. C++ does provide some help, such as [[destructor|destructors]] (which we will see when we get to classes), and also [[smart pointer|smart pointers]].


## Arrays on the Free Store
`new` and `delete[]` (the `[]`-brackets are important!) are used to allocate
and de-allocate *arrays* on the free store:
        
```cpp
int* arr = new int[5]; // arr points to a newly allocated array of 5 ints 
                       // on the free store; the value of the ints is unknown
```

The `[]`-brackets in `new int[5]` are essential: that's how C++ knows you want to allocate an array instead of a single `int` initialized to 5. If you wrote `new int(5)`, C++ would assume you're allocating a single `int` with an initial value of 5.

`arr` points to the first value of the array, and so we can use [[pointer arithmetic]] to access its individual elements:

```cpp
*(arr + 0) = 1;
*(arr + 1) = 2;
*(arr + 2) = 3;
*(arr + 3) = 4;
*(arr + 4) = 5;
```

You can also use `[]`-bracket notation: `arr[i]` is shorthand for `*(arr + i)`:

```cpp
arr[0] = 1;
arr[1] = 2;
arr[2] = 3;
arr[3] = 4;
arr[4] = 5;
```

Here's how you can imagine the array in memory:

```            
     218 219 220 221 222 223 224
    +---+---+---+---+---+---+---+ 
... |   | 1 | 2 | 3 | 4 | 5 |   | ...
    +---+---+---+---+---+---+---+
              
          arr == 219
```
             
Of course, we don't know the address values for sure, so we've just made them up in this example.

Since `arr` is an `int*` (i.e. a pointer to an `int`), for an array it points to the *first* element of the array, i.e. it contains the address of the first element (219 in this case). Then:

- `arr + 1` is 220, the second element of the array
- `arr + 2` is 221, the third element of the array
- `arr + 3` is 220, the fourth element of the array
- etc.

Unfortunately, C++ arrays **don't** stop you from accessing elements *before* or *after* the array. For example, `arr - 1` is address 218, which is `int`-sized chunk of memory that appears just before the start of the array. In general, you have no idea what memory value might be before the array, or even if you are allowed to access it. Similarly, `arr + 5` is address 215, which is the `int`-size chunk of memory immediately after the last element of the array.

C++ lets you add and subtract values to pointers, and this is called [[pointer arithmetic]]. While [[pointer arithmetic]] is useful for accessing array values, it has few restrictions: from a single pointer you can theoretically read/write *any* memory location on your computer, which is both dangerous and a security risk.

As another example of using pointers, here are three different ways to print the contents of `arr`:

```cpp
for(int i = 0; i < 5; ++i) {           // *(arr + i) style
    cout << *(arr + i) << "\n";
}

for(int i = 0; i < 5; ++i) {           // arr[i] style
    cout << arr[i] << "\n";
}

for(int* p = arr; p < arr + 5; p++) {  // direct pointer style
    cout << *p << "\n";
}
```

The last for-loop is interesting: it directly increments a pointer to print each element, and no `i` index variable is needed. C++ knows that `p++` increments the address in `p` by the length of 1 `int` since it knows that `p` is an `int*` pointer.

Note that in all three loops we need to keep track of the size of the array ourselves since C++ arrays don't store their size.

## Example: Sorting a `str_array`
Suppose `str_array` is defined like this:

```cpp
struct str_array {
    string* arr;
    int sz;
    int cap;
}
```

`str_array` is meant to store a *dynamic array* where:
- `arr` points to the first `string` in the array
- `sz` is the number of strings in the array from the point of view of the programmer using it
- `cap` is the size of the underlying array

Here's a diagram:

```
                                     |
       0                         sz-1| sz             cap-1
    +----+--------     ---------+----+----+--     ---+----+
arr |    |         ...          |    |    |   ...    |    |
    +----+--------     ---------+----+----+--     ---+----+
                 used entries        |   unused entries                     
                                     |                     
```

`arr[0]` to `arr[sz-1]` are the first `sz` elements of the array, and they are the elements the programmer knows about.

`arr[sz]` to `arr[cap-1]` are the *unused* array entries. They are there to make it fast to expand the size of the array.

If there aren't any unused entries, then to increase the size of the dynamic array you must make a new (bigger) array, copy all the elements from the old array into it, and then delete the original array. Keeping some extra entries means often you won't have to do this relatively expensive array copying.

Now suppose we want to sort the elements of a `str_array`, i.e. we want to re-arrange the strings in the underlying array so they are in alphabetical order. We a `sort` function that works like this:

```cpp
// words is a str_array that has already been created
sort(words);
```

After calling `sort`, the strings in `words` will be in alphabetical order.

Here is a function that works:

```cpp
#include <algorithm>

void sort(str_array a) {
    std::sort(a.arr, a.arr + a.size);  // std::sort is from <algorithm>
} 
```

To sort a `str_array` named `a`, we have to sort all the elements in `a.arr` from `a.arr[0]` to `a.arr[a.size-1]`. Fortunately, the C++ standard library has a sorting function called `std::sort(begin, end)` that lets us sort any sub-array in an array.

We need to give `std::sort` two input pointers: one that points to the first element of the array (`a.arr`), and one that points to *one past* the last element of the array (`a.arr + a.size`).

Note that `std::sort(a.arr, a.arr + a.size - 1)` would be wrong: it misses the last
element of the array, i.e. the element at `a.arr[a.size - 1]`.

See [[sort_example_cpp]]  for some code showing everything working together.

## Practice Questions
1. In your own words, describe the three main [[C++ memory regions||memory regions]] of a running C++ program, and what they store.
2. Write a fragment of code that creates a new `string` variable on the free store with the value `"orange"`. Show how to print the string to `cout`.
3. In your own words, explain the difference between `new int(5)` and `new int[5]`.
4. In your own words, explain the difference between `delete` and `delete[]`. 
5. In your own words, explain what a [[memory leak]] is and how it  might cause a program to crash.
6. What is a [[garbage collector]]? Why doesn't C++ use one?
7. Suppose `arr` is an array of `n` `double`s. Write a statement that sorts the values of `arr` in order from smallest to biggest using  the standard C++ sorting function (`sort`).
