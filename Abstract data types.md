---
aliases: [Abstract Data Type, ADT, ADTs]
---

![[attachments/Barbara-Liskov-Turing-Award-2008.jpg|250]]
Above is computer scientist and [Turing Award winner](https://en.wikipedia.org/wiki/Turing_Award) [Barbara Liskov](https://en.wikipedia.org/wiki/Barbara_Liskov), who invented abstract data types in 1974 with her student Stephen Ziles.

## Introduction
In C++, `int`, `bool`, `string`, and `vector<double>` are all examples of **data types**. The type of a piece of data tells you something about it, e.g. if you know that variable `n` contains an `int`, then you know that you can do arithmetic with `n`, or print out its value, and so on.

A data type doesn't tell you *everything* about a piece of data. For example, `n` might represent the number of people in a group text chat. Knowing that `n` is an `int` doesn't tell you anything about the group chat part, e.g. such as the fact that `n` is never negative.

An **abstract data type**, or **ADT** for short, is a mathematical model of a data type that is defined according to the behaviour that the user sees. Importantly, **the implementation details of an ADT are usually not specified**, allowing programmers to implement them in whatever way makes the most sense.

When discussing ADTs, it sometimes useful to refer to non-ADT types, such as `int`, `bool`, `string`, and `vector<double>` as **concrete types**. Concrete types typically have a specific implementation. For example, a C++ `int` is sequence (usually) 32 or 64 0s and 1s, and a `string` is a sequence of 0 or more characters. The implementation of these types is provided.
 
### ADT Example: Integers
The integers, along with operations on them, are an example of an abstract data type. The integers consist of the numbers { ..., -2, -1, 0, 1, 2, ...}, and as an ADT we *don't* specify how they are implemented. They could be encoded as [2s complement numbers](https://en.wikipedia.org/wiki/Two%27s_complement), or a more intricate ["big integer" format as in Python](https://rushter.com/blog/python-integer-implementation/).

The integer ADT includes operations on the numbers, such as addition, subtraction, and multiplication. As an ADT, addition is defined according to its *behaviour*. For example, if $a$ and $b$ are any two integers, then, no matter the implementation, it's should always be true that $a + b = b + a$, and that $a + 0 = a$. The implementation details of the integers and addition is left unstated in an ADT: the programmer can implement them any way that matches the behaviour.

> **Note** By *integers* we don't mean the C++ `int` type, which is a concrete type. We mean integers in the mathematical sense, as defined in a math class. In math, the *implementation* of integers is not usually given. Normally, you are just told the *properties* the integers have.

### ADT Example: Stacks
A **stack** is a container of values that has many uses in programming (e.g. the [[call stack]] in C++ programs). Intuitively, a stack of is a linear data structure with a top and a bottom. Elements are added/removed only at the top. Adding an element to the top of a stack is called **pushing** an element onto to the stack, and removing the top item on a stack is called **popping** the stack. Making a copy of the top element of a stack without removing it is sometimes called **peeking**.

For example, this stack has three numbers on it:

```
5   <---  top of the stack
3
1   <--- bottom of the stack
---
```

If we push 2 onto the stack it looks like this:

```
2   <---  top of the stack
5
3
1   <--- bottom of the stack
---
```

If we pop the stack, then the 2 is removed:

```
5   <---  top of the stack
3
1   <--- bottom of the stack
---
```

If we pop it again, it looks like this:

```
3   <---  top of the stack
1   <--- bottom of the stack
---
```

If we pop it two more times, we get an empty stack:

```
<empty>
---
```

Popping an empty stack causes an error.

Here are the stack operations we'll consider:

- Test if the stack is empty.
- Push an element onto the top of the stack. For simplicity, we will *not* worry about the stack running out of memory.
- Peek at the top of the stack (without removing the top element). This lets you copy the top element of a stack without removing it.
- Pop an element from the top of the stack.

We'll consider two implementations the stack ADT: using a vector, and using a linked list. These are quite different implementations, but both are good ways of implementing the stack ADT.


### Abstract Classes in C++
In C++, we can model ADTs using [[abstract class|abstract classes]]. An [[abstract class]] is a class where one of more of its methods are *abstract* (i.e. the body of the method is `= 0`). An [[abstract method]] has no implementation, and so its impossible to create an object of abstract class. But we can *inherit* from abstract classes, and the inheriting classes provide the implementations of the abstract methods.

For example, take a look at the `Stack_base` class. It's an example of an abstract base class:

```cpp
class Stack_base {
public:
    virtual ~Stack_base() {}  // always include a virtual destructor in a base
                              // class; this allows inheriting classes to define
                              // their own destructor

    // Pre-condition:
    //    none
    // Post-condition:
    //    returns true if the stack is empty, and false otherwise
    virtual bool is_empty() const = 0;

    // Pre-condition:
    //    none
    // Post-condition:
    //    puts x on the top of the stack
    virtual void push(double x) = 0;

    // Pre-condition:
    //    !is_empty()
    // Post-condition: 
    //   removes and returns the top element of the stack
    virtual double pop() = 0;

    // Pre-condition:
    //    !is_empty()
    // Post-condition: 
    //   returns a copy of the top element of the stack (without removing it)
    virtual double peek() const = 0;

    // Pre-condition:
    //    none
    // Post-condition: 
    //   pops all elements from the stack
    // Note:
    //   pop_all() is virtual, which means a subclass can override it.
    //   But it is not abstract, i.e. it has a default implementation, and
    //   so a subclass may choose not to provide its own implementation.
    virtual void pop_all() {
        while (!is_empty()) pop();
    }

}; // class Stack_base
```

It defines the following abstract and virtual methods:

- `is_empty` tests if the stack is empty
- `push(x)` puts `x` on the top of the stack
- `pop()` removes and returns the top element of the stack
- `peek()` returns, but doesn't remove, the top element of the stack

All four of these methods are virtual and abstract: they have no implementations, and so subclasses must implement them.

In addition, there are two other methods in `Stack_base`:
- `~Stack_base()` is a [[virtual destructor]], which allows subclasses to provide their own destructor.
- `pop_all()` is a helper method that pops the stack until it is empty. It comes with a default implementation, and so subclasses are *not* required to provide their own implementation. But they can if they want: `pop_all()` is virtual, so a subclass can override it.

`stack.cpp` (at the end of this page) gives two concrete implementations of the stack ADT: `Vec_stack` (using a `vector`) and `List_stack` (using a linked list). Notice that the testing functions for these two classes are almost identical.


### Example: The size Function
Our stack ADT lacks a *size* method, i.e. it does not provide any way to get the number of items in a stack. Is it possible to implement a size function using just the operations in `Stack_base`? Think about this for a moment: the answer is not completely obvious.

It turns that yes, it is possible to implement a size function using just the stack ADT. For example:

- pop all the items out of the stack into a temporary stack, keeping a count of how many items are popped in a variable `num_popped`
- pop all the items out of the temporary stack and push them back onto the original stack
- return `num_popped`

```cpp
int size(Stack_base& s) {
    int result = 0;

    // pop each element from s and store them in temp
    Vec_stack temp;
    while (!s.is_empty()) {
        temp.push(s.pop());
        result++;
    }

    // push all items back into s
    while (!temp.is_empty()) {
        s.push(temp.pop());
    }

    return result;
}
```

Notice a couple of things:

- We don't know the exact type of `s`: it could be a `Vec_stack`, or a `List_stack`, or some other class that inherits from `Stack_base`. We can only use methods that are listed in `Stack_base`.
- We *can't* write the type of `s` as `const Stack_base&` because `s` is modified, i.e. we call `s.pop()`.
- The local variable `temp` is a `Vec_stack`, but could just as easily have been a `List_stack`.
- Copying all elements from one stack into another *reverses* the order of the elements. Copying them back again puts them back into the original order.
- This `size` function isn't very efficient, but there's not much else we can do given the limitations of our stack ADT.


## What's So Great about ADTs?
Some of the major benefits of using ADTs are:

- **Encapsulation**: An ADTs implementation might be very complex. But those details are *hidden* inside the ADT so that programmers don't need to know about them. All they need to know is how to *use* the ADT, which is generally much simpler than understanding its implementation.
- **Localization of change**: If the *implementation* of an ADT changes (e.g. it is made faster), and the *interface* of the ADT (i.e. the headers of the public methods) stays the same, then code that *uses* the ADT does *not* change.
- **Flexibility**: Different implementations of an ADT might be better in different situations, and so it is possible to change implementations as needed. This can make some programs more efficient.

## Practice Questions
1. What does ADT stand for? What is the definition of an ADT?
2. In C++, what is an [[abstract method]]? What is an [[abstract class]]?
3. The `size(Stack_base& s)` function in the notes passes the stack `s` using [[pass by reference]]. Is it possible to implement a version of size where `s` is passed by [[pass by constant reference|constant reference]] instead?
4. What are three major benefits of using ADTs?
5. Write a function called `double bottom(Stack_base& s)` that returns a copy of the bottom-most element of a non-empty stack. The function can modify `s` if need be, but when the function is done `s` should be in exactly the same state it was in before  `bottom` was called.

## Source Code
```cpp
// stack.cpp

#include <iostream>
#include <vector>
#include <cassert>

using namespace std;

//
// Stack_base is an abstract base class (ABC), i.e. a class where some of the
// methods are not implemented (i.e. they are abstract).
//
class Stack_base {
public:
    // always include a virtual destructor in a base class
    virtual ~Stack_base() {} 

    // Pre-condition:
    //    none
    // Post-condition:
    //    returns true if the stack is empty, and false otherwise
    virtual bool is_empty() const = 0;

    // Pre-condition:
    //    none
    // Post-condition:
    //    puts x on the top of the stack
    virtual void push(double x) = 0;

    // Pre-condition:
    //    !is_empty()
    // Post-condition: 
    //   removes and returns the top element of the stack
    virtual double pop() = 0;

    // Pre-condition:
    //    !is_empty()
    // Post-condition: 
    //   returns a copy of the top element of the stack (without removing it)
    virtual double peek() const = 0;

    // Pre-condition:
    //    none
    // Post-condition: 
    //   pops all elements from the stack
    // Note:
    //   pop_all() is virtual, which means a subclass can override it.
    //   But it is not abstract, i.e. it has a default implementation, and
    //   so a subclass may choose not to provide its own implementation.
    virtual void pop_all() {
        while (!is_empty()) pop();
    }

}; // class Stack_base

class Vec_stack : public Stack_base {
private:
    vector<double> v;
public:
    // default constructor makes an empty stack
    Vec_stack() { }

    // Pre-condition:
    //    none
    // Post-condition:
    //    returns true if the stack is empty, and false otherwise
    bool is_empty() const { 
        return v.size() == 0; 
    }

    // Pre-condition:
    //    none
    // Post-condition:
    //    puts x on the top of the stack
    void push(double x) {
        v.push_back(x);
    }

    // Pre-condition:
    //    !is_empty()
    // Post-condition: 
    //   removes and returns the top element of the stack
    double pop() {
        double result = v.back();
        v.pop_back();
        return result;
    }

    // Pre-condition:
    //    !is_empty()
    // Post-condition: 
    //   returns a copy of the top element of the stack (without removing it)
    double peek() const {
        return v.back();
    }

}; // class Vec_stack

void vec_stack_test() {
    Vec_stack s;
    assert(s.is_empty());

    s.push(5.4);
    assert(!s.is_empty());
    assert(s.peek() == 5.4);
    
    double top = s.pop();
    assert(top == 5.4);
    assert(s.is_empty());

    s.push(2);
    s.push(3);
    s.push(4);
    assert(!s.is_empty());
    assert(s.peek() == 4);

    s.pop();
    assert(!s.is_empty());
    assert(s.peek() == 3);

    s.pop();
    assert(!s.is_empty());
    assert(s.peek() == 2);

    s.pop();
    assert(s.is_empty());

    s.push(5);
    s.push(6);
    s.pop_all();
    assert(s.is_empty());

    cout << "All Vec_stack tests passed\n";
}

//
// A linked list implementation of a stack.
// 
class List_stack : public Stack_base {
private:
    class Node {
    public:
        double val;
        Node* next;
    };

    Node* head = nullptr;
public:
    // default constructor makes an empty stack
    List_stack()  { }

    // destructor: delete all nodes on the list
    ~List_stack() {
        pop_all();
    }

    // Pre-condition:
    //    none
    // Post-condition:
    //    returns true if the stack is empty, and false otherwise
    bool is_empty() const { 
        return head == nullptr; 
    }

    // Pre-condition:
    //    none
    // Post-condition:
    //    puts x on the top of the stack
    void push(double x) {
        head = new Node{x, head};
    }

    // Pre-condition:
    //    !is_empty()
    // Post-condition: 
    //   removes and returns the top element of the stack
    double pop() {
        double result = head->val;
        Node* p = head;
        head = head->next;
        delete p;
        return result;
    }

    // Pre-condition:
    //    !is_empty()
    // Post-condition: 
    //   returns a copy of the top element of the stack 
    //   (without removing it)
    double peek() const {
        return head->val;
    }

}; // class List_stack

void list_stack_test() {
    List_stack s;
    assert(s.is_empty());

    s.push(5.4);
    assert(!s.is_empty());
    assert(s.peek() == 5.4);
    
    double top = s.pop();
    assert(top == 5.4);
    assert(s.is_empty());

    s.push(2);
    s.push(3);
    s.push(4);
    assert(!s.is_empty());
    assert(s.peek() == 4);

    s.pop();
    assert(!s.is_empty());
    assert(s.peek() == 3);

    s.pop();
    assert(!s.is_empty());
    assert(s.peek() == 2);

    s.pop();
    assert(s.is_empty());

    s.push(5);
    s.push(6);
    s.pop_all();
    assert(s.is_empty());

    cout << "All List_stack tests passed\n";
}

// Returns the number of elements in stack s. It does this in a rather
// inefficient way, since Stack_base doesn't provide a size() method.
//
// Note that we cannot pass s by constant reference because size must call
// s.pop(), i.e. size must modify s.
//
// Importantly, the type of s is Stack_base&, and so s could be either
// Vec_stack or List_stack. The code inside size only uses methods listed in
// Stack_base.
int size(Stack_base& s) {
    int result = 0;

    // pop each element from s and store them in temp
    Vec_stack temp;
    while (!s.is_empty()) {
        temp.push(s.pop());
        result++;
    }

    // push all items back into s
    while (!temp.is_empty()) {
        s.push(temp.pop());
    }

    return result;
}

void size_test() {
    Vec_stack vs;
    List_stack ls;
    assert(size(vs) == 0);
    assert(size(ls) == 0);

    vs.push(6);
    ls.push(6);
    assert(size(vs) == 1);
    assert(size(ls) == 1);    

    vs.push(7);
    ls.push(7);
    assert(size(vs) == 2);
    assert(size(ls) == 2);  

    vs.pop();
    ls.pop();
    assert(size(vs) == 1);
    assert(size(ls) == 1);

    cout << "All size tests passed\n";
}

int main() {
    vec_stack_test();
    list_stack_test();
    size_test();
}
```

