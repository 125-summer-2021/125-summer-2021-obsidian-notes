The C++ programming language began in the late 1970s and early 1980s as a project by [Bjarne Stroustrup](https://en.wikipedia.org/wiki/Bjarne_Stroustrup):

![[Bjarne_Stroustrup.png]]

He wanted a language that was as efficient as the language C, but with better features for designing programs. C is a popular systems programming language that has been used to implement, for instance, the Linux operating system.

While C is great for low-level programming with bits and bytes, it can be difficult to use for higher-level applications. Similarly, while languages like [Python](https://www.python.org/) or [JavaScript](https://en.wikipedia.org/wiki/JavaScript) are pleasant for creating many kinds of user applications, they're too inefficient and inflexible for many high-performance programs (like operating systems). 

So, C++ is [Stroustrup's](https://en.wikipedia.org/wiki/Bjarne_Stroustrup) attempt at creating a language that is as efficient as C, but can be more easily uses for high-level programs. He borrowed the idea of classes and objects from the [Simula programming language](https://en.wikipedia.org/wiki/Simula) and added them to C. Classes let you create new data types specific to the problem you're solving.
 
C++ has been a very successful language. Under constant development since its creation in the early 1980s, many new features have been added to C++ over the years. It is now a huge language, and its not possible to understand *every* feature of it in a single course. We'll focus on a small but useful subset.

One final note. While C++ supports programming that is higher-level than C, C++ still tends to be best used for programs that need low-level access to computer hardware, or where speed or memory-efficiency is paramount. C++ would be a **good** language for writing, say, an operating system for a phone, or a graphics engine for a video game. It's **not so good** for writing user-oriented apps that appear on a phone, or scripting individual levels of a game. 

## Prior Programming Experience Assumed!
We assume this is not your first programming course. You should already have learned some other programming language, such as Python. It's okay if you don't know C++.

C++ is a **compiled language**. We write C++ source code in a text file, and then we use a program called a compiler to translate the source code into an executable program. The executable program can then be run by the user. In this course, we will be using the g++ compiler in Linux. You can edit C++ source files in any text editor you like.

## Hello, World!
The traditional first C++ program prints the message "Hello, world!" on the terminal. Save this source code in a text file named `hello.cpp`:

```cpp
// hello.cpp

#include <iostream>

int main() {
	std::cout << "Hello, world\n";
}
```

**Type it exactly**! Even one wrong character could cause a problem.

To run a C++ program we must first compile it. We're using the Linux g++ compiler, and we compile it by typing this command in the terminal:

```bash
$ g++ hello.cpp
```

If there are no compiling errors, this creates an executable file named `a.out` that you run in the terminal by typing this:

```bash
$ ./a.out
Hello, world!
```

Instead of compiling programs directly using g++, in this course we are going to use a a *makefile* to simplify things a bit. Save this in a text file named `makefile`:

```makefile
# makefile
CPPFLAGS = -std=c++17 -Wall -Wextra -Werror -Wfatal-errors -Wno-sign-compare -Wnon-virtual-dtor -g
```

If you put `makefile` in the same folder as `hello.cpp`, you can now compile `hello.cpp` by typing this in the terminal:

```bash
$ make hello
g++  -std=c++17 -Wall -Wextra -Werror -Wfatal-errors -Wno-sign-compare -Wnon-virtual-dtor -g   hello.cpp   -o hello
```

g++ is automatically called using the options in the `makefile`. Plus, the executable files is named `hello`, and you run it like this:

```bash
$ ./hello
```

Because of this, we will often refer to the process of compiling a file as **making**, or **building** it.

## Compiler Errors
If your program has a mistake, then the compiler using catches the error:

```cpp
// hello.cpp

#include <iostream>

int main() {
	std::cout << "Hello, world\n"   // oops: missing semi-colon ;
}
```

When this file is compiled an error is reported:

```bash
$ make hello
g++  -std=c++17 -Wall -Wextra -Werror -Wfatal-errors -Wno-sign-compare -Wnon-virtual-dtor -g   hello.cpp   -o hello
hello.cpp: In function ‘int main()’:
hello.cpp:6:32: error: expected ‘;’ before ‘}’ token
    6 |  std::cout << "Hello, world!\n"
      |                                ^
      |                                ;
    7 | }
      | ~                               
compilation terminated due to -Wfatal-errors.
make: *** [<builtin>: hello] Error 1
```

In this case the compiler gave a very helpful error message. Sometimes, though, compiler error messages can be harder to understand. Consider this error:

```cpp
// hello.cpp

#include <iostream>

int main()                          // oops: missing {
	std::cout << "Hello, world\n"; 
}
```

The compiler gives this error message:

```cpp
$ make hello
g++  -std=c++17 -Wall -Wextra -Werror -Wfatal-errors -Wno-sign-compare -Wnon-virtual-dtor -g   hello.cpp   -o hello
hello.cpp:6:2: error: expected initializer before ‘std’
    6 |  std::cout << "Hello, world!\n";
      |  ^~~
compilation terminated due to -Wfatal-errors.
make: *** [<builtin>: hello] Error 1
```

The error message is pointing to `std` as being the problem, but the real problem is the missing `{` just before that. This error shows that `std` is the first token that caused  the compiler a problem.

## A Note on Formatting
C++ doesn't usually mind if you use extra or unusual [[whitespace]]. For example, this program compiles and prints "Hello, world!":

```cpp
            #include   <iostream>

int

main
(
)                   {std
              ::

cout                    << 
                      "Hello, world!\n"

       ;  }
```

This is poorly formatted source code --- humans have a hard time reading it. 

## Source Code Comments
In C++, ``//`` indicates a source comment. Everything from ``//`` to the end of the line is ignored by the compiler. For example, ``//`` is used twice in this program:

```cpp
// hello.cpp

#include <iostream>

int main() {
	std::cout << "Hello, world\n";
}
```

Another kind of C++ source comment starts with `/*`  and ends with `*/`. These kinds of comments can cover multiple lines. For example:

```cpp
/* hello.cpp */

/* 
   This program prints the string
   "Hello, world!" on the terminal.
*/

#include <iostream>

int main() {
	std::cout << "Hello, world\n";
}
```

Source code comments are notes to the programmer, and should be used to help explain pieces of code that are hard to understand.

**Limitation**: You can't put `/* */`-style comments inside `/* */`-style comments. For example this causes a compiler error:

```cpp
/* This does /* not */ compile! */
```

The problem is that C++ things the first `*/` matches with the `/*` at the start.

However, it's okay to put `//`-style comments inside `/* */`-style or `//`-style comments.


## Types, Variables, and Values
C++ is a [[statically typed language]]. That means that the type of values and functions are known at compile-time, and the compiler can find many type-related errors. This is very different than languages such as [Python](https://www.python.org/) or [JavaScript](https://en.wikipedia.org/wiki/JavaScript), which are [[dynamically typed language]]s. In [[dynamically typed language]]s, types are not known until you run the program

### Integers
Numbers like 82, -399, and 0 are examples of **integer literals**, and the standard integer data type in C++ is `int`.

An `int` variable is a named location in a computers memory that stores an `int`. For example:

```cpp
#include <iostream>

int main() {
	int a = 5;
	std::cout << "a = " << a << "\n";
}
```

The statement `int a = 5;` **defines** the variable `a`. `a` is of type `int`, and so you can only store `int` values in it. For example, this is a compiler error:

```cpp
#include <iostream>

int main() {
	int a = "five";   // doesn't compile: can't assign a string to an int
	std::cout << "a = " << a << "\n";
}

```

However, this works, but maybe not the way you might expect:

```cpp
#include <iostream>

int main() {
	int a = 5.8;
	std::cout << "a = " << a << "\n";  // prints 5
}
```

5.8 is not an `int`, it's a floating-point number that has the type `double`. C++ simply chops off the .8 when you assign it to the `int` `a`.

In this example, `a` is not given any initial value:

```cpp
#include <iostream>

int main() {
	int a;  // a has an unknown initial value: could be any int!
	std::cout << "a = " << a << "\n";
}
```

If you don't assign a value to `a`, then C++ doesn't give it any value. `a` gets the value of whatever bits in are in the memory location it gets assigned to. With the compiler options set by our `makefile`, this turns out to cause a compiler error: when C++ gets to the `a` in the `cout` statement, it recognizes that `a` has some unknown value and causes an error. But be careful: not all compilers necessarily catch unassigned variables! **You should always assign an initial value to newly defined variables**.

The type `long` also represents integers, but  `long` values typically have more bits than `int` values, and so can be used for calculations with larger numbers.

The types `unsigned int` and `unsigned long` represent non-negative integers, i.e, unsigned typed have no sign and so can never be negative. Unsigned integer values are often useful when you want to manipulate the individual bits of a value.

### Arithmetic Operators
If `x` and `y` are variables of some numeric type, then:
- `x + y` is the sum of `x` and `y`
- `x - y` is the difference of `x` and `y`
- `x * y` is  the product of `x` and `y`
- `x / y` is `x` divided by `y`. If `y` is 0 then you this causes an error value, or possibly an error at run-time.

If `x` and `y` are integer types, then the expression `x % y` is the *remainder* when `x` is divided by `y`. For example, `7 % 3` evaluates to 4, and `14 % 7` evaluates to 0. A useful example of `%` is the expression `x % 2`, which is 0 if `x` is even and 1 if `x` is odd. 

Use `=` to assign a value to a variable. For example:
- `x = 5;` assigns the value 5 to the variable `x`
- `y = x;` assigns a copy of the value in `y` to the variable `x`
- `x = x + 1;` assigns the result of `x + 1` to `x`. Notice that this is *not* a mathematical equation. If it were, you could subtract `x` from both sides and get `0 = 1`, which is a mathematical contradiction. `=` is *not* equality, it is assignment*.

C++ provides a number of assignment shortcuts. For example:

- `x++;` adds 1 to the value in `x`. The name of the C++ language comes from this, i.e. C++ is "C plus 1". You can also write this as `++x`.
- `x += 3;` adds 3 to the value in `x`.
- `x += y;` adds the value in `y` to the value in `x`.
- `y *= 2.5;` multiples the value in `y` by 2.5
- `x -= 4;` subtracts 4 from the value in `x`
- `x /= 3;` divides the value in `x` by 3

### Floating Point Values
Numbers like 3.98, -1.002, and 3.0 are all examples of **floating point numbers**, i.e. numbers with a decimal point in them. In C++, the standard floating point type is `double`.

For example:

```cpp
#include <iostream>

int main() {
	double a = 2.6;
	std::cout << "a = " << a << "\n";   // prints 2.6
}
```

The statement `double a = 2.6;` **defines** the variable `a`. `a` is of type `double`, and so you can only store `double` values in it.

If you assign an `int` to a `double` variable, C++ automatically converts the `int` to a `double`. For example:

```cpp
#include <iostream>

int main() {
	double a = 2;  // assigning an int to a double
	std::cout << "a = " << a << "\n";   // prints 2
}
```

Converting an `int` to a `double` is always okay because integers are a subset of floating point numbers.

Another C++ floating point type you will sometimes run into is `float`. The `float` type represents floating point numbers, but it doesn't usually have as many bits as `double`, and so `float` cannot represent as many numbers as `double`. In practice, you should always try to use `double` when you can because it is more accurate than `float`. A `float` would probably be most useful in cases where you want to save memory, i.e. often a `float` value uses half the number of bits as a `double` value.

### Character Values
`'a'`, `'!'`, and `'\n'` are all examples of **character literals**. Character literals always begin with a single-quote, and end with a single-quote. Some character literals, such as `'\n'` use **escape codes**, i.e. the back-slash indicates the next character is special.

In C++, the standard floating point type is `char`. For example:

```cpp
#include <iostream>

int main() {
	char a = 'T';
	std::cout << "a = " << a << "\n";  // prints: a = T
}
```

Interestingly, in C++ the `char` type is a kind of integer type. For example, in this program we directly assign `'a'` the integer 84:

```cpp
#include <iostream>

int main() {
	char a = 84;                            // 84 is the ASCII value for 'T'
	std::cout << "a = " << a << "\n";       // prints: a = T
	std::cout << "a = " << int(a) << "\n";  // prints: a = 84
}
```

It's useful to keep in mind that `char`s are just small integers, and anything you can do to an integer you can also do to a `char`. For example, since characters are integers it makes sense to test if one character is less than another, or to add 1 to a character, e.g. `'a' + 1` equals `b`.

### Booleans
The type `bool` is used for true false values. For example:

```cpp
#include <iostream>

using namespace std;

int main() {
	bool flag1 = false;
	bool flag2 = true;

	cout << "flag1 = " << flag1 << "\n";
	cout << "flag2 = " << flag2 << "\n";
}
```

This program prints:

```
flag1 = 0
flag2 = 1
```

In general, 0 means *false* in C++, and any non-zero value means *true*. It's usually clearest to use `false` and `true`.

C++ has three main **boolean operators** that can process `bool` values. In the following we assume `a` and `b` are both `bool` values:

- `&&` means **and**, and the expression `a && b` is `true` just when both `a` and `b` are `true`. If `a` is `false`, or `b` is `false`, or both are `false`, then `a && b` is `false`.
- `||` means **or**, and the expression `a || b` is `true` just when both `a` is `true`, or `b` is `true`, or both `a` and `b` are `true`. If `a` and `b` are both `false`, then `a || b` is `false`.
- `!` means **not**, and the expression `!a` is `true` when `a` is `false`, and `false` when `a` is `true`.

These boolean operators can be combined to make more complex expressions. For example suppose `a`, `b`, and `c` are all values of type `bool`.  Then:

- `a && b && c` is `true` just when all of `a`, `b`, and `c` are `true`.
- `a || b || c` is `true` just when one, or more, of `a`, `b`, and `c` are `true`.
-  `a && !b && !c` is `true` just when `a` is `true`, and `b` and `c` are both `false`.
-  `!(a && b)` is `true` just when it's *not* the case that both `a` and `b` are `true`. In other words, `!(a && b)` is `true` when `a` is `false`, or `b` is `false`, or both `a` and `b` are `false`.
-  `!!a`  is `true` just when `a` is `true`, and `false` otherwise. In fact, `a!!` always has the same logical value as `a`.

`&&` and `||` have important feature: they use **short circuit evaluation**. For instance, when C++ evaluates the expression `false && expr`, where `expr` is some boolean expression that evaluates to `true` or `false`, then the value of the expression is `false`. C++ does *not* evaluate `expr`: once it sees `false` and then `&&`, it knows the entire expression must be `false`. Similarly, C++ determines that the expression `true || expr` is `true` without evaluating `expr`.

Boolean expressions are important in C++ because they are used in loops and if-statements. We'll see many more examples of them later.

### Relational Operators
Suppose that `x` and `y` are both variables of some numeric type, e.g. `int`, `double`, or `char`. Then you can compare `x` and `y` as follows:

- `x == y` returns `true` if `x` and `y` are equal, and `false` otherwise.
- `x ! y` returns `true` if `x` and `y` are *not* equal, and `false` otherwise.
- `x < y` returns `true` if `x` is less than `y`, and `false` otherwise.
- `x <= y` returns `true` if `x` is less than, or equal to, `y`, and `false` otherwise.
- `x > y` returns `true` if `x` is greater than `y`, and `false` otherwise.
- `x >= y` returns `true` if `x` is greater than, or equal to, `y`, and `false` otherwise.

Importantly, these expressions also work if `x` and `y` are `string` variables. For example, `x < y` is `true` just when the string `x` is alphabetically before the string `y`.

These are all boolean expressions, i.e. they evaluate to either `true` or `false`. This means you can combine them with boolean operators to make more expressions. For example, suppose `x`, `y`, and `z` are all variables with the *same* numeric type. Then:

- `x == y && y == z` is `true` just when `x` equals `y` and `y` equals `z`, i.e. when all of them have the same value.
- `x != y && x != z && y != z` is `true` just when `x`, `y`, and `z` all have *different* values.
- `x <= y && y <= z` is `true` just when `x`, `y`, and `z` are in order from smallest to biggest, i.e.  they are in [[sorted order|ascending sorted order]].
- `x > y && (x == 5 || z == 5)` is `true` just when `x` is bigger than `y`, and either `x` is 5, or `z` is 5 (or both are 5).


### Defining Variables with auto
C++ variables can be defined by explicitly specifying their type:

```cpp
int    a = 5;      // the types of
double x = 6.22;   // a, x, and c
char   c = 'm';    // are explicitly given
cout << a << ' ' 
     << x << ' '
     << c << ' '
     << "\n";
```

`a`, `x`, and `c` are all variables. The code explicitly declares `a` to be of type `int`, `x` to be of  type `double`, and `c` to be of type `char`. 

Since C++11, the `auto` keyword can infer the types of variables in some situations. For example:

```cpp
auto a = 5;       // auto uses type inference
auto x = 6.22;    // to determine the types
auto c = 'm';     // of a (and int), x (a double), and c (a char)
cout << a << ' ' 
     << x << ' '
     << c << ' '
     << "\n";
```

C++ infers that `a` is of type `int` because the literal value 5 is of type `int`. Similarly, it infers `x` as a `double` because the literal value 6.22 is of type `double`.


### Type Conversions and Type Casts
Sometimes C++ will let you mix different types together. For example, this program shows that you can assign a `char` to an `int`:

```cpp
#include <iostream>

using namespace std;

int main() {
	char letter = 's';
	int code = letter;
	cout << letter  // prints s
	     << " " 
	     << code    // prints 115 (ASCII code for s)
	     << "\n";
}
```

This makes sense since a `char` is essentially a small integer value. You can also assign a `char` to an `int`:

```cpp
int code = 115;
char letter = code;
cout << letter  // prints s
	 << " " 
	 << code    // prints 115 (ASCII code for s)
	 << "\n";	
```

**Be careful**: an `int` value could be outside the range of a `char`, in which case you can get nonsense values, e.g.:

```cpp
int code = 500; // more than the biggest int
char letter = code;
cout << letter  // prints unknown value
	 << " " 
	 << code    // prints 500
	 << "\n";	
```

C++ also converts implicitly between integers and floating point types, e.g.:

```cpp
int n = 50;
double x = n;
cout << n    // prints 50
     << " "
     << x    // prints 50
     << "\n";
```

Assigning an `int`  to a `double` is okay because every `int` value has a corresponding `double` value. **Be careful**: assigning a `double` to an `int` loses any digits after the decimal point:

```cpp
double x = 4.999;
int n = x;   // chops off digits after the decimal
cout << n    // prints 4
     << " "
     << x    // prints 4.999
     << "\n";
```

Sometimes you may need t0 explicitly **cast**, or change, the type of a value. For example:

```cpp
char c = 's';
cout << c      // prints s
     << " " 
     << int(c) // prints 115 (ASCII code for s)
     << "\n";	
```

The expression `int(c)` is sometimes called a **cast**. It converts the `char` `c` to its corresponding `int` value. 

Another way to write a cast in C++ is to use `static_cast` like this:

```cpp
char c = 's';
cout << c      // prints s
     << " " 
     << static_cast<int>(c) // prints 115 (ASCII code for s)
     << "\n";	
```

This notation is easier to read and search than the `int(c)` notation, and so some programmers prefer it. It's also longer and uglier, which some programmers think is a *good* thing since it discourage from using it.

Yet another way to write a case is `(int)c`:

```cpp
char c = 's';
cout << c      // prints s
     << " " 
     << (int)c // prints 115 (ASCII code for s)
     << "\n";
```

The expression `(int)c` looks a bit unusual, and is the notation that C uses for casting. This notation can be useful later when we start using pointers. There is yet another casting notation in C++, `static_cast<int>(c)`:

```cpp
char c = 's';
cout << c                   // prints s
     << " " 
     << static_cast<int>(c) // prints 115 (ASCII code for s)
     << "\n";	
```

A nice feature of this notation is that it is easy to read and search for in a program. 

In general, you should avoiding casting types. Needing to change the type of a value can be a sign you've stored the value as the wrong type to begin with.


### Structs
A `struct` stores one or more variables of different types. They are like records in a database, and later we will that structs are the used extensively in [[object-oriented programming]].

For example, an (x,y) point in mathematics can be represented as a struct:

```cpp
#include <iostream>

using namespace std;

struct Point {
	double x;
	double y;
}; // <-- don't forget this semi-colon!

int main() {
	Point p{0, 0};
	cout << p.x   // 0
	     << " " 
	     << p.y   // 0
	     << "\n";  

	p.x++;
	p.y = 20;

	cout << p.x   // 1
         << " " 
         << p.y   // 20
         << "\n";
}
```

**Dot notation** is used to access the variables in a struct, i.e. `p.x` is the x-value of the point `p`, and `p.y` is its y-value.

You can even structs within structs. For example:

```cpp
#include <iostream>

using namespace std;

struct Point {
	double x;
	double y;
}; // <-- don't forget this semi-colon!

struct Circle {
	Point center;
	double radius;
}; // <-- don't forget this semi-colon!

int main() {
	Circle c{{5, 10}, 6.2};
	cout << "center: " 
	     << c.center.x << " "
	     << c.center.y << "\n"
	     << "radius: " << c.radius
	     << "\n";
}
```

A struct can have one or more variables of other types.  For instance:

```cpp
#include <iostream>
#include <string>

using namespace std;

struct Person {
	string first_name;
	string last_name;
	int age;
}; // <-- don't forget this semi-colon!

int main() {
	Person a{"Margaret", "Atwood", 81};
    string full_name = a.first_name + " " + a.last_name;
    cout << full_name << " is " << a.age << " years young\n";
}
```

In C++, structs are often used in the context of [[object-oriented programming]], and we will see more examples of them later in the course. 

### Enumerations
In C++, an **enumeration** is a user-created data type with a small set of explicitly defined values. For example, here is a **scoped enumeration**:

```cpp
#include <iostream>

using namespace std;

enum class Color { 
	red, green, blue 
};

int main() {
	Color a = Color::blue;
	cout << int(a) << "\n"; // prints 2
}
```

We call `Color` an `enum class`, or sometimes just an enumeration. You access the values inside an enum class using the `::` operator as shown. 

The underlying type of this enum is an `int`, and you so can cast it to an `int` as shown. However, the type of variable `a` is *not* an `int`; the type of `a` is `Color`. That means it is an error to assign an `int` to `a`, e.g.:

```cpp
a = 2; // compiler error: can't assign an int to a Color
```

C++ has another way to write enums, which it inherited from C. These are called **unscoped enumerations**, and we won't cover them in this course. They are generally more difficult and error-prone to use than scoped enumerations, although many C++ programs use them since they existed in C++ long before the addition of scoped enums.


## Control Structures: Loops, If-statements, and Switches
### If-statements
Here's the general form of a C++ if-statement:

```cpp
if (cond1) {
	body_1;
} else if (cond2) {
	body_2;
} else if (cond3) {
	body_3
} 
.
.
.
} else if (cond_n) {
	body_n;
} else {
	body_else;
}
```

Each of `cond1` to `condn` are *conditions*, i.e. boolean expressions that evaluate to either `true` or `false`.

### Switch Statements
Switch statements are an alternative to if-statements that can make decisions using basic types like `char`s and `int`s, e.g.:

```cpp
char sep = ',';
switch (sep) {
	case ' ':
	case ',':
	   cout << "definitely a separator";
	   break;
	case '-':
	   cout << "maybe a separator" 
	   break;
	default:
	   cout << "not a separator";
}
```

The code under the cases will executed if the value matches the case. Notice that `break` *must* be used to prevent the program from "falling through" to the next case.

Each case in a switch statement is one value of the variable being switched on. If you want to check for a range of cases, then you must list each value explicitly with its own `case`.

`default:` is the catch-all case. The code after that will be run if none of the cases above it are matched.

You *cannot* use a `switch` statement with `string`s or other more complex types. This makes it much less general that an if-statement.

### While Loops
A C++ while-loop has this general form:

```cpp
before_code;
while (cond) {
	body;
}
after_code;
```

The condition `cond` is a boolean expression that evaluates to either `true` or `false`. If `cond` is true, then `body` is executed. After that finishes, `cond` is checked again, and if it is true `body` will be executed again. If `cond` evaluates to `false`, then `body` is *not* executed, and the programs skips to `after_code`. 

### Do-while Loops
A C++ do-while loop has this general form:

```cpp
before_code;
do {
	body;
} while (cond);
after_code;
```

It is essentially the same as a while-loop, except the condition `cond` is check *after* `body` is executed. This means the body of a do-while loop is always executed at least once.

We will rarely, if ever, use do-while loops in this course. For most purposes, while-loops and for-loops are all we need.

### C-Style For-loop
A C-style for-loop has this general form:

```cpp
before_code;
for(init; cond; incr) {
	body;
}
after_code;
```

 When the for-loop is executed, the first thing it does is run `init`. Then it checks to see if `cond` is `true`; if it's not, then it skips to `after_code`. But if `cond` is `true`, then it executes `body`, and then after that executes `incr` and checks the condition again.

`init` and `incr` are often used with an index variable. For example, this prints the numbers from 1 to 100:

```cpp
for(int i = 0; i < 100; i++) {
	cout << i + 1;
}
```

In general, a for-loop can be re-written as a while loop as follows:

```cpp
before_code;

{
   init;
   while (cond) {
      body;
	  incr;
   }
}

after_code;
```

### For-each Style Loop
Since C++11, there is also a for-each style loop in C++ that directly loops through the elements of a vector or array without the need for an index variable. 

For example, this prints all the values of `nums`:

```cpp
vector<int> nums = {5, 6, 3, -2, 0, 5};
for(int n : nums) {
	cout << n << "\n";
}
```

The `:` can be read as "in", and so the loop is saying "for each of the `int`s `n` in `nums`, do the following ...".

## Functions
The kind of C++ functions we'll mostly use have this general form:

```cpp
T fun_name(T1 param_1, T2 param_2, ..., Tn param_n) {
	// ... C++ code ...
	
	// ... return statement that returns a value of type T ...
}
```

This function is called `fun_name`, and it takes `n` input parameters of types `T1`, `T2`, ..., `Tn`. It returns one value of type `T`, and use must use a return statement of the form `return expr` to end the function, where `expr` evaluates to a value of type `T`. 

The details of [[calling a function]] are quite useful to know. In particular, they will later help us better understand [[recursion]].

### Passing Parameters to Functions
We will mainly use two types of parameter passing in this course: [[pass by value]], and [[pass by reference]]. When parameters are passed by value, a *copy* of them is passed to the function, e.g.:

```cpp
int f(int n) {  // n is passed by value
	n++;
	return n + 2;
}

int main() {
	int a = 5;
	cout << f(a) << "\n"; // prints 7
	cout << a << "\n";    // prints 5 (a is NOT changed)
}
```

Note that the variable `a` is *not* changed in this example.

[[Pass by reference]] passes the actual variable to the function *without* making a copy. There are two main reasons for using [[pass by reference]]:
1. [[Pass by reference]] can save memory. For example, if you use pass by value to pass a string to a function, then the string will be copied. If the string is big, making the copy could take a significant time and memory. With pass by reference, no copy would be made of the string.
2. [[Pass by reference]] lets you modify the value of a variable.

Compare this example to the one above:

```cpp
int f(int& n) {  // n is passed by reference: note the &
	n++;
	return n + 2;
}

int main() {
	int a = 5;
	cout << f(a) << "\n"; // prints 7
	cout << a << "\n";    // prints 6 (a is changed)
}
```

An important variation of [[pass by reference]] is [[pass by constant reference]]. [[Pass by constant reference]] passes a parameter by reference, but does *not* allow the function to modify it any way. In many cases, [[pass by constant reference]] is the best way to pass parameters because it is efficient and avoids the problem of functions changing variables that appear in other functions. For example, the string `s` is passed by conference reference:
```cpp
int spaces(const string& s) {
	int num_spaces = 0;
	for(char c : s) {
		if (c == ' ') num_spaces++;
	}
	return num_spaces;
}
```

### void Functions
If a function doesn't return a value, then its return type can be `void`:

```cpp
void fun_name(T1 param_1, T2 param_2, ..., Tn param_n) {
	// ... C++ code ...
}
```

It's okay to put  a `return;` (with no value returned) statement in a `void` function. It will cause the function to immediately end.

### Local Variables
A variable defined inside a function is called a [[local variable]]. For example, `num_spaces` is a local variable in the `spaces` function:
```cpp
int spaces(const string& s) {
	int num_spaces = 0;  // local variable
	for(char c : s) {
		if (c == ' ') num_spaces++;
	}
	return num_spaces;
}
```

Also, the passed-in parameter `s` is treated as if it were local to `spaces` as well.

Local variables are created when their function is called, and then they are automatically deleted when their function ends. Code outside of the function cannot read or write the values of local variables.


## Includes and the Standard Namespace
C++ has a large standard library of useful functions and classes. They are in a [[namespace]] called `std` (short for "standard"), and also spread across a series of header file that you must include. For example:

```cpp
#include <iostream>

int main() {
	std::cout << "Hello, world!\n";   // std:: is required!
}
```

`cout` is a standard value, and because it's in the `std` [[namespace]] we must access using the prefix `std::`.

To avoid writing `std::` a lot, we'll often use a `using` statement like this:

```cpp
#include <iostream>

using namespace std;

int main() {
	cout << "Hello, world!\n";   // std:: no longer required
}
```

The `using` statement tells C++ to check the `std` [[namespace]] if it runs into a name that it doesn't know about. This lets us write `cout` instead of `std::cout`.

We should point out that the way C++ uses `#include` to include declarations of functions is relatively simple, i.e. they do textual inclusion.  While this approach is simple, it causes some problems. For example, compile-times of large C++ program can be in the *hours*, in part due to the repeated `#include` statement. C++20 has added *modules* to C++ which solves many of the problems of `#include`. The hope is that modules will catch on and`#include` statements can eventually be phased out of C++ entirely.

## Practice Questions
1. What's the name of the original inventor and implementer of C++?
2. What programming language did C++ get the idea of objects and classes from?
3. Name two kinds of applications that C++ would probably be good for writing. Name two sorts of programs that C++ would probably not be good for writing. Justify your answers.
4. What does it mean when we say that C++ is a [[statically typed language]]?
5. Why are we using `make` to compile our programs instead of calling `g++` directly?
6. Give two examples of a C++ source code comment, using the two different comment notations.
7. If `a` is a `bool` variable with the value `false`,  and `b` is a `bool` variable with the value `false`, then what does `(a || b) && (!c)` evaluate to?
8. In your own words, describe what short-circuit evaluation of boolean expressions means.
9. Assuming it compiles, what does this code fragment print? 
   ```cpp
   int n;
   cout << n;
   ```
8. Explain how the `auto` keyword works in C++. Give an example of how to use it.
9. Write a struct that represents a book with a title, author, and number of pages.
10. Define an enum class that represents the three SFU semesters: fall, winter, and spring.
11. What types of variables can a `switch` statement switch on?
12. In a `switch` statement, why do you need to end each case with a `break`?
13. In a `switch` statement, what is the name of the "catch-all" case?
14. In your own words, describe how while-loops and do-while loops work, and how they differ.
15. What is the general form of a C-style for-loop?
16. Give an example of how to use a for-each style loop.
17. How many different values can a C++ function return?
18. In your own words, explain how *pass by reference* parameter passing works, and also how *pass by value* parameter passing works. When would you use each?
19. In your own words, explain [[pass by constant reference]] parameter passing. Give an example of where it could be used.
20. What is a namespace? In what namespace is `cout`?