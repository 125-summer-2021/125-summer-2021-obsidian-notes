[[Template Functions]], also known as [[generic function|generic functions]], are C++ functions where one, or more, of the input or output values have an unspecified type.

## A Generic Swap Function
For example, here are two implementations of a function for swapping values of a variables:

```cpp
void swap(int& a, int& b) {
    int temp = a;
    a = b;
    b = temp;
}

void swap(string& a, string& b) {
    string temp = a;
    a = b;
    b = temp;
}

```

The only difference between these swap functions is that the first uses the type `int`, and the second uses the type `string`. Using a template function, we can write a single swap function that works with both `int`s and `string`s:

```cpp
template<typename T>
void swap(T& a, T& b) {
    T temp = a;
    a = b;
    b = temp;
}
```

The first line of this function is `template<typename T>`, which indicates that the function that follows uses `T` to stand for some type. `T` can be any type that works with `opedrator=`, i.e. `T` has to work with the assignment operators in the body of the function.

> **Note**: You can usually write `template<class T>` instead of `template<typename T>`. 

Even though we don't know exactly what type `T` is, we can see that variables `a`, `b`, and `temp` are all of type `T`. The C++ compiler will check at compile-time that these variables all have the same type.

The exact value of `T` is determined when `swap` is called. For example:

```cpp
int x = 4;
int y = 7;
swap(x, y);          // line 1

string s = "cow";
string t = "rabbit";
swap(s, t);         // line 2
```

Consider line 1. `x` and `y` are both of type `int`, and so when the compiler sees the statement `swap(x, y)`, the variable `T` in the `swap` template function is assigned the value `int`. Similarly, in line 2, `s` and `t` are both of type `string`, and so when the compiler  sees `swap(s, t)` it knows to assign `string` to `T` in the swap function.

Notice that if you called, say, `swap(x, t)`, then the compiler would catch that error because `x` and `t` are of different types, and the `swap` requires that they both be of the same type.

## A Generic Min Function
Here is a templated function that returns the smaller of two values:

```cpp
template<typename T>
T min_of(const T& a, const T& b) {
    if (a < b) return a;
    return b;
}
```

Notice that the return type is `T`, as well as the input variables `a` and `b`. `T` is a type that works with `operator<`, and so this will work with numbers and strings.

Here is a templated function that returns the min value of a vector:

```cpp
template<typename T>
T min_of(const vector<T>& v) {
    assert(v.size() > 0);
    T min = v[0];
    for(int i = 1; i < v.size(); i++) {
        min = min_of(min, v[i]);
    }
    return min;
}
```

The input variable `v` is of type `vector<T>`, i.e. it's a vector of values of type `T`. From looking at the code in the body of the function, we can see that `T` must work with `operator=`, and also `operator<` (because it calls `min_of(a, b)`).

The exact value of the type `T` isn't known until `min_of` is called. For example:

```cpp
vector<double> nums = {1.6, 9.22, -4.2, -5};
cout << min_of(nums); // -5

vector<string> words = {"up", "down", "all", "around"};
cout << min_of(nums); // "all"

vector<vector<int>> vv = {
	{4}, {1, 2, 3}, {5, 6}, {2}
};
cout << min_of(vv); // {1, 2, 3}
```

When `min_of(nums)` is called, the `T` in `min_of(const vector<T>& v)` is set to `double` because that's the type of the values in `nums`. The `T` in `min_of(const T& a, const T& b)` also has it's `T` set to `double`. The compiler automatically determines the value for `T` and checks that the type supports all the necessary operations on it.

Notice that `vv` is a vector of vector. C++ defines a standard `oparator<` and `operator=` for vectors, and so we can call `min_of` on a `vector<vector<int>>`.

## Class Templates
The most common use of templates is probably to implement **generic containers**. For example, `vector<T>` is a generic container, and all the methods in it work with almost any type `T`. The STL provides a number of pre-made generic containers, but you can also write your own. For example, here is a simple implementation of a generic stack:

```cpp
#include <iostream>
#include <string>
#include <vector>
#include <cassert>

using namespace std;

template<typename T>
class Stack {
    vector<T> v;
public:
    Stack() // default constructor
    : v() 
    { }

    Stack(const Stack<T>& other) // copy constructor
    : v(other.v)
    { }

    bool is_empty() const { return v.size() == 0; }

    void push(const T& x) {
        v.push_back(x);
    }

    T peek() const {
        assert(!is_empty());
        return v.back();
    }

    T pop() {
        T top = peek();
        v.pop_back();
        return top;
    }

    void print() const {
        if (is_empty()) {
            cout << "empty stack";
        } else {
            for(const T& x : v) cout << x << " ";
        }
    }

    void println() {
        print();
        cout << "\n";
    }

}; // class Stack

// This print function is inefficient because it makes a copy of the passed-in
// stack. If the header were instead print(const Stack<T>& s), then the
// function wouldn't compile because s.pop() modifies s.
//
// The print method in Stack<T> is more efficient because it doesn't need to
// make a copy of the stack.
template<typename T>
void print(Stack<T> s) {
    if (s.is_empty()) {
        cout << "empty stack";
    } else {
        while (!s.is_empty()) {
            T val = s.pop();
            cout << val << " ";
        }
    }
}

template<typename T>
void println(const Stack<T>& s) {
    print(s);
    cout << "\n";
}

int main() {
  Stack<int> a;
  a.println();
  a.push(5);
  a.push(18);
  a.push(-3);
  a.println();

  Stack<string> b;
  b.println();
  b.push("cat");
  b.push("dog");
  b.push("mouse");
  b.push("parrot");
  b.println();
} // main
```

Generic containers are so useful that they alone are the reason a programming might add support for generics. They let you write general-purpose code that can work with a wide variety data types. 

## Advanced C++ Templates
We've only scratched the surface of the use of templates in C++: templates are one of the distinctive and most-used features of modern C++ code libraries. The C++ library uses templates throughout, and a number of changes to the base C++ language have been made over the years to better support programming with templates.

While some programmers like templates for their power and generality, most programmers agree that they are quite complex and take extra care to use. Writing general-purpose code that works with *any* type of data that makes sense is more abstract and challenging than writing code for specific types.

## Questions
1. Here is another way to swap two `int`s:

   ```cpp
   void swap2(int& a, int& b) {
      a = a - b;
      b = a + b;
      a = b - a;
   }
   ```
   
   Convert this into a templated function like `swap` in the notes. Why is this version *not* as general as the one in the notes?
 
2. Implement a templated version of `max_of(a, b)` that returns the bigger of `a` and `b`, and also `max_of(v)` that returns the biggest element in `v` (which has type `vector<T>`).
3. Write a function called `identity(x)` that just returns a copy of `x`. Can you write this function without using templates?
4. Using the `Stack` class as defined above, explain why you **can't** create a `Stack<Stack<int>>`. How would you modify `Stack` so that you could create a `Stack<Stack<int>>`?