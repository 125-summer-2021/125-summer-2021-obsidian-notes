---
aliases: [OOP]
---

## Introduction
Object-oriented programming (OOP) is a popular approach to programming where data and code is combined together in *objects*. In C++, an object is a value that contains both variables *and* functions. A function in an object is called a [[method]], and methods have direct access to all the variables in their object. We sometimes refer to the variables and methods in a object as **members** of the object.

In C++, we create objects using a `struct` or a `class`. We'll usually use a `class` for objects as to emphasize OOP. Inside a `class` we list all the variables and methods that are part of the class. Then we can create objects in the same way that we create structs.

As we will see below, objects have many useful features that make them safer and easier to use than plain structs. Two of the most useful features of objects are [[constructor|constructors]] and [[destructor|destructors]]. An object [[constructor]] specifies how to create an object, and an object [[destructor]] specifies how to delete the object. Together, they give us a lot of control over objects, and make it much easier to prevent certain kinds of errors.

Objects are *not* classes. A class describes the contents of an object, but is not itself an object. In C++, exist at compile-time, but are usually not accessible at run-time. Similarly, objects usually only exist at run-time, and cannot be accessed at compile time.

In C++, classes act like newly created *types*. Objects are the values of those types. For example, the standard C++ `string` type is defined as a class.


## Example: Dates
### Date as a Struct
Take a look at [[date_struct_cpp|date_struct.cpp]].  It's a typical example on non-OOP program design. It implements a date as a `struct`, and provides a few helper functions. `Date` itself contains only three variables and no methods.

### Date with Public Member Variables
Compare consider [[date_class_cpp|date_class.cpp]], which uses some object-oriented features. It does the same thing as [[date_struct_cpp|date_struct.cpp]], but in a different way. In particular:

- `Date` uses a `class` instead of a `struct`. The first line of the `Date` class is `public:`, which mean all the code that follows can be accessed by any code in the program. If you comment-out `public:` the program will no longer compile (try it!).
- `Date` has a [[constructor]], i.e. a special method inside the `Date` class whose job is to initialize the `Date` variables:
  
  ```cpp
  // constructor: assigns values to member variables
  Date(int d, int m, int y)
  : day(d), month(m), year(y)  // initialization list
  { }
  ```
  
  Notice that the name of the constructor is `Date`, which is the same name as the class. Constructors *always* have the same name as their class.

  The constructor does *not* have a return type (not even `void`). Constructors *never* have an explicitly listed return type: they always construct an object of the type of the class.

  This [[constructor]] happens to use an [[initialization list]] to assign initial values to the variables  of `Date`. Initialization lists can only be used with constructors. You should always use initialization lists if you can: they ensure that the variables of the class/struct are initialized before they are used, and they also allow you to use values that don't work the [[assignment operator]], `operator=`.

  In this particular example constructor the body is empty, but in general you
  can put whatever code you like in to the constructor body. 

- `is_valid()`, `print()`, and `println()` are all declared inside of `Date`.
  Functions declared inside a class like this are called [[method|methods]], and,
  importantly, methods have access to all the members of the class. So we
  *don't* need to pass a `Date` variable a method: it automatically has access
  to the variables in the `Date` class.

- `is_valid()`, `print()`, and `println()` are all [[const method]]s, i.e.
  when they are called they *never* modify any variables in their `Date`
  object (they just read them, never write them).

  A method is declared `const` by putting the keyword `const` after the header
  and before the body.

  In practice, [[const method]]s are very common and useful and make you programs easier to understand. For example, if a class has only [[const method]]s, then it is easy to use it in a [[concurrent program]] where multiple CPUs might be running at the same time. Non-const methods generally have a much harder time working correctly in a [[concurrent program]], since two different CPUs might try to modify the same value at the same time. That can never happen if an object that has only [[const method]]s.

  Sometimes const methods are called [[const method|non-mutating methods]]. And a method that is not const, i.e. a method that when called might modify a value in the object, is called a [[mutating method]].


### Date with Private Member Variables
One of the most common patterns in object-oriented programming is to make the member variables of a class *private*, and to provide *public methods* for accessing those variables in a controlled way. A method that returns the value of a member variable is called a [[getter]], and a method that assigns a value to a member variable is called a [[setter]]. 

[[date_class_private_cpp|date_class_private.cpp]] demonstrates how the single change of making the member variables of `Date` private has a significant impact on the entire program. The `Date` class now looks like this:

```cpp
class Date {
    int day;   // 1 to 31
    int month; // 1 to 12
    int year;  // e.g. 2017

public:
    Date(int d, int m, int y)
    : day(d), month(m), year(y)  // initialization list
    { }

    bool is_valid() const {
		// ...
    }

	// ...

    // Getters: methods that return the value of a variable. Getters should
    // not modify the object, and so they are declared const.
    int get_day()   const { return day;   }
    int get_month() const { return month; }
    int get_year()  const { return year;  }

    // Setters: methods that change the value of a variable. Setters are never
    // const because they modify the object.
    void set_day(int d)   { day   = d; }
    void set_month(int m) { month = m; }
    void set_year(int y)  { year  = y; }

}; // class Date
```

For each of the now private member variables in `Date`, we add a corresponding
[[getter]] and [[setter]]. For example, for `month`, we add the [[getter method|getter]] `get_month()` that returns the value of `month`, and the [[setter method|setter]] `set_month(int m)` that assigns the value of `m` to `month`. The *variables* are private, but the methods are *public*, and so we can read/write them through the methods.

The way we've written the [[getter|getters]] and [[setter|setters]] gives us total access to read or write the member variables, and so it's not much different than if we'd made the variables public. But an advantage of this approach is that we can modify the methods to do whatever we like. For instance, we could add error-checking to `set_month`:

```cpp
void set_month(int m) { 
	if (m < 1 || m > 31) {
		cmpt::error("invalid month");
	}
	month = m; 
}
```

Or we could add a "helper" [[setter]] to make the `Date` class more convenient to use:

```cpp
void set_month(const string& m) {
	if (m == "JAN") {
		month = 1;
	} else if (m == "FEB") {
		month = 2;
	} 

	// ...

	} else if (m == "DEC") {
		month = 12;
	} else {
		cmpt::error("unknown month");
	}
}
```

### friend Functions
Using the `friend` keyword, you can give functions permission to access the private members of a class. For example:

```cpp
class Point {
    double x;                  // x and y are private
    double y;
public:
    Point(double a, double b)  // constructor
    : x(a), y(b)
    { }

    double get_x() const { return x; }
    double get_y() const { return y; }

    void print() const { cout << x << ", " << y << "\n"; }
    
    friend void set_to_origin(Point& p);
};

void set_to_origin(Point& p) {   // friend function
    p.x = 0.0;
    p.y = 0.0;
}

void point1() {
    Point p(10, -3.4);
    p.print();
    set_to_origin(p);
    p.print();
}
```

In this example, the `Point` class declares `set_to_origin` to be a [[friend function]], which means `set_to_origin` is allowed to read/write private variables in its parameter `p`.

Generally, you should avoid `friend` functions. It's usually better to write a method instead of a `friend`.

### A Read-only Date Class
Another common object-oriented design pattern is *don't use [[setter|setters]]*: make the `Date` class read-only. This simplifies the `Date` class, which makes it easier to use correctly.

Here is an excerpt from [[date_class_readonly_cpp|date_class_readonly.cpp]]:

```cpp
class Date {
    int day;   // 1 to 31
    int month; // 1 to 12
    int year;  // e.g. 2017

public:
    // constructor: assigns values to member variables
    Date(int d, int m, int y)
    : day(d), month(m), year(y)  // initialization list
    { }

    // ...

    // Getters: methods that return the value of a variable. Getters should
    // not modify the object, and so they are declared const.
    int get_day()   const { return day;   }
    int get_month() const { return month; }
    int get_year()  const { return year;  }

    //
    // No setters or other non-const methods ... Dates are read-only.
    //

    // returns a new Date exactly one year after this one
    Date next_year() const {
        return Date{day, month, year + 1};
    }

}; // class Date
```

Every member of this class is either private or const: once you construct a `Date` object, there's no way to change it. If you want a new date, then you must construct another one using the `Date` [[constructor]], or a helper method such as `next_year()`.


## Other Kinds of Constructors
A class/struct can have more than one [[constructor]], and some [[constructor]]s have special names. For example, consider this `Point` class:

```cpp
struct Point {
    double x = 0;  // member initialization
    double y = 0;  // i.e. x and y are set to 0 by default

    // default constructor
    Point() // x and y are initialized above
    { }

    Point(double a, double b)
    : x(a), y(b)         // initializer list
    { }

    // copy constructor
    Point(const Point& other)
    : Point(other.x, other.y)  // constructor delegation
    { }

    // ...

}; // struct Point

// ...
```

`Point` has three different [[constructor]]s. In addition to a [[constructor]] that
takes an x and y value as input, it has a:

- [[Default constructor]], i.e. a constructor that has no input parameters. In this particular example, the default constructor does nothing since the member variables `x` and `y` are initialized using [[member initialization]].
- [[Copy constructor]], i.e. a constructor that takes another `Point` as input and makes a copy of it. Notice that the [[copy constructor]] in this example uses [[constructor delegation]].

[[Default constructor]]s and [[copy constructor]]s follow a few special rules. For example:

- If your class doesn't define a [[default constructor]] or [[copy constructor]], then
  C++ *automatically* creates an *implicit* default constructor (that does nothing), and an *implicit* [[copy constructor]] that copies the member variables in the class.
- Many standard functions and containers, such as `vector<T>`, **require** that the class/struct `T` has, at least, a default constructor and a [[copy constructor]].

## Destructors
A [[destructor]] is a special method in a class/struct that is *automatically* called when its object is de-allocated. A programmer cannot call a [[destructor]] whenever they like: [[destructor]]s are always called *automatically*.

While a class can have as many constructors as it wants, a class can only have *one* [[destructor]]. The name of a [[destructor]] is *always* the name of the class with a `~` at the start. [[Destructor]]s *never* have input parameters. For example, for a class named `int_list`, the header of its destructor is `~int_list()`.

[[Destructor]]s and [[constructor]]s go together: when you create an object you must
call a [[constructor]] for it, and when the object is deleted, it's [[destructor]] is automatically called.

Inside a [[destructor]] you can put almost any code you want to run when the object is deleted. Often, this involves "clean up" code, e.g. code that calls `delete`/`delete[]` on variables that were `new`-ed in the constructor. Together, constructors and [[destructor]]s are a good way of managing free store memory: the [[constructor]] calls `new`, and the [[destructor]] guarantees `delete`/`delete[]` is called correctly.

An important fact about [[destructor]]s is that they *can't* be called explicitly by the  programmer. This prevents the programmer from, for example, accidentally de-allocating variables in an object before the program is finished using them.

Here's an example of a class with a [[destructor]] from [[point_destructor_this_cpp|point_destructor_this.cpp]]:

```cpp
#include "cmpt_error.h"
#include <iostream>
#include <string> 

using namespace std;

struct Point {
    double x    = 0;  // member initialization
    double y    = 0;  // i.e. x and y are set to 0 by default
    string name = "no name";

    // default constructor
    Point()                                // x and y are initialized above
    { }

    Point(double a, double b, const string& n)
    : x(a), y(b), name(n)                  // initializer list
    { }

    // copy constructor
    Point(const Point& other)
    : Point(other.x, other.y, other.name)  // constructor delegation
    { }

    // destructor
    ~Point() {
        cout << "Point: " << name << " destructor called\n";
    }

}; // struct Point

int main() {
    Point p{2, -4, "p"};
    p.x = 0;
}
```

The [[destructor]] is called `~Point()`, and all it does is print a message every time a `Point` object is deleted. Such messages can be quite useful for debugging.


## The this Pointer
C++ automatically adds [[this pointer]] to every object. It is a special pointer called `this` that points to the object. Here's an example of how you could use `this` in the `Point` class from above (see [[point_destructor_this_cpp|point_destructor_this.cpp]]): 

```cpp
struct Point {
    double x    = 0;  // member initialization
    double y    = 0;  // i.e. x and y are set to 0 by default
	string name = "no name";

	// ...

    string to_str() const {
        return "(" + to_string(this->x) + ", "
                   + to_string(this->y) + ", "
                   + this->name
                   + ")";
    }

    void print() const {
        cout << to_str();
    }

    void println() const {
        print();
        cout << "\n";
    }

}; // struct Point
```

The `to_str` method is accessing `x` and `y` through the `this` pointer using the standard pointer `->` notation. In this particular cases there's no need to use `this`(we could just write `x` and `y`), but there are situations where `this` is required.


## Practice Questions
1. In your own words, explain the difference between a 
   - `struct` and a `class`
   - function and a [[method]]
   - a [[const method]], and a [[mutating method]]
2. In your own words, describe
   - what *public* and *private* mean when used in a struct/class
   - what a [[constructor]] is, and how to write one and call one
   - what an [[initialization list]] is, and when and why it is used
   - why using [[const method]]s is a good idea
   - what [[getters]] and [[setters]] are
3. As noted in the comments for [[date_struct_cpp|date_struct.cpp]], `is_valid(const Date& d)` sometimes returns the wrong answer. How would you fix so that it works correctly for more (ideally all!) dates?
4. Implement the following for `Date` in [[date_class_readonly_cpp|date_class_readonly.cpp]]:
   - `operator==(const Date& a, const Date& b)` returns `true` just when `a`
     and `b` are the same date, and `false` otherwise
   - `operator!=(const Date& a, const Date& b)` returns `true` just when `a`
     and `b` are different dates, and `false` when they're the same
   - `operator<(const Date& a, const Date& b)` returns `true` just when `a`
     occurs before `b`, and `false` otherwise
5. In your own words, describe what a [[destructor]] does. When it is called? Why can't a programmer call a [[destructor]] whenever they want?
6. In your own words, describe the `this` pointer.

