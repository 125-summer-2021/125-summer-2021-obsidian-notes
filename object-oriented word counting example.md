The files below show how a non-objected oriented program can be converted into an object-oriented one.

## count_chars1.cpp
Slightly incorrect code for counting characters, lines, and words. It over-counts the number of words because it treats every whitespace character as a word.

```cpp
// count_chars1.cpp

//
// Counts the number of characters, lines, tabs, and words in a file. The file
// is read from cin, so you use it like this:
//
//     $ ./count_chars1 < austenPandP.txt 
//     #chars: 704158
//     #lines: 13427
//     #tabs : 0
//     #words: 127342
//
// However, there is a slight bug in how words are counted. Every space causes
// the word count to be incremented, and so the word count is actually a count
// of the number of spaces. If there's more than one whitespace character
// between words, then this can over-count the number of words. We see that
// this is indeed the case when we use the standard word count utility wc to
// count the number of words:
//
//     $ wc -w austenPandP.txt 
//     124580 austenPandP.txt
//
// The program below says there are 127342 words, which is small but
// non-trivial over-counting.
//

#include <iostream>

using namespace std;

int main() {
    // initialize the count variables
    int num_chars = 0;
    int num_lines = 0;
    int num_tabs  = 0;
    int num_words = 0;

    // process cin one character at a time, incrementing the appropriate count
    // variables
    char c;
    while (cin.get(c)) {
        switch (c) {
            case '\n': num_chars++;
                       num_lines++;
                       num_words++;
                       break; // break is needed to prevent fallthrough
            case '\t': num_chars++;
                       num_tabs++;
                       break; // break is needed to prevent fallthrough
            case ' ' : num_chars++;
                       num_words++;
                       break; // break is needed to prevent fallthrough
            default  : num_chars++; // default case catches all other characters
                                    // break is not needed on last case
        } // switch
    } // while

    // report the results
    cout << "#chars: " << num_chars << "\n";
    cout << "#lines: " << num_lines << "\n";
    cout << "#tabs : " << num_tabs  << "\n";
    cout << "#words: " << num_words << "\n";
}
```

## count_chars2.cpp
Fixes the bug in the previous version by adding a boolean variable that tracks if the previous character was a whitespace character. A new word is counted only when a character is whitespace *and* the previous character is *not* whitespace.

```cpp
// count_chars2.cpp

//
// Fixes the word over-counting bug in count_chars1.cpp. 
//
// It does this by using a boolean varaible to track if the last character was
// a whitespace character. The word count is only incremented at a whitespace
// character is preceded by a non-whitespace character. This stops multiple
// whitespace characters in a row from being counted as multiple words.
//
// The file is read from cin, so you use it like this:
//
//     $ ./count_chars2 < austenPandP.txt
//

#include <iostream>

using namespace std;

int main() {
    // initialize the count variables
    int num_chars = 0;
    int num_lines = 0;
    int num_tabs  = 0;
    int num_words = 0;

    // process cin one character at a time, incrementing the appropriate count
    // variables
    char c;
    bool last_char_whitespace = true;
    while (cin.get(c)) {
        num_chars++;
        switch (c) {
            case '\n': num_lines++;
                       if (!last_char_whitespace) num_words++;
                       last_char_whitespace = true;
                       break;
            case '\t': num_tabs++;
                       if (!last_char_whitespace) num_words++;
                       last_char_whitespace = true;
                       break;
            case ' ' : if (!last_char_whitespace) num_words++;
                       last_char_whitespace = true;
                       break;
            default  : last_char_whitespace = false; // default catches all other characters
                       // break is not needed on last case
        } // switch
    } // while

    // report the results
    cout << "#chars: " << num_chars << "\n";
    cout << "#lines: " << num_lines << "\n";
    cout << "#tabs : " << num_tabs  << "\n";
    cout << "#words: " << num_words << "\n";
}
```

## count_chars3.cpp
Puts the count variables into a struct. Also puts the printing code into a function named `print_results`. The name is self-descriptive and so no comment is needed before it in main.
 
```cpp
// count_chars3.cpp

//
// Replaces the count variables of count_chars2.cpp with a struct. Also added
// a function called print_results to make main a little simpler.
//
// The file is read from cin, so you use it like this:
//
//     $ ./count_chars3 < austenPandP.txt
//

#include <iostream>

using namespace std;

struct Count {
   int chars = 0;
   int lines = 0;
   int tabs  = 0;
   int words = 0;
};

void print_results(const Count& num) {
    cout << "#chars: " << num.chars << "\n";
    cout << "#lines: " << num.lines << "\n";
    cout << "#tabs : " << num.tabs  << "\n";
    cout << "#words: " << num.words << "\n";
}

int main() {
    // initialize the count object
    Count num;

    // process cin one character at a time, incrementing the appropriate count
    // variables
    char c;
    bool last_char_whitespace = true;
    while (cin.get(c)) {
        num.chars++;
        switch (c) {
            case '\n': num.lines++;
                       if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            case '\t': num.tabs++;
                       if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            case ' ' : if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            default  : last_char_whitespace = false;
        } // switch
    } // while

    print_results(num);
}
```

## count_chars4.cpp
Instead of reading the file from `cin`, it reads it from an `ifstream` object.

```cpp
// count_chars4.cpp

//
// The same as count_chars3.cpp, but instead of reading the file from cin the
// file is read using an ifstream object.
//
// You use it like this:
//
//     $ ./count_chars4
//

#include <iostream>
#include <fstream>

using namespace std;

struct Count {
   int chars = 0;
   int lines = 0;
   int tabs  = 0;
   int words = 0;
};

void print_results(const Count& num) {
    cout << "#chars: " << num.chars << "\n";
    cout << "#lines: " << num.lines << "\n";
    cout << "#tabs : " << num.tabs  << "\n";
    cout << "#words: " << num.words << "\n";
}

int main() {
    ifstream infile("austenPandP.txt");

    // initialize the count object
    Count num;

    // process infile one character at a time, incrementing the appropriate
    // count variables
    char c;
    bool last_char_whitespace = true;
    while (infile.get(c)) {
        num.chars++;
        switch (c) {
            case '\n': num.lines++;
                       if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            case '\t': num.tabs++;
                       if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            case ' ' : if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            default  : last_char_whitespace = false;
        } // switch
    } // while

    print_results(num);
}
```

## count_chars5.cpp
`print_results` is changed from being a function to method of `Count`.

```cpp
// count_chars5.cpp

//
// The same as count_chars4.cpp, but made print_results a method of count.
//
// You use it like this:
//
//     $ ./count_chars5
//

#include <iostream>
#include <fstream>

using namespace std;

struct Count {
    int chars = 0;
    int lines = 0;
    int tabs  = 0;
    int words = 0;

    void print_results() {
        cout << "#chars: " << chars << "\n";
        cout << "#lines: " << lines << "\n";
        cout << "#tabs : " << tabs  << "\n";
        cout << "#words: " << words << "\n";
    }
}; // Count


int main() {
    ifstream infile("austenPandP.txt");

    // initialize the count object
    Count num;

    // process infile one character at a time, incrementing the appropriate
    // count variables
    char c;
    bool last_char_whitespace = true;
    while (infile.get(c)) {
        num.chars++;
        switch (c) {
            case '\n': num.lines++;
                       if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            case '\t': num.tabs++;
                       if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            case ' ' : if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            default  : last_char_whitespace = false;
        } // switch
    } // while

    num.print_results();
}
```

## count_chars6.cpp
The `ifstream` object is moved into `Count` as a variable.

```cpp
// count_chars6.cpp

//
// The same as count_chars5.cpp, but ifstream object has been moved into
// Count.
//
// You use it like this:
//
//     $ ./count_chars6
//

#include <iostream>
#include <fstream>

using namespace std;

struct Count {
    ifstream infile = ifstream("austenPandP.txt");

    int chars = 0;
    int lines = 0;
    int tabs  = 0;
    int words = 0;

    void print_results() {
        cout << "#chars: " << chars << "\n";
        cout << "#lines: " << lines << "\n";
        cout << "#tabs : " << tabs  << "\n";
        cout << "#words: " << words << "\n";
    }
}; // Count


int main() {
    // initialize the count object
    Count num;

    // process infile one character at a time, incrementing the appropriate
    // count variables
    char c;
    bool last_char_whitespace = true;
    while (num.infile.get(c)) {
        num.chars++;
        switch (c) {
            case '\n': num.lines++;
                       if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            case '\t': num.tabs++;
                       if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            case ' ' : if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            default  : last_char_whitespace = false;
        } // switch
    } // while

    num.print_results();
}
```

## count_chars7.cpp
Adds a constructor to `Count` that initializes the `ifstream` object.

```cpp
// count_chars7.cpp

//
// The same as count_chars6.cpp, but with a constructor added to Count so that
// different files can be read in.
//
// You use it like this:
//
//     $ ./count_chars7
//

#include <iostream>
#include <fstream>

using namespace std;

struct Count {
    ifstream infile;

    int chars = 0;
    int lines = 0;
    int tabs  = 0;
    int words = 0;

    // constructor
    Count(const string& fname) 
    : infile(fname)  // initialization list
    { }

    void print_results() {
        cout << "#chars: " << chars << "\n";
        cout << "#lines: " << lines << "\n";
        cout << "#tabs : " << tabs  << "\n";
        cout << "#words: " << words << "\n";
    }
}; // Count


int main() {
    // initialize the count object
    Count num("austenPandP.txt");

    // process infile one character at a time, incrementing the appropriate
    // count variables
    char c;
    bool last_char_whitespace = true;
    while (num.infile.get(c)) {
        num.chars++;
        switch (c) {
            case '\n': num.lines++;
                       if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            case '\t': num.tabs++;
                       if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            case ' ' : if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            default  : last_char_whitespace = false;
        } // switch
    } // while

    num.print_results();
}
```

## count_chars8.cpp
The code for processing the file is moved into a `Count` method named `process_file`.

```cpp
// count_chars8.cpp

//
// The same as count_chars7.cpp, but now the code for processing the file is
// written as a method inside Count.
//
// You use it like this:
//
//     $ ./count_chars8
//

#include <iostream>
#include <fstream>

using namespace std;

struct Count {
    ifstream infile;

    int chars = 0;
    int lines = 0;
    int tabs  = 0;
    int words = 0;

    // constructor
    Count(const string& fname) 
    : infile(fname)  // initialization list
    { }

    void print_results() {
        cout << "#chars: " << chars << "\n";
        cout << "#lines: " << lines << "\n";
        cout << "#tabs : " << tabs  << "\n";
        cout << "#words: " << words << "\n";
    }

    // process infile one character at a time, incrementing the appropriate
    // count variables
    void process_file() {
        char c;
        bool last_char_whitespace = true;
        while (infile.get(c)) {
            chars++;
            switch (c) {
                case '\n': lines++;
                           if (!last_char_whitespace) words++;
                           last_char_whitespace = true;
                           break;
                case '\t': tabs++;
                           if (!last_char_whitespace) words++;
                           last_char_whitespace = true;
                           break;
                case ' ' : if (!last_char_whitespace) words++;
                           last_char_whitespace = true;
                           break;
                default  : last_char_whitespace = false;
            } // switch
        } // while
    } // process_file
}; // Count


int main() {
    Count num("austenPandP.txt");
    num.process_file();
    num.print_results();
}
```
  
## count_chars9.cpp
The usage of the `Count` object is changed to that `process_file` does not need to be called explicitly. Instead, it is called in the constructor.

```cpp
// count_chars9.cpp

//
// Based on count_chars8.cpp, but now its easier to use. The programmer just
// creates a Count object, and then calls the getters. process_file is called
// inside the constructor, and so the programmer doesn't need to worry about
// calling it.
//
// The variables have been made private so the user can't accidentally (or
// intentionally!) change them. The user can only access them through the
// getters.
//
// The process_file method has been made private to ensure it is only called
// once in the constructor.
// 
// You use it like this:
//
//     $ ./count_chars9
//

#include <iostream>
#include <fstream>

using namespace std;

struct Count {
private:
    ifstream infile;

    int chars = 0;
    int lines = 0;
    int tabs  = 0;
    int words = 0;

    // process infile one character at a time, incrementing the appropriate
    // count variables
    void process_file() {
        char c;
        bool last_char_whitespace = true;
        while (infile.get(c)) {
            chars++;
            switch (c) {
                case '\n': lines++;
                           if (!last_char_whitespace) words++;
                           last_char_whitespace = true;
                           break;
                case '\t': tabs++;
                           if (!last_char_whitespace) words++;
                           last_char_whitespace = true;
                           break;
                case ' ' : if (!last_char_whitespace) words++;
                           last_char_whitespace = true;
                           break;
                default  : last_char_whitespace = false;
            } // switch
        } // while
    } // process_file

public:
    // constructor
    Count(const string& fname) 
    : infile(fname)  // initialization list
    { 
        process_file();
    }

    int num_chars() {
        return chars;
    }

    int num_lines() {
        return lines;
    }

    int num_tabs() {
        return tabs;
    }

    int num_words() {
        return words;
    }

    void print_results() {
        cout << "#chars: " << num_chars() << "\n";
        cout << "#lines: " << num_lines() << "\n";
        cout << "#tabs : " << num_tabs()  << "\n";
        cout << "#words: " << num_words() << "\n";
    }
}; // Count


int main() {
    Count count("austenPandP.txt");
    count.print_results();
    cout << "Average words per line: " 
         << count.num_words() / count.num_lines() << "\n";
}
```

## count_chars10
A modification of the previous version to be "lazy", i.e. `process_file` is only called if needed. This requires the addition of a boolean variable to track if `process_file` has been called. The **usage** of `Count` is exactly the same as in the previous version.

```cpp
// count_chars10.cpp

//
// The same as count_chars9.cpp, but it has been made *lazy*. Instead of
// calling process_file in the constructor, process_file is now called in the
// getters *if* necessary.
//
// This can improve the performance of Count objects. If you create a Count
// object but then never use any of its getters, then the file is never read.
// 
// The implementation of Count now includes a boolean varaible called
// is_processed that keeps track of whether or not process_file has been
// called. Each getter checks it before returning its value.
//
// While this does make the implementation of the Count a little more
// complicated, the *use* of Count is exactly the same as before. The user
// just creates a Count object and calls its getters, and they don't need to
// worry about the implementation details.
//
// You use it like this:
//
//     $ ./count_chars10
//

#include <iostream>
#include <fstream>

using namespace std;

struct Count {
private:
    ifstream infile;
    bool is_processed = false;

    int chars = 0;
    int lines = 0;
    int tabs  = 0;
    int words = 0;

    // process infile one character at a time, incrementing the appropriate
    // count variables
    void process_file() {
        char c;
        bool last_char_whitespace = true;
        while (infile.get(c)) {
            chars++;
            switch (c) {
                case '\n': lines++;
                           if (!last_char_whitespace) words++;
                           last_char_whitespace = true;
                           break;
                case '\t': tabs++;
                           if (!last_char_whitespace) words++;
                           last_char_whitespace = true;
                           break;
                case ' ' : if (!last_char_whitespace) words++;
                           last_char_whitespace = true;
                           break;
                default  : last_char_whitespace = false;
            } // switch
        } // while
        
        is_processed = true;
    } // process_file

public:
    // constructor
    Count(const string& fname) 
    : infile(fname)  // initialization list
    { }

    int num_chars() {
        if (!is_processed) process_file();
        return chars;
    }

    int num_lines() {
        if (!is_processed) process_file();
        return lines;
    }

    int num_tabs() {
        if (!is_processed) process_file();
        return tabs;
    }

    int num_words() {
        if (!is_processed) process_file();
        return words;
    }

    void print_results() {
        cout << "#chars: " << num_chars() << "\n";
        cout << "#lines: " << num_lines() << "\n";
        cout << "#tabs : " << num_tabs()  << "\n";
        cout << "#words: " << num_words() << "\n";
    }
}; // Count


int main() {
    Count count("austenPandP.txt");
    count.print_results();
    cout << "Average words per line: " 
         << count.num_words() / count.num_lines() << "\n";
}
```

## Suggested extra features
1. Add a `string` variable to the `Count` that stores the name of the file. Add a getter called `get_name()` that returns it, and modify `print_results` to also print the name of the file.

2. If the file name pass to `Count` isn't found then, instead of crashing, print a helpful and friendly error message.

3. Add these two methods to `Count`:

   - `avg_word_per_line()` returns the average number of words per line

   - `avg_chars_per_word()` returns the average number of characters per word

   When implementing these make sure to call the getter methods for the various counts. Don't use the count variables directly, since they might not yet contain the correct counts.

4. Look up on the web how to read files from the command line using `argc` and `argv` parameters to `main`. Let the user pass one or more file names to the function when it runs, and print results for each of them. In addition, print the overall sum of all characters, lines, tabs, and words in the files. Do this for all the files that are found. For files that don't exist, just print a message like "file <name> not found" and don't include them in the final results.