
## The Problem
Suppose you want to calculate $a^n$, i.e. $a$ to the power of $n$ where $a$ is an integer, and $n$ is a large *positive* integer. For example, $a^{65537}$ is [often calculated in the RSA cryptosystem](http://en.wikipedia.org/wiki/65537_(number)). [RSA](https://en.wikipedia.org/wiki/RSA_(cryptosystem)) is one popular way of securely doing things like online banking and shopping.

## Some Special Cases
There are a few simple cases that can handled individually:

- $a^0$ is 1 if $a \neq 0$
- $0^n$ is 0 if $n \neq 0$
- $1^n$ is 1 if $n \neq 0$
- $a^1$ is $a$ if $a \neq 0$
- $0^0 = 1$ ([although sometimes it is said to be undefined](https://en.wikipedia.org/wiki/Exponentiation#Zero_to_the_power_of_zero))

In what follows, when discussing how to calculate $a^n$, we won't worry about these special cases.

## The Obvious Approach (Iterative)
The "obvious" way to calculate $a^n$ is to multiple $a$ by itself $n-1$ times. Here is how that can be done iteratively i.e. using a loop:

```cpp
// Pre-condition: 
//    n >= 0
// Post-condition: 
//    returns a to the power of n, i.e. a^n
// Note: 
//    The bases cases can be compressed somewhat. However, we list them
//    individually to make it easier to see that all the cases have been 
//    handled.
int power_iter(int a, int n) {
    assert(n >= 0);

    if (a == 0 && n == 0) return 1;
    if (a == 0 && n != 0) return 0;
    if (a != 0 && n == 0) return 1;
    if (a == 1) return 1;
	
    int pow = 1;
    for(int i = 0; i < n; ++i) {
        pow *= a;
    }
    return pow;
}
```

Note the use of [[contract|contracts]] to specify what the function ought to do.

## The Obvious Approach (Recursive)
Here's the same [[algorithm]] implemented using [[recursion]]:

```cpp
int power_recur(int a, int n) {
    assert(n >= 0);

    if (a == 0 && n == 0) return 1;
    if (a == 0 && n != 0) return 0;
    if (a != 0 && n == 0) return 1;
    if (a == 1) return 1;
	
    return a * power_recur(a, n - 1);
}
```

While the source code is shorter than `power_iter`, it's a little less efficient since each call to `power_recur` uses extra [[call stack]] memory.

## Testing `power_iter`
`power_iter` is not too complicated, but it is still useful to do some testing just in case we've made a mistake:

```cpp
void power_iter_test() {
    assert(power_iter(2, 0) == 1);
    assert(power_iter(2, 1) == 2);
    assert(power_iter(2, 2) == 2*2);
    assert(power_iter(2, 3) == 2*2*2);
    assert(power_iter(2, 4) == 2*2*2*2);
    assert(power_iter(2, 5) == 2*2*2*2*2);
    cout << "power_iter_test: all tests passed\n";
}
```

One way to test a power function is to use [[property based testing]], and to test basic properties of powers like these: 
- $a^{m+n} = a^m\cdot a^n$. So `power_iter(a, m+n) == power_iter(a, m) * power_iter(a, n)` should  be true for any non-negative `int`s `m` and `n`.
- $(a^m)^n = a^{mn}$. So `power_iter(power_iter(a, m), n) == power_iter(a, m*n)` should  be true for any non-negative `int`s `m` and `n`.


## Performance of the Obvious Approach
Both the iterative and recursive versions of the obvious approach do the same number of multiplications: 

- $a^2 = a \cdot a$ does 1 multiplication
- $a^3 = a \cdot a \cdot a$ does 2 multiplications
- $a^4 = a \cdot a \cdot a \cdot a$ does 3 multiplications
- ...
- $a^n = a \cdot a \cdot \ldots a \cdot a$ does $n-1$ multiplications (for $n > 1$).

In general, the obvious approach does $n - 1$ multiplications to calculate $a^n$. The running time of a function that implements this approach is *proportional* to the number of multiplications done. For example, calculating $a^{2n}$ takes about twice as many multiplications as $a^n$.

## A Faster Algorithm
The obvious algorithm shows that it is possible to calculate $a^n$ using about $n$ multiplications. But can we do better? Is there an algorithm that does fewer multiplications in general?

It turns out there is. Take a look at this calculation:

- $a \cdot a = a^2$
- $a^2 \cdot a^2 = a^4$

Only 2 multiplications were needed to get $a^4$.  That's one less than the obvious algorithm.

It gets better. For example, $a^4 \cdot a^4 = a^8$, needs only 1 more multiplication for a total of 3 (instead of 7 for the obvious algorithm). $a^8 \cdot a^8 = a^{16}$, needs only 1 more multiplication for a total of 4 (instead of 15 for the obvious algorithm).

These examples show that **repeated squaring** can, at least sometimes, be used to calculate $a^n$ using fewer multiplications than the obvious algorithm.

By repeated squaring we can quickly calculate $a^n$ when $n$ is a power of 2, but what about other values of $n$?

It turns out we can make this trick work with any positive $n$. The idea is distinguish when $n$ is *even*, and when $n$ is *odd*. When $n$ is even, we can use the squaring trick; when it's odd, we can do one extra multiplication and then a squaring.

More precisely, when $n=2k$, to calculate $a^{2k}$, we can square $a^k$ (and recursively calculate $a^k$). When $n=2k+1$, we can't get $a^{2k+1}$ by squaring. However, we can re-arrange it like this: $a^{2k+1} = a \cdot a^{2k}$. So to get $a^{2k+1}$ we can square $a^k$ and multiply that by $a$.

Here's a recursive implementation of this faster power algorithm:

```cpp
int power_recur_fast(int a, int n) {
    assert(n >= 0);

    if (a == 0 && n == 0) return 1;
    if (a == 0 && n != 0) return 0;
    if (a != 0 && n == 0) return 1;
    if (a == 1) return 1;

    int half = power_recur_fast(a, n / 2);
    if (n % 2 == 0) {
        return half * half;
    } else {
        return half * half * a;
    }
}
```

> **Be careful**: The expression `n / 2` in `power_recur_fast` is **integer division**. That means it evaluates to an `int`. So`10 / 2` is 5, and `11 / 2` is also 5 because C++ always chops off (**truncates**) any digits after the decimal point when doing integer division. For example:
 >```
 -5 / 2 == -2
 -4 / 2 == -2
 -3 / 2 == -1
 -2 / 2 == -1
 -1 / 2 == 0
  0 / 2 == 0
  1 / 2 == 0
  2 / 2 == 1
  3 / 2 == 1
  4 / 2 == 2
  5 / 2 == 2
 >```

## Testing `power_recur_fast`
`power_recur` fast is a little  trickier than `power_recur_iter`, and so it is important to test it, e.g.:

```cpp
void power_recur_fast_test() {
    assert(power_recur_fast(2, 0) == 1);
    assert(power_recur_fast(2, 1) == 2);
    assert(power_recur_fast(2, 2) == 2*2);
    assert(power_recur_fast(2, 3) == 2*2*2);
    assert(power_recur_fast(2, 4) == 2*2*2*2);
    assert(power_recur_fast(2, 5) == 2*2*2*2*2);
    cout << "power_recur_fast_test: all tests passed\n";
}
```

## Counting Multiplications
How much faster is "repeated squaring" than the obvious power algorithm? In other words, how many multiplications does `power_recur_fast` do? It is not so obvious from looking at the code, and so lets modify the function to count how many multiplications it does:

```cpp
int mult_count = 0;

int power_recur_fast(int a, int n) {
    assert(n >= 0);

    if (a == 0 && n == 0) return 1;
    if (a == 0 && n != 0) return 0;
    if (a != 0 && n == 0) return 1;
    if (a == 1) return 1;

    if (n % 2 == 0) {
        int half = power_recur_fast(a, n / 2);
        mult_count += 1;
        return half * half;
    } else {
        int half = power_recur_fast(a, (n - 1) / 2);
        mult_count += 2;
        return a * half * half;
    }
}

void power_test(int a, int n) {
    mult_count = 0;
    power_recur_fast(a, n);
    cout << a << "^" << n << ", " << mult_count 
	     << " multiplications" << "\n";
}
```

Here's some test code:

```cpp
for(int i = 0; i <= 20; ++i) {
    power_test(2, i);
}
```

And its output:

```
2^0, 0 multiplications
2^1, 2 multiplications
2^2, 3 multiplications
2^3, 4 multiplications
2^4, 4 multiplications
2^5, 5 multiplications
2^6, 5 multiplications
2^7, 6 multiplications
2^8, 5 multiplications
2^9, 6 multiplications
2^10, 6 multiplications
2^11, 7 multiplications
2^12, 6 multiplications
2^13, 7 multiplications
2^14, 7 multiplications
2^15, 8 multiplications
2^16, 6 multiplications
2^17, 7 multiplications
2^18, 7 multiplications
2^19, 8 multiplications
2^20, 7 multiplications
```

When $n$ is big, the number of multiplications being done is less then for the obvious algorithm (obviously!)

Is there a pattern to the number of multiplications? Look at the sequence of multiplications: 0, 2, 3, 4, 4, 5, 5, 6, 5, 6, 6, 7, 6, 7, 7, 8, 6, 7, 7, 8, 7, .... Notice that the numbers don't always increase --- sometimes they go down!

It turns out to be useful to print a message when each part of the if-statement is called. This version prints a 1 when one multiplication is done, and a 2 when two multiplications are done:

```cpp
int power_recur_fast(int a, int n) {
    assert(n >= 0);

    if (a == 0 && n == 0) return 1;
    if (a == 0 && n != 0) return 0;
    if (a != 0 && n == 0) return 1;
    if (a == 1) return 1;

    if (n % 2 == 0) {
        int half = power_recur_fast(a, n / 2);
        mult_count += 1;
        cout << 1;           // print 1
        return half * half;
    } else {
        int half = power_recur_fast(a, (n - 1) / 2);
        mult_count += 2;
        cout << 2;          // print 2
        return a * half * half;
    }
}

void power_test(int a, int n) {
    mult_count = 0;
    power_recur_fast(a, n);
	cout << " " << a << "^" << n << ", " 
         << mult_count << " multiplications\n";
}
```

The output (with a bit of added formatting) is:

```
      2^0, 0 multiplications
2     2^1, 2 multiplications
21    2^2, 3 multiplications
22    2^3, 4 multiplications
211   2^4, 4 multiplications
212   2^5, 5 multiplications
221   2^6, 5 multiplications
222   2^7, 6 multiplications
2111  2^8, 5 multiplications
2112  2^9, 6 multiplications
2121  2^10, 6 multiplications
2122  2^11, 7 multiplications
2211  2^12, 6 multiplications
2212  2^13, 7 multiplications
2221  2^14, 7 multiplications
2222  2^15, 8 multiplications
21111 2^16, 6 multiplications
21112 2^17, 7 multiplications
21121 2^18, 7 multiplications
21122 2^19, 8 multiplications
21211 2^20, 7 multiplications
```

Can you see a pattern in these values?

It might be easier to see if you change 1s to 0s and 2s to 1s:

```
      2^0, 0 multiplications
1     2^1, 2 multiplications
10    2^2, 3 multiplications
11    2^3, 4 multiplications
100   2^4, 4 multiplications
101   2^5, 5 multiplications
110   2^6, 5 multiplications
111   2^7, 6 multiplications
1000  2^8, 5 multiplications
1001  2^9, 6 multiplications
1010  2^10, 6 multiplications
1011  2^11, 7 multiplications
1100  2^12, 6 multiplications
1101  2^13, 7 multiplications
1110  2^14, 7 multiplications
1111  2^15, 8 multiplications
10000 2^16, 6 multiplications
10001 2^17, 7 multiplications
10010 2^18, 7 multiplications
10011 2^19, 8 multiplications
10100 2^20, 7 multiplications
```

These are the [[binary number|binary numbers]] from 1 to 20 (!). In addition to (quickly) calculating powers, we've also apparently created an algorithm that generates the [[binary number|binary numbers]].

To figure out the number of multiplications, first note that a 0 means one multiplication is done, and a 1 means two multiplications are done. If we knew the number of bits in the binary representation of $n$, then we could use that to estimate the number of multiplications done when `power_recur_fast` calculates  $a^n$.

A positive integer $n$ has about $\log_2 n$ bits when represented in binary. So, *at worst*, if every bit were a 1, then $2\log_2 n$ multiplications would be done. This is *much* smaller than the $n - 1$ multiplications done by the obvious algorithm.

So we can say that, *in the worst case*, the running time of `power_recur_fast` is *proportional* to $\log_2 n$. This makes it fast enough in practice to calculate large exponents. For example, the [RSA cryptosystem](http://en.wikipedia.org/wiki/65537_(number)) example $a^{65537}$ from above can be calculated in about $\log_2 65537 \approx 16$ multiplications.


## Even Fewer Multiplications?
This is an interesting algorithm! The performance is not at all obvious at first, nor is the connection to binary numbers.

You may wonder if this is the fastest way to calculate exponents. And the answer is ... no! For *some* exponents even fewer multiplications are possible. For example, consider $2^{15}$:

- you can get $2^{3}$ in 2 multiplications
- then square it to get $2^{6}$, 1 more multiplication
- then square it to get $2^{12}$, 1 more multiplication
- then multiply by $2^{3}$ from the first step to get $2^{15}$, 1 more multiplication

This uses *5 multiplications* in total to calculate $2^{15}$. But the fast recursive algorithm takes 8 multiplications.

## Practice Questions
1. Write a test function for `power_recur`.
2. If you were using the obvious algorithm in a program, which implementation would you prefer to use: the iterative (loop) one, or the recursive one? Explain your answer.
3. In the *worst case*, how many multiplications do the  obvious algorithm and the fast algorithm do to calculate $a^{1000000}$, i.e. $a$ to the power of one million?
4. For `power_recur_fast(a, n)`, it was shown in the notes that in the worst case about $2\log_2 n$ multiplications are needed. But what about the *best* case, when the binary representation of $n$ has just one 1-bit? About how many multiplications are done? And how often does this best case occur for an $n$-bit number?
5. Using the fast power function as a starting point, write a function called `to_binary_string(int n)` that returns the binary representation of a non-negative integer n as a string of `0`s and `1`s.

## Source Code
```cpp
// powers.cpp

#include <iostream>
#include <cassert>

using namespace std;


// Pre-condition: 
//    n >= 0
// Post-condition: 
//    returns a to the power of n, i.e. a^n
// Note: 
//    The bases cases can be compressed somewhat. However, we list them
//    individually to make it easier to see that all the cases have been
//    handled.
int power_iter(int a, int n) {
    assert(n >= 0);

    if (a == 0 && n == 0) return 1;
    if (a == 0 && n != 0) return 0;
    if (a != 0 && n == 0) return 1;
    if (a == 1) return 1;

    int pow = 1;
    for(int i = 0; i < n; ++i) {
        pow *= a;
    }
    return pow;
}

void power_iter_test() {
    assert(power_iter(2, 0) == 1);
    assert(power_iter(2, 1) == 2);
    assert(power_iter(2, 2) == 2*2);
    assert(power_iter(2, 3) == 2*2*2);
    assert(power_iter(2, 4) == 2*2*2*2);
    assert(power_iter(2, 5) == 2*2*2*2*2);
    cout << "power_iter_test: all tests passed\n";
}


// Pre-condition: 
//    n >= 0
// Post-condition: 
//    returns a to the power of n, i.e. a^n
// Note:
//   recursive
int power_recur(int a, int n) {
    assert(n >= 0);

    if (a == 0 && n == 0) return 1;
    if (a == 0 && n != 0) return 0;
    if (a != 0 && n == 0) return 1;
    if (a == 1) return 1;

    return a * power_recur(a, n - 1);
}

// Pre-condition: 
//    n >= 0
// Post-condition: 
//    returns a to the power of n, i.e. a^n
// Note:
///   recursive
int power_recur_fast(int a, int n) {
    assert(n >= 0);

    if (a == 0 && n == 0) return 1;
    if (a == 0 && n != 0) return 0;
    if (a != 0 && n == 0) return 1;
    if (a == 1) return 1;

    int half = power_recur_fast(a, n / 2);
    if (n % 2 == 0) {
        return half * half;
    } else {
        return half * half * a;
    }
}

void power_recur_fast_test() {
    assert(power_recur_fast(2, 0) == 1);
    assert(power_recur_fast(2, 1) == 2);
    assert(power_recur_fast(2, 2) == 2*2);
    assert(power_recur_fast(2, 3) == 2*2*2);
    assert(power_recur_fast(2, 4) == 2*2*2*2);
    assert(power_recur_fast(2, 5) == 2*2*2*2*2);
    cout << "power_recur_fast_test: all tests passed\n";
}

int mult_count = 0;

// Pre-condition: 
//    n >= 0
// Post-condition: 
//    returns a to the power of n, i.e. a^n
// Note:
//   uses global variable mult_count to count multiplications
int power_recur_fast2(int a, int n) {
    assert(n >= 0);

    if (a == 0 && n == 0) return 1;
    if (a == 0 && n != 0) return 0;
    if (a != 0 && n == 0) return 1;
    if (a == 1) return 1;

    if (n % 2 == 0) {
        int half = power_recur_fast2(a, n / 2);
        mult_count += 1;
        return half * half;
    } else {
        int half = power_recur_fast2(a, (n - 1) / 2);
        mult_count += 2;
        return a * half * half;
    }
}

void power_print2(int a, int n) {
    mult_count = 0;
    power_recur_fast2(a, n);
    cout << a << "^" << n << ", " << mult_count 
	     << " multiplications" << "\n";
}

void power_test2() {
    cout << "\n";
    for(int i = 0; i <= 20; ++i) {
        power_print2(2, i);
    }
}

// 2^0, 0 multiplications
// 2^1, 2 multiplications
// 2^2, 3 multiplications
// 2^3, 4 multiplications
// 2^4, 4 multiplications
// 2^5, 5 multiplications
// 2^6, 5 multiplications
// 2^7, 6 multiplications
// 2^8, 5 multiplications
// 2^9, 6 multiplications
// 2^10, 6 multiplications
// 2^11, 7 multiplications
// 2^12, 6 multiplications
// 2^13, 7 multiplications
// 2^14, 7 multiplications
// 2^15, 8 multiplications
// 2^16, 6 multiplications
// 2^17, 7 multiplications
// 2^18, 7 multiplications
// 2^19, 8 multiplications
// 2^20, 7 multiplications

// Pre-condition: 
//    n >= 0
// Post-condition: 
//    returns a to the power of n, i.e. a^n
// Note:
//    uses global variable mult_count to count multiplications,
//    prints a different character depending on whether the if
//    or else part is executed 
int power_recur_fast3(int a, int n) {
    assert(n >= 0);

    if (a == 0 && n == 0) return 1;
    if (a == 0 && n != 0) return 0;
    if (a != 0 && n == 0) return 1;
    if (a == 1) return 1;

    if (n % 2 == 0) {
        int half = power_recur_fast3(a, n / 2);
        mult_count += 1;
        cout << 1;           // print 1
        return half * half;
    } else {
        int half = power_recur_fast3(a, (n - 1) / 2);
        mult_count += 2;
        cout << 2;          // print 2
        return a * half * half;
    }
}

void power_print3(int a, int n) {
    mult_count = 0;
    power_recur_fast3(a, n);
	cout << " " << a << "^" << n << ", " 
         << mult_count << " multiplications\n";
}

void power_test3() {
    cout << "\n";
    for(int i = 0; i <= 20; ++i) {
        power_print3(2, i);
    }
}

// 2
// 21
// 22
// 211
// 212
// 221
// 222
// 2111
// 2112
// 2121
// 2122
// 2211
// 2212
// 2221
// 2222
// 21111
// 21112
// 21121
// 21122
// 21211

int main() {
    power_iter_test();
    power_recur_fast_test();
    // power_test2();
    // power_test3();
}
```