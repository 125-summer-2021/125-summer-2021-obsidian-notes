## The Basic Idea of Binary Search
[[sorted order|Sorted data]] is extremely valuable. A good demonstration of this is the amazing performance of [[binary search]], an [[algorithm]] that finds the location of a given element in a sorted vector. Binary search is specified like this:

```cpp
// Pre-condition: 
//    v is in ascending sorted order, i.e.
//    v[0] <= v[1] <= ... <= v[n-1]
// Post-condition: 
//    returns an index i such that v[i] == x
int binary_search(int x, const vector<int>& v)
```

The [[pre-condition]] is essential here: binary search only works on data that is in [[sorted order]].

Binary search works by first looking for `x` in the very *middle* of the vector. There are three possible cases:

1. **The middle element is equal to `x`**. In this case `x` has been found and so binary search stops.
2. **`x` is smaller than the middle element**. In this case, if `x` is in the
   vector it's among the values to the *left* of the middle element, and so
   binary search is run on this *left* half.
3. **`x` is bigger than the middle element**. In this case, if `x` is in the vector it's among the values to the *right* of the middle element, and so binary search is run on this *right* half.

What makes this so efficient is that each comparison cause *half* of the remaining elements to be discarded. In contrast, each comparison [[linear search]] does discards only 1 element.

For example, suppose our vector looks like this:

```
4, 5, 8, 11, 15, 20, 21
```

Does it contain 6? Binary search does the following:

```
4, 5, 8, 11, 15, 20, 21
          ^
          |
      check the middle-most element
```

Since 11 is not 6, we know that *if* 6 is in the vector, it must be to the left of 11. Thus we can ignore all the elements from 11 to 21:

```
4, 5, 8
   ^
   |
 check the middle-most element
```

Since 5 is not 11, we know that *if* 6 is among these numbers it must be to the right of 5. Thus we can ignore 4 and 5:

```
8
^
|
check the middle-most element
```

After checking that 8 is not equal to 6, we are done: we've proven that 6 is not in the list of numbers. And it only tool *3* comparisons to do this. In contrast, [[linear search]] would have had to have checked *all* the numbers to make sure none are 3, and so would have done *7* comparisons.

## Binary Search with a Loop
Here's an implementation:

```cpp
// Pre-condition: 
//   v[begin] to v[end - 1] is in ascending sorted order
// Post-condition: 
//   returns an index i such that v[i] == x and begin <= i < end; 
//   if x is not found, -1 is returned
int binary_search(int x, const vector<int>& v, int begin, int end) {
  while (begin < end) {
    int mid = (begin + end) / 2;
    if (v[mid] == x) {          // found x!
      return mid;
    } else if (x < v[mid]) {
      end = mid;
    } else if (x > v[mid]) {
      begin = mid + 1;
    }
  }
  return -1;                    // x not found
}

// Pre-condition:
//    v is in ascending sorted order
// Post-condition:
//    returns an index i such that v[i] == x; if x is not in
//    v, -1 is returned
int binary_search(int x, const vector<int>& v) {
  return binary_search(x, v, 0, v.size());
}
```

The [[algorithm]] is rather subtle at points, and it is easy to code it incorrectly. For example, this line is easy to get wrong:

```cpp
int mid = (begin + end) / 2;
```

This calculates the mid-point of a range. You can derive it as follows. The length of the range is $end - begin$, and half of that is $\frac{end - begin}{2}$. Since the range starts at $begin$, the mid-point is $begin + \frac{end - begin}{2}$, which simplifies to $\frac{end + begin}{2}$.

If $end - begin$ happens to be odd, then $\frac{end + begin}{2}$ ends with .5, and we C++ chops that off (e.g. 34.5 becomes 34). This is not obviously the right thing to do, but if you test it with a few actual values by hand, and write some good tests, you can be pretty confident that it's correct.

## Binary Search with Recursion
Here's a recursive version of binary search:

```cpp
int binary_search_rec(int x, const vector<int>& v, int begin, int end) {
   const int n = end - begin;

   // if the sub-vector being searched is empty, then x is not in it
   if (n <= 0) return -1; // x not found
  
   int mid = (begin + end) / 2;
   if (x == v[mid]) {
     return mid;
   } else if (x < v[mid]) {
     return binary_search_rec(x, v, begin, mid);
   } else {
     return binary_search_rec(x, v, mid + 1, end);
   }
}

int binary_search_rec(int x, const vector<int>& v) {
  return binary_search_rec(x, v, 0, v.size());
}
```

The code is similar in length and complexity to the non-recursive version, and so in practice the non-recursive version is generally preferred. But this recursive version is still extremely efficient.

Here are few tests for binary search:

```cpp
void binary_search_test() {
   vector<int> empty;
   vector<int> one = {5};
   vector<int> two = {2, 7};

   assert(binary_search(2, empty) == -1);
   assert(binary_search(2, one) == -1);
   assert(binary_search(5, one) == 0);
   assert(binary_search(0, two) == -1);
   assert(binary_search(2, two) == 0);
   assert(binary_search(4, two) == -1);
   assert(binary_search(7, two) == 1);
   assert(binary_search(10, two) == -1);

   cout << "all binary_search tests passed\n";
}
```

## How Fast is Binary Search?
How fast is binary search? Blazingly fast, as it turns out: for an n-element vector, it checks, at most, $\log_2 n$ different values. So, for instance, on a million-element vector binary search will do *at most* 20 comparisons (because $\log_2 1000000 \approx 20$).

While binary search is extremely efficient for searching large amounts of data, it comes with a price: the vector must be in [[sorted order]]. The problem with sorting is that even the fastest general-purpose sorting algorithms are slower than *linear* search. Thus sorting a vector and then calling binary search is usually slower than doing [[linear search]] a few times. However, sorting the data once followed by *multiple* binary searches may be faster than lots of linear searches.

> Did you know that you can drive your car a mile without using a drop of gas? Just start at the top of a mile-long hill and roll down. Of course, the hard part is getting your car up there in the first place! Binary search suffers from a similar problem.


## Why is Binary Search so Fast?
Every time binary search iterates once through its main loop, it discards *half* of all the elements of `v`. So if `v` initially has $n$ elements, the number of possible elements that could be equal to `x` decreases by half each time through the loop: $n$, $\frac{n}{2}$, $\frac{n}{4}$, $\frac{n}{8}$, ..., $\frac{n}{2^i}$.

Eventually, $2^i$ is bigger than $n$, meaning there are no more elements to consider. We want to know the *first* value of $i$ that makes $\frac{n}{2^i} < 1$. That is, we want to solve for $i$ in this inequality:

$\frac{n}{2^i} < 1 \leq \frac{n}{2^{i-1}}$

Multiplying everything by $2^i$ gives this:

$n < 2^i \leq 2n$

And taking the base-2 logarithm:

$\log_2 n < i \leq \log_2 2n$

Since $\log_2 2n = \log_2 2 + \log_2 n = 1 + \log_2 n$, it further simplifies to:

$\log_2 n < i \leq 1 + \log_2 n$

Thus, `i` is approximately equal to $\log_2 n$.

This result shows that binary search does about $\log_2 n$ comparisons *in the worst case*. Sometimes it might do fewer, of course, but $\log_2 n$ is small for even big values of $n$. For instance, $\log_2 10^9 \approx 30$, meaning that binary search does *at most* 30 comparisons to find a given number in a vector with a *billion* elements.

| $n$        | $\log n$ (approx) | 
|------------|-------------------|
| 10         | 3                 | 
| 100        | 7                 |
| 1000       | 10                |
| 10000      | 13                |
| 100000     | 17                |
| 1000000    | 20                |
| 10000000   | 23                |
| 100000000  | 27                |
| 1000000000 | 30                |

> **Fact** The base-2 logarithm $log_2 n$ of a number $n$ is the least number of bits needed to represent $n$ in binary.

## Practice Questions
1. In your own words, describe the basic method that binary search uses to find an element in a sorted list.
2. Suppose `v` is a `vector<int>` with $n$ elements, and we know for a fact that the number 5 is *not* in `v`. How many comparisons must binary search do to prove that 5 is not in `v`?
3. Suppose binary search takes 5 seconds in the worst-case to find a target value in a list of $n$ elements. About how many seconds would you expect it to take in the worst case to find a target value in a list with $2n$ elements?