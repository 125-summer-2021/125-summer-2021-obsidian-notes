```cpp
// count_chars7.cpp

//
// Conversion of count_chars6 to use a constructor.
//

#include <iostream>
#include <fstream>

using namespace std;

struct Count {
  fstream infile;

  int num_chars = 0;
  int num_lines = 0;
  int num_tabs  = 0;
  int num_words = 0;

  // constructor (initializes a Count object)
  Count(const string& fname) 
  : infile(fname)
  { }

  // returns true if c is a whitespace character, and false otherwise
  bool is_whitespace(char c) {
      switch (c) {
          case ' ' : return true;
          case '\n': num_lines++;
                     return true;
          case '\t': num_tabs++;
                     return true;
          default  : return false;
      }
  }

  void skip_whitespace() {
      char c;
      infile.get(c);
      if (infile.eof()) return;
      num_chars++;
      while (is_whitespace(c)) {
          infile.get(c);
          if (infile.eof()) return;
          num_chars++;
      }
  }
}; // class Count

int main(int argc, char* argv[]) {
  // check that exactly one filename argument provided
  if (argc != 2) {
    cout << "Wrong number of arguments\n";
    return -1;
  }

  Count count(argv[1]);
  char c;
  while (count.infile.get(c)) {
      switch (c) {
          case '\n': count.num_chars++;
                     count.num_lines++;
                     count.num_words++;
                     count.skip_whitespace();
                     break; // fall-through occurs if this break is removed
          case '\t': count.num_chars++;
                     count.num_tabs++;
                     count.skip_whitespace();
                     break; // fall-through occurs if this break is removed
          case ' ' : count.num_chars++;
                     count.num_words++;
                     count.skip_whitespace();
                     break; // fall-through occurs if this break is removed
          default  : count.num_chars++; // default case catches all other characters
                                        // break is not needed on last case
      } // switch
  } // while
  cout << "#chars: " << count.num_chars << "\n";
  cout << "#lines: " << count.num_lines << "\n";
  cout << "#tabs : " << count.num_tabs  << "\n";
  cout << "#words: " << count.num_words << "\n";
}
```