```cpp
// count_chars1.cpp

//
// The standard Linux utility "wc" characters, lines, and words in a file. In
// this program we create our own version of wc.
//
// 1. Count the number of characters in a file. Hint: cin.get(c) sets the char
//    c to the next character on cin. Use file re-direction in the shell
//    to process a file.
//
// 2. Count the number of lines in a file.
//
// 3. Count the number of number tab characters in a file. Use a switch
//    statement to check for the newline and tab characters.
//
// 4. Count the number of words in a file. To simplify this, you can just
//    count the number of substrings consisting of one or more whitespace
//    characters.
//

#include <iostream>

using namespace std;

// These are global variables. Global variables are often a bad idea because
// the could be modified at any time by any part of the program, thus
// potentially causing subtle bugs. But in a small program like this it is
// actually convenient to use globals variables because the skip_whitespace
// function needs to modify multiple variables.
int num_chars = 0;
int num_lines = 0;
int num_tabs  = 0;
int num_words = 0;

// returns true if c is a whitespace character, and false otherwise
bool is_whitespace(char c) {
    switch (c) {
        case ' ' : return true;
        case '\n': num_lines++;
                   return true;
        case '\t': num_tabs++;
                   return true;
        default  : return false;
    }
}

void skip_whitespace() {
    char c;
    cin.get(c);
    if (cin.eof()) return;
    num_chars++;
    while (is_whitespace(c)) {
        cin.get(c);
        if (cin.eof()) return;
        num_chars++;
    }
}

int main() {
    char c;
    while (cin.get(c)) {
        switch (c) {
            case '\n': num_chars++;
                       num_lines++;
                       num_words++;
                       skip_whitespace();
                       break; // fall-through occurs if this break is removed
            case '\t': num_chars++;
                       num_tabs++;
                       skip_whitespace();
                       break; // fall-through occurs if this break is removed
            case ' ' : num_chars++;
                       num_words++;
                       skip_whitespace();
                       break; // fall-through occurs if this break is removed
            default  : num_chars++; // default case catches all other characters
                                    // break is not needed on last case
        } // switch
    } // while
    cout << "#chars: " << num_chars << "\n";
    cout << "#lines: " << num_lines << "\n";
    cout << "#tabs : " << num_tabs  << "\n";
    cout << "#words: " << num_words << "\n";
}
```