```cpp
// count_chars2.cpp

//
// Conversion of count_chars1 to a style that uses a struct instead of global
// variables.
//

#include <iostream>

using namespace std;

struct Count {
  int num_chars = 0;
  int num_lines = 0;
  int num_tabs  = 0;
  int num_words = 0;
};

// returns true if c is a whitespace character, and false otherwise
bool is_whitespace(Count& count, char c) {
    switch (c) {
        case ' ' : return true;
        case '\n': count.num_lines++;
                   return true;
        case '\t': count.num_tabs++;
                   return true;
        default  : return false;
    }
}

void skip_whitespace(Count& count) {
    char c;
    cin.get(c);
    if (cin.eof()) return;
    count.num_chars++;
    while (is_whitespace(count, c)) {
        cin.get(c);
        if (cin.eof()) return;
        count.num_chars++;
    }
}

int main() {
  Count count;
  char c;
  while (cin.get(c)) {
      switch (c) {
          case '\n': count.num_chars++;
                     count.num_lines++;
                     count.num_words++;
                     skip_whitespace(count);
                     break; // fall-through occurs if this break is removed
          case '\t': count.num_chars++;
                     count.num_tabs++;
                     skip_whitespace(count);
                     break; // fall-through occurs if this break is removed
          case ' ' : count.num_chars++;
                     count.num_words++;
                     skip_whitespace(count);
                     break; // fall-through occurs if this break is removed
          default  : count.num_chars++; // default case catches all other characters
                                        // break is not needed on last case
      } // switch
  } // while
  cout << "#chars: " << count.num_chars << "\n";
  cout << "#lines: " << count.num_lines << "\n";
  cout << "#tabs : " << count.num_tabs  << "\n";
  cout << "#words: " << count.num_words << "\n";
}

```