# Integrated Development Environment 
A software tool that combines various tools for running and creating software. They are very popular in practice. 

However, in this course, we recommend against using an IDE. Instead, you use a programming editor (like [Sublime Text](https://www.sublimetext.com/3)) and the command line. This will give you a clearer idea of how the basic tools work together. Plus some things, like setting up makefiles and re-directing output, are easier at the command-line than with an IDE.
