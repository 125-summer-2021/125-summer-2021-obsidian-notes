The following tags are used in this vault:
- #terminology tags a page that defines a term related to the course
- #todo is used on pages where more content is needed

Syntax for aliases:
```
---
aliases: [note title, alternative title]
tags: ["#tag", "#anothertag"]
---
```