## Theoretical Computer Science: Alan Turing's Pioneering Work

[Theoretical computer science](https://en.wikipedia.org/wiki/Theoretical_computer_science) is a subfield of computer science that studies computational problems using mathematical techniques. Theoretical computer scientists often do very little actual programming, and are typically interested in theorems, proofs, and mathematically rigorous descriptions.


## A Mathematical Model of Computer: the Turing Machine
One of the most influential people in the history of theoretical computing was [Alan Turing][Turing]. Here he is at 16 years old:

![[Alan_Turing_Aged_16_small.jpg]]

Turing was interested in questions like "What is a computer?", and "What can be computed?". When you look at the wide variety of ways in which computers can be implemented, these questions are not so simple. These slides show the variety of different ways computers can be implemented: [[computerExamples.pdf]].

In 1936, he published a pioneering paper that gave a mathematical definition of a computer now known as a [Turing machine][Turing machine]. A [Turing machine][Turing machine] is an abstract mathematical model of computation that doesn't depend upon any actual physical computer. Using this, [Turing][Turing] proved there are computational problems for which no algorithm that exists that solves them. To this day, [Turing machines][Turing machine] are still one of the most commonly used mathematical models of computation.

A [Turing machine][Turing machine] can be imagined as a read-write head moving along an infinitely long paper tape divided into cells:

![[turing-machine.jpg]]

The read-write head can only move left or right, and can read or write symbols on any cell. In addition, the [Turing machine][Turing machine] stores the **state** of the computation. When it reads a symbol in a cell, it decides what cell to go to next based on the current state and the symbol currently under the read-write head. A big look-up table is used to control the read-write head: this table tells the machine what to do in every situation, and it is this that controls the [Turing machine][Turing machine]. Despite it's simplicity, [Turing][Turing] proved that almost any practical computation can be implemented on a [Turing machine][Turing machine].

While [Turing][Turing] meant his machine to be a mathematical idea only, people have tried to actually implement them. For example, here is [a video of a physical Turing machine](https://www.youtube.com/watch?v=E3keLeMwfHY). And here is [a hand-cranked one made from wood](https://www.youtube.com/watch?v=vo8izCKHiF0).


## What Can be Computed: The Church-Turing Thesis
The [Church-Turing thesis][CT thesis] is a claim about the nature of computation. Informally, it says that anything that *can* be computed can be computed by a [Turing machine][Turing machine] (or a formalism of equivalent computational power to a [Turing machine][Turing machine]). In other words, any possible computation could, at least in theory, be implemented as a [Turing machine][Turing machine].

A process is said to be [Turing-complete][Turing Complete] if it can simulate any [Turing machine][turing machine] (at least in theory --- the infinite tape of the [Turing machine][Turing machine] must be finite for any physical process). A [Turing-complete][Turing complete] process can thus, in theory, do any computation a [Turing machine][turing machine] can do, which is any possible computation (if you believe the [Church-Turing thesis][CT thesis]).

Almost all modern programming languages are [Turing-complete][Turing Complete], i.e. it is possible to implement [Turing machines][Turing machine] in them. An example of a language that is **not** [Turing-complete][Turing Complete] is the plain language of [regular expressions](https://en.wikipedia.org/wiki/Regular_expression) (with no special extra features added), which is often used for string-matching.

More surprisingly, there are other processes that turn out to be [Turing-complete][Turing Complete], such as:

- Some video games, e.g. [Minecraft](https://en.wikipedia.org/wiki/Minecraft) and [Minesweeper](https://en.wikipedia.org/wiki/Minesweeper_(video_game)).
- The card game [Magic: The Gathering](https://en.wikipedia.org/wiki/Magic:_The_Gathering).
- [C++ Templates](https://en.wikipedia.org/wiki/Template_(C%2B%2B)), i.e. C++ templates are powerful enough that, at *compile* time, before a C++ program even runs, it is possible for templates to simulate any [Turing machine][Turing machine].
 

## An Undecidable Problem: The Halting Problem
[Turing][Turing] created [Turing machines][Turing machine] to help solve a problem in mathematical logic known as [the halting problem][halting problem] (more specifically, he solved the [Entscheidungsproblem](https://en.wikipedia.org/wiki/Entscheidungsproblem)). The [halting problem][halting problem] asks if it is possible to write an **infinite-looper checker**, i.e. a program that reads another program (e.g. a C++ source file) as input, and determines if some input to that read-in program would cause it to go into an infinite loop. [Turing][Turing] proved that there is no [Turing machine][Turing machine] that can answer that problem in general. It's more than just a hard problem: no program exists that can do it!

While the precise mathematical details of the proof are beyond this course, the basic trick that [Turing][Turing] used to proved this is surprisingly simple. This [animated video walks through the essential logic of the proof](https://www.youtube.com/watch?v=92WHN-pAFCs).


## The Turing Test
In addition to this foundational work on [Turing machines][Turing machine] and computability, [Turing][Turing] was also an early pioneer in artificial intelligence (AI). For instance, he created one of the first chess-playing programs (although he never implemented it on a computer --- but it could be traced by hand).

In 1950, [Turing] proposed the [Turing Test]as a way to determine if a computer is intelligent. In the [Turing Test], a human questioner talks to either another human or computer (they don't know which) through a text-chat interface. The interrogator tries to determine if they are talking to a human or a computer just by conversation. The interrogator can ask *any* question they want, or say anything they want. [Turing] believed that if a computer could usually fool human interrogators into believing it was human, then we could conclude that the computer is intelligent (or at least as intelligent as an average human).

The [Turing Test] is not (yet!) a practical test of computer intelligence since programs that converse fluently and intelligently with humans are still beyond the state of the art of AI. But for some non-trivial tasks, like recognizing images or converting speech to text, some programs can perform as well as, or even better, than humans.

## Practice Questions
1. What year was [Alan Turing][Turing] born? What year did he die?
2. Why can't a [Turing Machine] actually be implemented?
3. State the [Church Turing thesis][CT thesis]. Do you think it's true?
4. What does it mean for a process to be [Turing Complete]?
5. Give an example of computational problem for which it has been proven there is no algorithm that solves it.
6. Pete the programmer thinks that it *is* possible to write an [[infinite loop]] checker in C++. "All you need to do," says Pete, "is search for patterns of infinite loops, like for(;;) { ... }, or while(true) { ... }. There would be a lot of such patterns, but if you had the time you could write a program that checks for them all."  Explain to Pete why this idea will *not* lead to the creation of an infinite-loop checker.
7. Describe how the [Turing Test] works. Do you think that a computer that could pass it should be considered intelligent? Why is it *not* a good test for artificial intelligence?


[Turing]: https://en.wikipedia.org/wiki/Alan_Turing
[Turing machine]: https://en.wikipedia.org/wiki/Turing_machine
[Turing complete]: https://en.wikipedia.org/wiki/Turing_completeness
[CT thesis]: https://en.wikipedia.org/wiki/Church%E2%80%93Turing_thesis
[halting problem]: https://en.wikipedia.org/wiki/Halting_problem
[Turing Test]: https://en.wikipedia.org/wiki/Turing_test