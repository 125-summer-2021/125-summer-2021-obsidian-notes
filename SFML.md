SFML stands for "Simple and Fast Media Library", and it is an objected-oriented collection of functions and classes for drawing, making sounds, detecting keyboard input, and so on.

To install it on Ubuntu Linux type this command into a terminal:

```bash
$ sudo apt-get install libsfml-dev
```

For a sample SFML program and more help using it, check out these [SFML notes](https://gitlab.com/125-summer-2021/125-summer-2021-code-notes/-/tree/master/sfml_test).

