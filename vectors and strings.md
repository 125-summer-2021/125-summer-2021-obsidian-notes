## Strings
In general, a string is a sequence of 0 or more characters. A string with 0 characters is called the **empty string**.

### String Literals
A C++ **string literal** begins and ends with a `"`-mark. The empty string literal is written `""`, i.e. two "-marks with no space between them.

String literals are of type `char*`, i.e. they are C-style strings (see below). This can occasionally cause problems when you use them with string variables of type `string`.

C++ string literals can contain **escape sequence**. A C++ escape sequence in a string literal always starts with a backslash `\` character, followed by one or more characters that are interpreted is a special way. For example, the string `"one\ttwo\nthree"` contains two different escape sequences, `\t` (tab) and `\n` (newline). We have to use escape sequences for tab and newline characters because they are both [[whitespace]] characters that have no standard visual representation.

An escape sequence like `\t` (tab) or `\n` counts as a single character in a string. For example, the string `"\n\n\t\t"` has four characters: two newlines followed by two tabs.

If you want to put a `"` or  a `\` character in a C++ string literal, you need to use an escape sequence: `\"` or `\\`. For instance, this string literal has two characters, a quote-mark followed by a backslash: `"\"\\"`.

### C-style Strings
A **C-style string** is an array of `char`s that ends with a `\0` character. Since they are arrays, C-style strings follow all the usual array rules. For instances, the size of a C-style string can never change after it's created.

While C-style strings are conceptually simple, they are notoriously error-prone and have a reputation of being unpleasant to work with. For instance:
- You must be careful never to over-write or forget the `\0` at the end of the string. Doing  this properly is entirely up to the programmer, and is often easier said than done.
- Arrays of strings can be complicated because they are really arrays of arrays, and you need to be comfortable with pointers to pointers. Plus, allocating and de-allocating strings is done manually, which is a common source of errors.
- Common operations, like testing if one string comes before the other alphabetically, or combining two strings to make a new one, require careful use of special string functions. You can't just use the `<` and `+` operators as you can with standard `string`s.

For most programs, the standard `string` is much easier to use.

### The Standard string Data Type
In C++, `string` is the standard data type for representing and manipulating strings. Conceptually you can think of it as a sequence of characters, and the low-level details are automatically managed. For instance, you don't have to worry about a `\0` at the end of a standard `string`, and if `s` is  `string` then `s.size()` is the number of characters in it. Basic operators like `==`/`!=` (testing if strings are the same/different), `<`/`<=` (testing if strings are in alphabetical order), and `+` (combining two strings together) work in the expected way.

For example:

```cpp
#include <iostream>
#include <string>

using namespace std;

int main() {
	string s = "Jane";
	string t = "Goodall";
	if (s < t) {
		cout << s << " comes before " << t << " alphabetically\n";
	} 
    string name = s + " " + t;
    cout << name << "\n";
}
```

You access individual characters of a `string` using [ ]-notation. For example, if `s` is  a non-empty string, then `s[0]` is the first character, `s[1]` is  the second character, and so on up to the last character `s[s.size()-1]`.

For example:

```
#include <iostream>
#include <string>

using namespace std;

int main() {
	string s = "hamster";
	for(int i = 0; i < s.size(); i++) {
		cout << "s[" << i << "] = '" << s[i] << "'\n";
	}

	// for-each loop
	for(char c : s) {
		cout << c << " ";
	}
	cout << "\n";
}
```

## Vectors
A `vector<T>` is container of 0 or more values of type `T`. A `vector<T>` acts very much like a an array of values of type `T`, but with a few extra features. For instance, a `vector<T>` keeps track of its own size, and the size of a `vector<T>` can increase and decrease as needed.

Here's an example program that reads strings from `cin`, storing them all in a `vector<string>`:

```cpp
#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main() {
	vector<string> words;
	int char_count = 0;
	string w;
	while (cin >> w) {
		char_count += w.size(); // doesn't count spaces
		words.push_back(w);
	}
	cout << "# of chars: " << char_count << "\n";
	cout << "# of words: " << words.size() << "\n";
}
```

### Vectors Compared to Arrays
Arrays are lower-level than vectors, and so lack many of their features. You cannot change the size of array, and so inserting or removing values can be tricky.

A `vector<T>` is often implemented in terms of arrays, automatically managing the details of its use.

In most programs, vectors are preferred over arrays. They have more features, are easier to use, and are generally not any less efficient compared to the same operations done on arrays.

## Practice Questions
1. Give an example of two different escape sequences that can appear in a C++ string. Why are escape sequences necessary?
2. What's the size (number of characters) of the string `"\\\n\\"n\\t"`?
3. How are C-style strings implemented?
4. What are two major difficulties of using C-style strings?
5. If `s` and `t` are both variables of type string, write an expression that returns `true` when they have *different* values, and `false` otherwise.
6. What is a `vector<T>`? How is it often implemented?