**Finite state machines** are a mathematical model of a kind of computer that recognizes patterns in strings. **Regular expressions** are a language for describing string patterns, and they are usually implemented as a kind of finite state machine.

## Finite State Machines: Pattern Recognizers
The finite state machines that we're going to discuss are called **finite state recognizers**. They take a string as input, and return *yes* if the string matches a particular pattern, *no* if it doesn't.

 Suppose we want to know if a [[bit string]] **ends with a 0**. This could be useful in a real program since binary numbers that end in 0 are even.

To test if a binary string ends with 0, imagine a machine that scans the bits in the string from left to right. It starts with the left-most bit, and checks if it's a 0 or a 1. If it's a 0, the machine goes into an **accepting state**, i.e. it remembers that the last bit it saw was a 0. If instead the bit is a 1, it goes into a **non-accepting state**, i.e. it remembers that the last bit it saw was a 1.

The machine keeps doing this for each bit in succession. After it reads a bit, it's either in the accepting state or the non-accepting state. If it's in the accepting state when all the bits have been read, then the last bit must be a 0.

We can draw this finite state machine as a graph: 

![[fsm_endsWith0.png]]

Note the following:

- A circle represents a state of the machine. This particular machine has 3 different states, labeled *A*, *B*, and *C*.

- State A is the **start state**, i.e. the state the machine is in *before* it reads the first bit. This is important: if the machine starts in a different state, then it may not return the correct result.

- The arrows that go from one state to another are **transitions**, and they tell the machine what state to go into after seeing a particular input. Every state handles both a 0 and a 1. Note that it's possible for an arrow to loop back on itself to the state it started in.

- States with **2 circles represent accepting states**. There is only 1 accepting state in this particular machine, state B, but in general a finite state machine could have 1, or more, accepting states.

- We assume that the input string consists entirely of 0s and 1s. If there is some other character in the input string, then the machine will crash.

- We assume that the machine reads bits in left to right order, and can never go backward to re-read a bit. Bits *cannot* be written, i.e. we *cannot* change the input string. This read-only restriction is one of the reasons why finite recognizers are *less* powerful than [Turing machines](https://en.wikipedia.org/wiki/Turing_machine). 

Lets trace through the machine with the bit string "11010":

- starts in state A;

- "11010": moves to state C since the next bit is "1"

- "1010": stays state C since the next bit is "1"

- "010": moves to state B since the next bit is "0"

- "10": moves to state C since the next bit is "1"

- "0": moves to state B since the next bit is "0"

At this point, the entire string has been read, so the machine stops. Since it stops in state B, the accepting state, the machine correctly determines the bit string ends with a "0".


## Example: Recognizing a Bit String of all 1s
As another example, suppose you want to **recognize bit strings consisting of all 1s**, i.e. a bit string that consist of one, or more, 1s, and no 0s.

The idea is to scan through the bits one at a time, and if a 0 is ever seen then the machine goes into a non-accepting state that it never escapes:

![[fsm_all1s_small.png]]

If the machine ever gets to state C, then it stays there forever. An optimization that could be applied to such a machine would be to have it stop as soon as it gets into state C.


## Regular Expressions
Finite state recognizers can be compactly described using the language of **regular expressions**. A regular expression is a mathematical description of a set of strings. For simplicity, we will only consider bit strings, but regular expressions work with any set of characters in essentially the same way.

First, a specific bit string itself is a regular expression. For example, `10001` is a regular expression that matches just the string consisting of a `1`, followed by three `0`s, and then a `1`. `00` is a regular expression that matches just the string consisting of two `0`s.

Next, there are three **meta characters** that describe patterns: `*`, `|`, and `()`.

- `a*`: matches 0, or more, occurrences of the regular expression `a`

- `a|b`: matches either the regular expression `a`, or the regular expression `b`

- `(a)`: groups the characters of a regular expression into one chunk so that they can be used with the previous two meta characters

For example, consider the set of all strings consisting of 0, or more, occurrences of a `1`, i.e. the set {`""`, `1`, `11`, `111`, `1111`, ... } (`""` is the empty string). We can represent it by the regular expression `1*`. The `*` means "match 0 or more" occurrences of the expression to its left". So `1*` is the set of all bit strings containing just `1`s, and `0*` is the set of all bit strings with just `0`s, i.e. {`""`, `0`, `00`, `000`, `0000`, ... }.

The `*` only applies to the *first* expression to its left. So the regular expression `01*` matches all strings that start with a `0` followed by 0, or more, `1`s. The regular expression `11*` matches all bit strings that consist of one, or more, `1`s.

Using`(` and `)`, `*` can apply to expressions of more than 1 character. For example, the regular expression:

- `(01)*` matches the set of strings {`""`, `01`, `0101`, `010101`, ...}

- `01(01)*` matches the set of strings {`01`, `0101`, `010101`, ... }

- `(101)*` matches the set of strings {`""`, `101`, `101101`, `101101101`,
  ...}

- `101(101)*` matches the set of strings {`101`, `101101`, `101101101`, ...}

- `(00)*(11)*` matches the sets of strings that consist of an even number of `0`s (possibly zero), followed by an even number of `1`s (possibly zero). For example, it matches all of these strings: `""`, `0000`, `11`,
  `0000001111`, `0000111111`. But it *doesn't* many any of these ones: `0`,
  `1`, `00011`, `000011111`, `0001110`

The `|` character match one or both of two choices. Intuitively, it means "or". For example, the regular expression `0|1` matches either a single `0`, *or* a single `1`. Using brackets, we can match entire sub-expressions. For example, `11|01` matches either `11` or `01`, and is the same as `1(1|0)1`. The regular expression `(11)|(01)` matches either `11` or `01`.

Combining these meta character lets us match many different kinds of bit strings. For example:

- `(0|1)*` matches *any* bit string, including the empty string.

- `(0|1)(0|1)*` matches any *non-empty* bit string, i.e. all bit strings of
  length one or greater.

- `(0|1)*0` matches all bit strings that *end* with a `0`.

- `1(0|1)*` matches all bit strings that *start* with a `1`.

- `0*|1*` matches all bit strings consisting either of all `0`s, or all `1`s.
  It includes the empty string.

Other meta characters have been added to regular expressions to make them easier to work with, but we won't cover them here. Some programming languages, such as JavaScript, Ruby, and Perl, have regular expressions built into them as a fundamental part of the language. Other languages, such as Python and C++, provide regular expression libraries.

C++ has a standard library called `<regex>` that supports regular expressions. We will not cover it here, but you may want to look into it (just search for "c++ regex" on the web). Regular expressions can be quite useful for doing things like input validation, or searching for text in files.

In practice regular expressions are often used for validating input data, e.g. from a form on a web. For instance, [this regular expression](https://stackoverflow.com/questions/201323/how-can-i-validate-an-email-address-using-a-regular-expression) matches valid **email addresses**:

  ```
  (?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])
  ```
  
This particular regular expression matches more than just 0s and 1s, and uses features we haven't discussed. The complexity of this regular expression is due largely to the fact that email addresses are surprisingly complex.
  
## Finite State Recognizers Can't Recognize Everything
Finite state recognizers only have a finite number of states (i.e. a finite amount of *memory*), and so there are some sets of strings they can't recognize. For example, consider all the non-empty bit strings that begin with $n$ `0`s that are immediately followed by $n$ `1`s (where $n$ is an integer greater than 0). A finite state recognizer **can't match the set of all such strings** because $n$ can be arbitrarily large integer $n$. 

However, for any *fixed* value of $n$, we can make a finite recognizer that works. For example, it's possible to write a finite state recognizer that matches just bit strings that begin with three `0`s, and end with three `1`s.

It is possible to mathematically prove these facts about finite state recognizers, but we won't go into the proofs in this course. The mathematical study of finite state machines, and other forms computation, is a major topic in theoretical computer science.

## Questions
For each of the following, draw a finite state machine that accepts just **non-empty** bit strings that match the description. Use as few states as you can. In addition, write a regular expression that describes the same set of strings the finite machine accepts.

 1. Ends with 1.
 2. All 0s.
 3. Starts with 1.
 4. Starts and ends with 1.
 5. The first bit and the last bit are the same. For example, "10011" and "0"
    should be accepted, but not "01" or "101110".
 6. All 0s, or all 1s.
 7. Six bits, the first three are 0, and the last three are 1.

[[regex solutions|Here are the solutions]].

