
## A Bit of History: Donald Knuth and the Art of Computer Programming
![[Donald-Ervin-Knuth.jpg|200]]

[Donald Knuth](https://en.wikipedia.org/wiki/Donald_Knuth) is a computer scientist whose most famous work is a series of books called [The Art of Computer Programming](https://en.wikipedia.org/wiki/The_Art_of_Computer_Programming). In 1962, he wrote an outline for a series of 7 books covering all of computer science, and as of 2021 has not quite finished volume 4.

In these books he gives detailed mathematical analyses of many important algorithms. In this note, we will look at a problem he discusses in his Volume 1 (published in 1967): finding the max value in a list of numbers.

## Finding the Max
Finding the max value in a list is a common practical problem. The algorithm that solves this problem isn't difficult to discover or understand, but it has an interesting an unexpected detail in its running-time.

The problem can be stated precisely as follows:

> **The Max of a List**: Given values $v_0, v_1, \ldots, v_{n-1}$ with $n > 0$, find $i$ such that $v_i \geq v_j$ for all $j$ in $0, 1, \ldots , n-1$. In other words, find the largest value among $v_0, v_1, \ldots, v_{n-1}$.

This description allows for the possibility that the max occurs 2 or more times. It also assumes *nothing* about the order of the elements: $v_0, v_1, \ldots, v_{n-1}$ could be in any order at all.

## Basic Solution
The basic solution involves checking each element of the list to see if it's bigger than the values that have been checked earlier. Here $k$ is the index of the max value seen so far:

- $k \leftarrow 0$ ($\leftarrow$ is assignment)

- if $v_1 \geq v_k$ then $k \leftarrow 1$

- if $v_2 \geq v_k$ then $k \leftarrow 2$

- if $v_3 \geq v_k$ then $k \leftarrow 3$

- ...

- if $v_i \geq v_k$ then $k \leftarrow i$

- ...

- if $v_{n-1} \geq v_k$ then $k \leftarrow n-1$

- return $k$

Since we don't know what $n$ is ahead of time, we have to use a loop (or [[recursion]]) to execute all the if-statements. Here is a [[pseudocode]] implementation that does that:

```
max_so_far = v[0]
for i = 1 to n - 1 do
   if v[i] > max_so_far then
       max_so_far = v[i]
   end
end
```

## Basic Solution Performance
How efficient is this max-finding algorithm? The traditional measurement of the performance of the algorithms like this is to count how many *comparisons* it does, i.e. how many times `v[i] > max_so_far` is executed. 

By inspecting the algorithm, we can see that it does exactly $n-1$ comparisons in *every* case, no matter where in `v` the max value happens to be.

So we say the number of comparisons it does is **proportional to $n$**, i.e. it's **linear**. 

> **Note** When get to [[O-notation]], we can say this algorithm does $O(n)$ comparisons.

## A C++ Implementation
A C++ implementation of the max algorithm is fairly straightforward. Notice it requires the vector has at least one element:

```cpp
// Pre-condition:
//    v.size() > 0
// Post-condition:
//    Returns an index value mi such that 0 <= mi < v.size()
//    and v[mi] >= v[i] for 0 <= i < v.size().
int index_of_max(const vector<int>& v) {
    if (v.empty()) cmpt::error("empty vector");

    int mi = 0;
	// note i is initialized to 1
    for(int i = 1; i < v.size(); i++) {
        if (v[i] > v[mi]) {
            mi = i;
        }
    }
    return mi;
}


int max(const vector<int>& v) {
    return v[index_of_max(v)];
}
```

Read the post-condition carefully, it is carefully worded!

Here's a version that uses a while-loop:

```cpp
int index_of_max2(const vector<int>& v) {
    if (v.empty()) cmpt::error("empty vector");

    int mi = 0;
    int i = 1;
    while (i < v.size()) {
        if (v[i] > v[mi]) {
            mi = i;
        }
        i++;
    }
    return mi;
}
```

This version will be useful below when counting how many times each line of the function runs.

## A Recursive Max
The max algorithm recursively using these two cases:

- **Base case**: if `v` has 1 element, return that 1 element
- **Recursive case**: otherwise, return the max of the first element and the max of the rest of `v`

In C++, we can implement it like this:

```cpp
int index_of_max_rec(const vector<int>& v, int begin) {
    if (begin >= v.size()) cmpt::error("empty vector");

    if (begin == v.size() - 1) {  // only 1 element, so return it
        return begin;
    } else {
        int mi = index_of_max_rec(v, begin + 1);  // recursive call
        if (v[mi] > v[begin]) {
            return mi;
        } else {
            return begin;
        }
    }
}

int index_of_max_rec(const vector<int>& v) {
    return index_of_max_rec(v, 0);
}
```

As is often the case with recursive functions, we have two functions here, one of them with an extra parameter to support the recursion.

## Counting Statements
Above we saw that the basic max algorithm does $n-1$ comparisons when finding the max of a $n$-element vector.

Here, we're going to count how many times *all* the lines of code are called. This can give us a better understanding of its performance.

When `index_of_max2(v)` is called on a vector with `n` elements, then each line of code is executed this many times:

```cpp
int index_of_max2(const vector<int>& v) {
    if (v.empty()) cmpt::error("empty vector");

    int mi = 0;              // 1
    int i = 1;               // 1
    while (i < v.size()) {   // n
        if (v[i] > v[mi]) {  // n - 1
            mi = i;          // ???
        }
        i++;                 // n - 1
    }
    return mi;               // 1
}
```

Most of the counts are not too hard to figure out, the first two lines, and the very last line, are executed once each. The expression `i < v.size()` is executed `n` times because `i` starts at 1, and the last time it is executed is when `i == v.size()`, i.e. when `i` is `n`. So that is `n` executions in total.

The expression  `v[i] > v[mi]` is executed *one less time* than the while-loop condition. When the condition `i < v.size()` evaluates to false, then `v[i] > v[mi]` is not executed.

But the number of times the statement `mi = i` is executed  is not so clear!


## Counting mi = i
How many times is `mi = i` called? There are two extreme cases:

- 0 times,  when `v[0]` is the max
- n - 1 times,  when `v` is in [[sorted order|ascending sorted order]]; every number is the max of all the numbers seen so far

What about on *average*? That's tricky! So lets run some experiments and count how many times `mi = i` is called. Here's some code that generates random vectors of length `n`:

```cpp
#include <cmath>

vector<int> rand_vec(int n) {
    vector<int> result(n);
    for(int i = 0; i < n; i++) {
        result[i] = rand();  // rand() returns a random int
    }                        // from 0 to RAND_MAX
    return result;
}
```

`rand()` and generates [[pseudo-random number|pseudo-random numbers]]. To make it truly unpredictable, we can set an initial seed for the generator based on the current time:

```cpp
#include <cmath>   // srand is from here
#include <ctime>   // time is from here 

int main() {
   srand(time(NULL));

   // ...
}
```

If you *don't* set a seed, or set the seed to be the same every time, then the random numbers will be the *same* every time you run the program. Having the same random numbers each time might be very useful for debugging.

This version of max counts the number of times the line `mi = i` executes, and prints a message on the screen when it's done:

```cpp
int index_of_max_count(const vector<int>& v) {
    if (v.empty()) cmpt::error("empty vector");

    int mi = 0;
    int i = 1;
    int mi_assign_i_count = 0;
    while (i < v.size()) {
        if (v[i] > v[mi]) {
            mi = i;
            mi_assign_i_count++;
        }
        i++;
    }
    cout << "mi_assign_i_count = " << mi_assign_i_count << "\n";
    return mi;
}
```

Now we can write some code for an experiment:

```cpp
void experiment() {
    const int n = 100;
    cout << "n = " << n << "\n";
    for(int i = 0; i < 10; i++) {
        vector<int> v = rand_vec(n);
        index_of_max_count(v);
    }
}
```

Here are some results:

- n = 100: 4, 6, 3, 7, 5, 2  (mean = 4.5)
- n = 1000: 7, 4, 8, 5, 3, 10 (mean = 6.17)
- n = 10000: 8, 7, 5, 12, 12, 10 (mean = 9.0)
- n = 100000: 8, 9, 9, 13, 14, 9 (mean = 10.33)
- n = 1000000: 9, 11, 19, 14, 8, 12 (mean = 12.17)

These results of are based on random data, so if you were to repeat these trials you probably won't get exactly the same values. But the means should be similar.

What's surprising about these numbers is how *small* they are, even with very large vectors. They suggest that for large amounts of randomly-ordered data `mi = i` is executed very few times. This is not obvious from the algorithm!

## An Estimate of the Correct Answer
Here's an informal argument about the average number of times `mi = i` is called on randomly ordered data. 

The probability that the 1st element is the largest of the first 1 elements is $\frac{1}{1}$. The probability that the 2nd element is the largest of the first 2 elements is $\frac{1}{2}$. The probability that the 3rd element is the largest of the first 3 elements is $\frac{1}{3}$. In general, the probability that the *n*th element is the largest of the first $n$ is $\frac{1}{n}$.

These probabilities are independent, and so the total expected number of times `mi = i` is called is $H_n = \frac{1}{1} + \frac{1}{2} + \frac{1}{3} + \ldots + \frac{1}{n}$.  This expression is the the *n*th [harmonic number](https://en.wikipedia.org/wiki/Harmonic_number), $H_n$, which grows very slowly, e.g. the billionth harmonic number is only about 21 (!).

So, if you call the max function on a vector with a billion entries, you would expect `mi = i` to be called about 21 times.

## Practice Questions
1. Give a definition for the *min* of a list, in the same style as the definition given in the notes for the max of a list.
2. Suppose you have a list of $n$ elements in some unknown order. About how many comparisons would you expect to do to calculate *both* the min and max item?
3. Implement a more general version of the max function that returns the max element in the range `v[begin]` to `v[end-1]` (note that `v[end]` is *not* included!):
   ```cpp
   int max(const vector<int>& v, int begin, int end)
   ```
4. In your own words, explain the *seed* for the `rand()` random number generator. How is it set? What happens if you set it to the same value every time? How can you make different random numbers each run of a program?
5. What is the value of $H_6$, i.e. the sixth harmonic number?
6. Suppose vector `v` has 5000 `int`s in some unknown order. What is the:
	a. minimum number of times that `mi = i` could be called? Under what conditions would that happen?
	b. maximum number of times that `mi = i` could be called? Under what conditions would that happen?
	c. average number of times that `mi = i` could be called?