## Recursion on Vectors (or arrays)
Suppose we want to sum the numbers in a vector. We can do that recursively as follows:

- **Base case**: the empty vector has sum 0, `sum({})` is 0.
- **Recursive case**: the sum of all the elements in `v` is `v[0] + sum(rest(v))`; the function `rest(v)` returns a copy of the original vector with its first element chopped off.

This definition is precise enough that we can trace examples by hand. For instance:

```
sum({8, 1, 4, 2}) = 8 + sum({1, 4, 2})
                  = 8 + 1 + sum({4, 2})
                  = 8 + 1 + 4 + sum({2})
                  = 8 + 1 + 4 + 2 + sum({})
                  = 8 + 1 + 4 + 2 + 0
                  = 15
```

To implement this in C++, we *could* write the `rest` function like this

```cpp
// Returns a new vector w of size v.size() - 1 such that 
// w[0] == v[1], w[1] == v[2], ..., w[v.size() - 2] == v[v.size() - 1].
// In other words, it returns a copy of v with the first element 
// chopped off.
vector<int> rest(const vector<int>& v) {
   vector<int> result;
   for (int i = 1; i < v.size(); ++i) {  // i starts at 1
      result.push_back(v[i]);
   }
   return result;
}
```

Now we can write `sum` as follows:

```cpp
int sum1(const vector<int>& v) {
   if (v.empty()) {  // base case
      return 0;  
   } else {          // recursive case
      return v[0] + sum1(rest(v));
   }
}
```

This works! Unfortunately, the `rest` function is *extremely inefficient*: for every call to `sum1` we end up making a new copy of almost the entire passed-in vector.

A more efficient approach is to *simulate* `rest` by re-writing `sum1` to accept *begin* and *end* parameters specifying the range of values in `v` that we want summed. Then we can efficiently access any sub-vector:

```cpp
// returns v[begin] + v[begin + 1] + ... + v[end - 1]
int sum2(const vector<int>& v, int begin, int end) {
   if (begin >= end) {
     return 0;
   } else {
     return v[begin] + sum2(v, begin + 1, end);
   }
}

// returns the sum of all the elements in v
int sum2(const vector<int>& v) {
   return sum2(v, 0, v.size());
}
```

Adding extra parameters in this way is a standard trick when writing recursive functions. Notice that we don't even need to include `end` in this case, because it never changes. So we could have just written this:

```cpp
// returns v[begin] + v[begin + 1] + ... + v[end - 1]
int sum3(const vector<int>& v, int begin) {
   if (begin >= v.size()) {
     return 0;
   } else {
     return v[begin] + sum3(v, begin + 1);
   }
}

// returns the sum of all the elements in v
int sum3(const vector<int>& v) {
   return sum3(v, 0);
}
```

Here's another example. Suppose we want a recursive function that returns `true` if all the numbers in a vector are even, and `false` otherwise:

```cpp
// Pre-condition:
//     all ints in v are >= 0
// Post-condition:
//     If v[0], v[1], ... v[n-1] are all even (n is v's size),
//     true is returned. Otherwise, false is returned.
//     If v is empty, true is returned.
bool all_even(const vector<int>& v) {
   // ...
}
```

Notice that the function is defined using a [[contract]]. This is a good way to define a function precisely, and we will often use [[contract|contracts]] to carefully specify how a function ought to work.

The recursive idea for implementing this function is the same as for the `sum` function: we check if the *first* number is even, and then recursively call `all_even` to check that the *rest* of the numbers are even. As with `sum`, we will use a helper function with an extra parameter to keep track of the sub-vector of `v` that is being processed:

```cpp
// Pre-condition:
//     begin >= 0
//     all ints in v are >= 0
// Post-condition:
//     Returns true if v[begin], v[begin+1], ... v[n-1] are all even,
//     where n is the size of v; false otherwise.
bool all_even(const vector<int>& v, int begin) {
   if (begin >= v.size()) {
     return true;
   } else if (v[begin] % 2 == 0) {
     return all_even(v, begin + 1);
   } else {
     return false;
   }
}
```

The first condition of the if-statement checks if the sub-vector being processed is empty. If `begin` is equal to the size of the vector, or is greater than the size, we consider that to be an empty vector, and so return true.

The next condition checks if the first element of the sub-vector is even. Since `begin` marks the start of the vector, `v[begin]` is that start of the vector (not `v[0]`!). If the first element is indeed even, then `all_even` is called on the rest of the vector, i.e. from location `begin + 1` onward.

Finally, the else part of the if-statement occurs just when `v[begin]` is odd. In that case, we no that the entire vector cannot be all even, and so `false` is returned immediately.

For convenience, we provide a function that doesn't require a starting index:

```cpp
bool all_even(const vector<int>& v) {
   return all_even(v, 0);
}
```
Functions with fewer parameters are quite common when writing recursive functions. Checking if an *entire* vector is all even is probably the most common case, and so `all_even(v)` makes this common case easier, and less error-prone, to use.

## Extra Examples
Here are a few more examples of using recursion on vectors (and strings):
- [[Recursive linear search]] shows how to implement [[linear search]] using recursion.
- [[binary search|Recursive binary search]] shows how to implement [[binary search]] using recursion.
- [[recursive acronym|Recursive acronyms]] are an amusing example of recursion.

## Practice Questions
Implement the following using recursion (and no loops, or library functions that do the hard part). For some questions, you may want to create a helper function with extra parameters. 

1. The sum of just the positive numbers in a vector. For example, the sum of the positive numbers in `{1, -3, -2, 6}` is 7.
2. The number of times `x` occurs in a vector. For example, in `{5, 2, 1, 5, 5}`, 5 occurs three times.
3. Print the elements of a `vector<int>`, one number per line.
4. Find the biggest number in a `vector<int>`.
5. Expand the [[recursive acronym]] YOPY = Your Own Personal YOPY some given number of times (as in the example in [[recursive acronym|recursive acronyms]]).

## Sample Solutions to Practice Questions
5. The sum of just the positive numbers in a vector. For example, the sum of the positive numbers in `{1, -3, -2, 6}` is 7.

   Sample solution:

   ```cpp
   int sum_pos(const vector<int>& v, int begin, int end) {
       if (begin >= end) {
           return 0;
       } else if (v[begin] > 0) {
           return v[begin] + sum_pos(v, begin + 1, end);
       } else {
           return sum_pos(v, begin + 1, end);
       }
   }

   int sum_pos(const vector<int>& v) {
       return sum_pos(v, 0, v.size());
   }

   void sum_pos_test() {
       cout << "Testing sum_pos ... ";
       vector<int> a = {2};
       vector<int> b = {-2, 1, 4, 0, -10};
       vector<int> c = {1, 2, 3};
       vector<int> d = {-1, -2, -3};
       assert(sum_pos(a) == 2);
       assert(sum_pos(b) == 5);
       assert(sum_pos(c) == 6);
       assert(sum_pos(d) == 0);
       cout << "all tests passed\n";
   }
   ```

6. The number of times `x` occurs in a vector. For example, in `{5, 2, 1,
   5, 5}`, 5 occurs three times.

   Sample solution:

   ```cpp
   int count_rec(const vector<int>& v, int x, int begin, int end) {
       if (begin >= end) {
           return 0;
       } else if (v[begin] == x) {
           return 1 + count_rec(v, x, begin + 1, end);
       } else {
           return count_rec(v, x, begin + 1, end);
       }
   }

   int count_rec(const vector<int>& v, int x) {
       return count_rec(v, x, 0, v.size());
   }

   void count_rec_test() {
       cout << "Testing count_rec ... ";
       vector<int> v = {2, 6, 6, 1, 6, 8, 2, 1};
       assert(count_rec(v, 5) == 0);
       assert(count_rec(v, 2) == 2);
       assert(count_rec(v, 6) == 3);
       assert(count_rec(v, 1) == 2);
       assert(count_rec(v, 8) == 1);
       cout << "all tests passed\n";
   }
   ```

7. Print the elements of a `vector<int>`, one number per line.

   Sample solution:

   ```cpp
   void print_vec(const vector<int>& v, int begin, int end) {
       if (begin >= end) {
           // do nothing
       } else {
           cout << v[begin] << "\n";
           print_vec(v, begin + 1, end);
       }
   }

   void print_vec(const vector<int>& v) {
       print_vec(v, 0, v.size());
   }

   void print_vec_test() {
       cout << "Testing print_vec ...\n";
       vector<int> v = {1, 2, 3, 4};
       print_vec(v);
       cout << "all tests passed\n";
   }
   ```

8. Find the biggest number in a `vector<int>`.

   Sample solution:

   ```cpp
   int max(const vector<int>& v, int begin, int end) {
       if (begin >= end) cmpt::error("empty vector has no max");
       if (end - begin == 1) {
           return v[begin];
       } else {
           return max(v[begin], max(v, begin + 1, end));
       }
   }

   int max(const vector<int>& v) {
       return max(v, 0, v.size());
   }

   void max_test() {
       cout << "Testing max ... ";
       vector<int> a = {2};
       vector<int> b = {-2, 1, 4, 0, -10};
       vector<int> c = {1, 2, 3};
       vector<int> d = {-1, -2, -3};
       assert(max(a) == 2);
       assert(max(b) == 4);
       assert(max(c) == 3);
       assert(max(d) == -1);
       cout << "all tests passed\n";
   }
   ```

9. Expand the [[recursive acronym]] YOPY = Your Own Personal YOPY some given number of times (as in the example in [[recursive acronym|recursive acronyms]]).

   Sample solution:

   ```cpp
   const string acronym = "YOPY";
   const string expansion = "Your Own Personal YOPY";
   string expand_YOPY(int n) {
       if (n == 0) {
           return "";
       } else if (n == 1) {
           return expansion;
       } else { // n > 1
           string prev = expand_YOPY(n - 1);
           // replace every occurrence of "MOMS" with its expansion
           int i = prev.find(acronym);
           while (i != string::npos) {
              prev.replace(i, acronym.size(), expansion);
              // search for next acronym after the current one
              i += expansion.size(); // skip over the just-replaced string
                                     // so the acronyms within it are not expanded
              i = prev.find(acronym, i);
           }
           return prev;
       } // if
   }

   void expand_YOPY_test() {
       cout << "Testing expand_YOPY ... ";
       cout << expand_YOPY(5) << "\n";
       cout << "all tests passed\n";
   }
   ```
